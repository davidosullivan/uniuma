﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UMA;

#if UNITY_EDITOR //Dev-Only
using UnityEditor;
#endif

namespace UniUMA
{
	public static class UniUMASkeleton
	{
		#region Dev-Only
#if UNITY_EDITOR
		[MenuItem("CONTEXT/Transform/TestBones", false, 0)]
		private static void CONTEXTTestBones(MenuCommand menuCommand)
		{
			SetMasculineFemale();
		}

		[MenuItem("CONTEXT/Transform/Gender Bone Code Log", false, 0)]
		private static void CONTEXTGenderBoneLog(MenuCommand menuCommand)
		{
			Transform rootBone = menuCommand.context as Transform;
			Transform[] stack = rootBone.GetComponentsInChildren<Transform>();
			Transform root = rootBone.parent;
			rootBone.SetParent(null, true);

			string text = "enum Bones Count: " + stack.Length.ToString();
			text += System.Environment.NewLine;
			text += "{";
			text += System.Environment.NewLine;

			foreach (Transform transform in stack)
			{
				text += transform.name + ",";
				text += System.Environment.NewLine;
			}

			text += "}";
			text += System.Environment.NewLine;

			Debug.Log(text);

			text = "";
			for (int i = 0; i < stack.Length; i++)
			{
				string position = string.Format("new Vector3({0}f, {1}f, {2}f)",
					FormatValue(stack[i].localPosition.x.ToString()),
					FormatValue(stack[i].localPosition.y.ToString()),
					FormatValue(stack[i].localPosition.z.ToString())
					);

				string rotation = string.Format("new Quaternion({0}f, {1}f, {2}f, {3}f)",
				   FormatValue(stack[i].localRotation.x.ToString()),
				   FormatValue(stack[i].localRotation.y.ToString()),
				   FormatValue(stack[i].localRotation.z.ToString()),
				   FormatValue(stack[i].localRotation.w.ToString())
				   );

				string scale = string.Format("new Vector3({0}f, {1}f, {2}f)",
				   FormatValue(stack[i].localScale.x.ToString()),
				   FormatValue(stack[i].localScale.y.ToString()),
				   FormatValue(stack[i].localScale.z.ToString())
				   );

				text += string.Format("case {0}: position = {1}; rotation = {2}; scale = {3}; break; //{4}", i, position, rotation, scale, stack[i].name);
				text += System.Environment.NewLine;
			}

			string path = EditorUtility.SaveFilePanelInProject("GenderBones", "GenderBone Log", "txt", "txt");
			if (path == "") return;
			if (!path.EndsWith(".txt")) Debug.LogWarning("file path must end with .txt");
			if (System.IO.File.Exists(path)) System.IO.File.Delete(path);
			System.IO.File.WriteAllText(path, text);
			AssetDatabase.Refresh();

			text = "";
			for (int i = 0; i < stack.Length; i++)
			{
				text += string.Format("case {0}: return {1};", i, FormatValue(GetIndex(stack, stack[i].parent).ToString()));
				text += System.Environment.NewLine;
			}

			Debug.Log(text);

			rootBone.SetParent(root, true);
		}


		private static int GetIndex(Transform[] items, Transform item)
		{
			if (item == null) return -1;
			for (int i = 0; i < items.Length; i++) if (items[i].name == item.name) return i;
			return -1;
		}

		private static string FormatValue(string value)
		{
			value = value.Replace("(", "");
			value = value.Replace(")", "");
			value = value.Replace(",", "");
			value = value.Replace(" ", "");
			value = value.Replace("E", "e");
			value = RemoveDecimal(value);

			return value;
		}

		private static string RemoveDecimal(string value)
		{
			string text = "";
			string[] position = value.Split(' ');
			for (int i = 0; i < position.Length; i++)
			{
				if (position[i].Length == 3)
					position[i] = position[i].Replace(".0", "");
				text += position[i] + (i == position.Length - 1 && position.Length > 1 ? "" : "");
			}
			return text;

		}
#endif
		#endregion Dev-Only

		public static readonly int boneCount = 161;

		#region Enum
		public enum UniBones
		{
			Hips,
			LeftUpLeg,
			LeftGluteus,
			LeftLeg,
			LeftFoot,
			LeftToeBase,
			LeftLegAdjust,
			LeftUpLegAdjust,
			LowerBack,
			LowerBackAdjust,
			LowerBackBelly,
			Spine,
			Spine1,
			LeftOuterBreast,
			LeftInnerBreast,
			LeftInnerBreastAdjust,
			LeftOuterBreastAdjust,
			LeftShoulder,
			LeftArm,
			LeftArmAdjust,
			LeftForeArm,
			LeftForeArmAdjust,
			LeftForeArmTwist,
			LeftForeArmTwistAdjust,
			LeftHand,
			LeftHandFinger01_01,
			LeftHandFinger01_02,
			LeftHandFinger01_03,
			LeftHandFinger02_01,
			LeftHandFinger02_02,
			LeftHandFinger02_03,
			LeftHandFinger03_01,
			LeftHandFinger03_02,
			LeftHandFinger03_03,
			LeftHandFinger04_01,
			LeftHandFinger04_02,
			LeftHandFinger04_03,
			LeftHandFinger05_01,
			LeftHandFinger05_02,
			LeftHandFinger05_03,
			LeftShoulderAdjust,
			LeftTrapezius,
			Neck,
			Head,
			HeadAdjust,
			InnerMouthUpperAdjust,
			UpperNeckAdjust,
			LeftCheek,
			LeftCheekAdjust,
			LeftEar,
			LeftEarAdjust,
			LeftEye,
			LeftEyeAdjust,
			LeftEyeGlobe,
			LeftLowerLid,
			LeftLowerLidAdjust,
			LeftUpperLid,
			LeftUpperLidAdjust,
			LeftEyebrowLow,
			LeftEyebrowLowAdjust,
			LeftEyebrowMiddle,
			LeftEyebrowMiddleAdjust,
			LeftEyebrowUp,
			LeftEyebrowUpAdjust,
			LeftLipsSuperiorMiddle,
			LeftLipsSuperiorMiddleAdjust,
			LeftLowCheek,
			LeftLowCheekAdjust,
			LeftNose,
			LeftNoseAdjust,
			LipsSuperior,
			LipsSuperiorAdjust,
			Mandible,
			InnerMouthLowerAdjust,
			LeftLips,
			LeftLipsAdjust,
			LeftLipsInferior,
			LeftLipsInferiorAdjust,
			LeftLowMaxilar,
			LeftLowMaxilarAdjust,
			LipsInferior,
			LipsInferiorAdjust,
			MandibleAdjust,
			RightLips,
			RightLipsAdjust,
			RightLipsInferior,
			RightLipsInferiorAdjust,
			RightLowMaxilar,
			RightLowMaxilarAdjust,
			Tongue01,
			Tongue02,
			NoseBase,
			NoseBaseAdjust,
			NoseMiddle,
			NoseMiddleAdjust,
			NoseTop,
			NoseTopAdjust,
			RightCheek,
			RightCheekAdjust,
			RightEar,
			RightEarAdjust,
			RightEye,
			RightEyeAdjust,
			RightEyeGlobe,
			RightLowerLid,
			RightLowerLidAdjust,
			RightUpperLid,
			RightUpperLidAdjust,
			RightEyebrowLow,
			RightEyebrowLowAdjust,
			RightEyebrowMiddle,
			RightEyebrowMiddleAdjust,
			RightEyebrowUp,
			RightEyebrowUpAdjust,
			RightLipsSuperiorMiddle,
			RightLipsSuperiorMiddleAdjust,
			RightLowCheek,
			RightLowCheekAdjust,
			RightNose,
			RightNoseAdjust,
			UpperLips,
			UpperLipsAdjust,
			NeckAdjust,
			RightOuterBreast,
			RightInnerBreast,
			RightInnerBreastAdjust,
			RightOuterBreastAdjust,
			RightShoulder,
			RightArm,
			RightArmAdjust,
			RightForeArm,
			RightForeArmAdjust,
			RightForeArmTwist,
			RightForeArmTwistAdjust,
			RightHand,
			RightHandFinger01_01,
			RightHandFinger01_02,
			RightHandFinger01_03,
			RightHandFinger02_01,
			RightHandFinger02_02,
			RightHandFinger02_03,
			RightHandFinger03_01,
			RightHandFinger03_02,
			RightHandFinger03_03,
			RightHandFinger04_01,
			RightHandFinger04_02,
			RightHandFinger04_03,
			RightHandFinger05_01,
			RightHandFinger05_02,
			RightHandFinger05_03,
			RightShoulderAdjust,
			RightTrapezius,
			Spine1Adjust,
			SpineAdjust,
			RightUpLeg,
			RightGluteus,
			RightLeg,
			RightFoot,
			RightToeBase,
			RightLegAdjust,
			RightUpLegAdjust,

		}

		#endregion Enum

		#region GetParent

		public static int Parent(int index)
		{
			switch (index)
			{

				case 0: return -1;
				case 1: return 0;
				case 2: return 1;
				case 3: return 1;
				case 4: return 3;
				case 5: return 4;
				case 6: return 3;
				case 7: return 1;
				case 8: return 0;
				case 9: return 8;
				case 10: return 9;
				case 11: return 8;
				case 12: return 11;
				case 13: return 12;
				case 14: return 13;
				case 15: return 14;
				case 16: return 13;
				case 17: return 12;
				case 18: return 17;
				case 19: return 18;
				case 20: return 18;
				case 21: return 20;
				case 22: return 20;
				case 23: return 22;
				case 24: return 20;
				case 25: return 24;
				case 26: return 25;
				case 27: return 26;
				case 28: return 24;
				case 29: return 28;
				case 30: return 29;
				case 31: return 24;
				case 32: return 31;
				case 33: return 32;
				case 34: return 24;
				case 35: return 34;
				case 36: return 35;
				case 37: return 24;
				case 38: return 37;
				case 39: return 38;
				case 40: return 17;
				case 41: return 17;
				case 42: return 12;
				case 43: return 42;
				case 44: return 43;
				case 45: return 44;
				case 46: return 44;
				case 47: return 43;
				case 48: return 47;
				case 49: return 43;
				case 50: return 49;
				case 51: return 43;
				case 52: return 51;
				case 53: return 51;
				case 54: return 51;
				case 55: return 54;
				case 56: return 51;
				case 57: return 56;
				case 58: return 43;
				case 59: return 58;
				case 60: return 43;
				case 61: return 60;
				case 62: return 43;
				case 63: return 62;
				case 64: return 43;
				case 65: return 64;
				case 66: return 43;
				case 67: return 66;
				case 68: return 43;
				case 69: return 68;
				case 70: return 43;
				case 71: return 70;
				case 72: return 43;
				case 73: return 72;
				case 74: return 72;
				case 75: return 74;
				case 76: return 72;
				case 77: return 76;
				case 78: return 72;
				case 79: return 78;
				case 80: return 72;
				case 81: return 80;
				case 82: return 72;
				case 83: return 72;
				case 84: return 83;
				case 85: return 72;
				case 86: return 85;
				case 87: return 72;
				case 88: return 87;
				case 89: return 72;
				case 90: return 89;
				case 91: return 43;
				case 92: return 91;
				case 93: return 43;
				case 94: return 93;
				case 95: return 43;
				case 96: return 95;
				case 97: return 43;
				case 98: return 97;
				case 99: return 43;
				case 100: return 99;
				case 101: return 43;
				case 102: return 101;
				case 103: return 101;
				case 104: return 101;
				case 105: return 104;
				case 106: return 101;
				case 107: return 106;
				case 108: return 43;
				case 109: return 108;
				case 110: return 43;
				case 111: return 110;
				case 112: return 43;
				case 113: return 112;
				case 114: return 43;
				case 115: return 114;
				case 116: return 43;
				case 117: return 116;
				case 118: return 43;
				case 119: return 118;
				case 120: return 43;
				case 121: return 120;
				case 122: return 42;
				case 123: return 12;
				case 124: return 123;
				case 125: return 124;
				case 126: return 123;
				case 127: return 12;
				case 128: return 127;
				case 129: return 128;
				case 130: return 128;
				case 131: return 130;
				case 132: return 130;
				case 133: return 132;
				case 134: return 130;
				case 135: return 134;
				case 136: return 135;
				case 137: return 136;
				case 138: return 134;
				case 139: return 138;
				case 140: return 139;
				case 141: return 134;
				case 142: return 141;
				case 143: return 142;
				case 144: return 134;
				case 145: return 144;
				case 146: return 145;
				case 147: return 134;
				case 148: return 147;
				case 149: return 148;
				case 150: return 127;
				case 151: return 127;
				case 152: return 12;
				case 153: return 11;
				case 154: return 0;
				case 155: return 154;
				case 156: return 154;
				case 157: return 156;
				case 158: return 157;
				case 159: return 156;
				case 160: return 154;

				default: return -1;
			}
		}

		#endregion GetParent

		#region BindPose

		private static void BindPoseLocalTranslation(int index, Transform transform)
		{
			Vector3 position; Quaternion rotation; Vector3 scale;

			switch (index)
			{
				case 0: position = new Vector3(-4.030033e-07f, 1.058908f, -0.02514136f); rotation = new Quaternion(0.03907284f, 0.03907242f, -0.7060263f, 0.7060266f); scale = new Vector3(0.9999998f, 1f, 1f); break; //Hips
				case 1: position = new Vector3(0.01727386f, -0.1102599f, 0.002575339f); rotation = new Quaternion(-0.05595891f, -0.07567072f, 0.9947243f, 0.04081914f); scale = new Vector3(1f, 1.000002f, 1.000002f); break; //LeftUpLeg
				case 2: position = new Vector3(-1.840017e-07f, -7.916242e-09f, -6.676419e-08f); rotation = new Quaternion(0.06176981f, -0.854369f, -0.01432007f, 0.515784f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftGluteus
				case 3: position = new Vector3(-0.4368479f, 4.63333e-08f, 2.153683e-09f); rotation = new Quaternion(-0.04350501f, -0.02888393f, -0.03022164f, 0.9981782f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftLeg
				case 4: position = new Vector3(-0.4971371f, 4.470348e-08f, 4.65661e-10f); rotation = new Quaternion(0.8610113f, -0.01866952f, -0.5025506f, 0.07585419f); scale = new Vector3(1f, 1f, 1f); break; //LeftFoot
				case 5: position = new Vector3(-0.1806862f, 2.817251e-08f, -7.450581e-09f); rotation = new Quaternion(-0.06246545f, -0.2067474f, -0.02363384f, 0.9761122f); scale = new Vector3(1f, 1f, 1f); break; //LeftToeBase
				case 6: position = new Vector3(-0.1242836f, 4.563481e-08f, 7.357448e-08f); rotation = new Quaternion(1.130393e-07f, -2.793968e-09f, -1.676381e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLegAdjust
				case 7: position = new Vector3(-0.1092127f, -1.816079e-08f, -9.936048e-08f); rotation = new Quaternion(-0.03335001f, -1.281282e-09f, 9.40577e-09f, 0.9994438f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftUpLegAdjust
				case 8: position = new Vector3(-0.05387875f, 4.067734e-09f, -7.450577e-09f); rotation = new Quaternion(5.486468e-08f, -0.07920214f, 3.437416e-08f, 0.9968586f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LowerBack
				case 9: position = new Vector3(-0.05110956f, 3.858688e-09f, 4e-15f); rotation = new Quaternion(2.218181e-08f, 9.313225e-09f, 1.774365e-15f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LowerBackAdjust
				case 10: position = new Vector3(-0.03330042f, 2.514131e-09f, 1.676381e-08f); rotation = new Quaternion(6.237154e-08f, -1.862647e-09f, -1.777506e-15f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LowerBackBelly
				case 11: position = new Vector3(-0.1835814f, 1.106442e-08f, -0.0001194403f); rotation = new Quaternion(-5.3581e-07f, -0.08683416f, 1.383218e-08f, 0.9962228f); scale = new Vector3(1f, 1f, 1f); break; //Spine
				case 12: position = new Vector3(-0.1771948f, 1.338729e-08f, -2.04891e-05f); rotation = new Quaternion(4.719643e-07f, 0.1107154f, -6.315932e-10f, 0.9938522f); scale = new Vector3(1f, 1f, 1f); break; //Spine1
				case 13: position = new Vector3(-0.03042253f, -0.06953058f, 0.1136089f); rotation = new Quaternion(0.1452052f, 0.6784657f, 0.1333818f, 0.7076786f); scale = new Vector3(1f, 1f, 1f); break; //LeftOuterBreast
				case 14: position = new Vector3(-0.02736984f, -6.519258e-09f, -1.001172e-08f); rotation = new Quaternion(0.02537104f, -0.05112287f, 0.008944063f, 0.99833f); scale = new Vector3(0.9999998f, 0.9999999f, 1f); break; //LeftInnerBreast
				case 15: position = new Vector3(2.235174e-08f, 3.539026e-08f, -6.239861e-08f); rotation = new Quaternion(6.61239e-08f, 1.303852e-08f, 6.30971e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftInnerBreastAdjust
				case 16: position = new Vector3(-1.862645e-08f, 1.955777e-08f, 4.703179e-08f); rotation = new Quaternion(-5.702022e-07f, -7.683411e-09f, 4.244794e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftOuterBreastAdjust
				case 17: position = new Vector3(-0.1819358f, -0.0523563f, 0.003283322f); rotation = new Quaternion(0.04116446f, -0.03522629f, 0.8212653f, 0.5679682f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftShoulder
				case 18: position = new Vector3(-0.1421482f, 7.576961e-05f, 1.796521e-05f); rotation = new Quaternion(-0.01499067f, -0.03559915f, 0.1911933f, 0.9807921f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftArm
				case 19: position = new Vector3(-0.06878747f, -3.119931e-07f, -1.713634e-07f); rotation = new Quaternion(0.2379593f, -2.445107e-08f, 2.610513e-07f, 0.9712752f); scale = new Vector3(1f, 1f, 1f); break; //LeftArmAdjust
				case 20: position = new Vector3(-0.2838205f, 0.0003528679f, 0.001950182f); rotation = new Quaternion(0.04842848f, 0.1092363f, -0.02532759f, 0.9925123f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftForeArm
				case 21: position = new Vector3(-0.02799021f, 2.518063e-07f, -1.136214e-07f); rotation = new Quaternion(4.91594e-05f, -4.563481e-08f, -2.537781e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftForeArmAdjust
				case 22: position = new Vector3(-0.1475397f, 2.02388e-07f, -6.519258e-08f); rotation = new Quaternion(3.532041e-07f, 2.04891e-08f, -2.741872e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftForeArmTwist
				case 23: position = new Vector3(-0.02798366f, 4.808535e-07f, -3.72529e-08f); rotation = new Quaternion(4.881911e-05f, -3.72529e-08f, 3.634123e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftForeArmTwistAdjust
				case 24: position = new Vector3(-0.280726f, -0.000445348f, 9.783916e-05f); rotation = new Quaternion(-0.7203866f, 0.004556113f, 0.01648098f, 0.693362f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999998f); break; //LeftHand
				case 25: position = new Vector3(-0.08573797f, 0.03069647f, 0.0004208754f); rotation = new Quaternion(-0.2053065f, -0.06117819f, -0.1396823f, 0.9667447f); scale = new Vector3(0.9999999f, 0.9999998f, 0.9999998f); break; //LeftHandFinger01_01
				case 26: position = new Vector3(-0.03693616f, 2.235174e-08f, 0f); rotation = new Quaternion(0.01495297f, -0.1447613f, -0.02117516f, 0.989127f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftHandFinger01_02
				case 27: position = new Vector3(-0.02210755f, 2.235174e-08f, 1.490116e-08f); rotation = new Quaternion(-0.03146791f, -0.04711933f, 0.003357276f, 0.9983879f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger01_03
				case 28: position = new Vector3(-0.09297282f, 0.01262062f, 0.006804227f); rotation = new Quaternion(-0.1347939f, -0.07683945f, -0.09632996f, 0.983182f); scale = new Vector3(0.9999998f, 0.9999998f, 0.9999999f); break; //LeftHandFinger02_01
				case 29: position = new Vector3(-0.03892574f, -1.490116e-08f, -1.117587e-08f); rotation = new Quaternion(0.004608932f, -0.1096316f, 0.00816725f, 0.9939281f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftHandFinger02_02
				case 30: position = new Vector3(-0.0321391f, 0f, 5.960464e-08f); rotation = new Quaternion(0.009086274f, -0.06614653f, -0.01302745f, 0.9976835f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger02_03
				case 31: position = new Vector3(-0.09300856f, -0.009580866f, 0.008817304f); rotation = new Quaternion(-0.07886449f, -0.05687004f, -0.02577983f, 0.9949279f); scale = new Vector3(0.9999998f, 0.9999998f, 0.9999998f); break; //LeftHandFinger03_01
				case 32: position = new Vector3(-0.04725769f, 2.235174e-08f, 2.533197e-07f); rotation = new Quaternion(0.002872071f, -0.09684265f, -0.01627373f, 0.9951625f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger03_02
				case 33: position = new Vector3(-0.0404212f, 0f, 2.793968e-09f); rotation = new Quaternion(0.01665002f, -0.016046f, 0.01317811f, 0.9996458f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftHandFinger03_03
				case 34: position = new Vector3(-0.09163309f, -0.03711054f, 0.003350079f); rotation = new Quaternion(-0.03604886f, -0.02278479f, 0.02498204f, 0.9987779f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftHandFinger04_01
				case 35: position = new Vector3(-0.05416504f, 5.587935e-09f, 4.65661e-10f); rotation = new Quaternion(0.00267789f, -0.07985283f, -0.004389229f, 0.9967934f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger04_02
				case 36: position = new Vector3(-0.03353671f, 0f, 6.891787e-08f); rotation = new Quaternion(-0.001482369f, -0.07270253f, -0.01461355f, 0.9972456f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger04_03
				case 37: position = new Vector3(-0.03816172f, -0.04425694f, -0.02005033f); rotation = new Quaternion(0.5593043f, -0.08592308f, 0.4328482f, 0.7017395f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftHandFinger05_01
				case 38: position = new Vector3(-0.0293213f, -9.406358e-08f, -2.980232e-08f); rotation = new Quaternion(-0.04490694f, -0.03217196f, -0.02465373f, 0.9981686f); scale = new Vector3(0.9999999f, 0.9999998f, 1f); break; //LeftHandFinger05_02
				case 39: position = new Vector3(-0.03140647f, -1.862645e-08f, -2.04891e-08f); rotation = new Quaternion(0.02490249f, -0.0553872f, -0.03426434f, 0.9975661f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftHandFinger05_03
				case 40: position = new Vector3(-0.0355275f, 4.598405e-08f, -1.303852e-08f); rotation = new Quaternion(0.1465888f, 8.320445e-08f, 2.291948e-07f, 0.9891975f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftShoulderAdjust
				case 41: position = new Vector3(-0.03261713f, 0.02943397f, -0.03281876f); rotation = new Quaternion(0.690668f, -0.08797558f, -0.164732f, 0.6986426f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftTrapezius
				case 42: position = new Vector3(-0.1943358f, 1.467203e-08f, 8.337611e-08f); rotation = new Quaternion(-3.055476e-07f, 0.09072302f, -2.61727e-08f, 0.9958762f); scale = new Vector3(0.9999999f, 1f, 1f); break; //Neck
				case 43: position = new Vector3(-0.1008214f, 7.611813e-09f, 1.862645e-08f); rotation = new Quaternion(1.246556e-07f, -0.08316033f, 2.214955e-08f, 0.9965362f); scale = new Vector3(0.9999999f, 1f, 1f); break; //Head
				case 44: position = new Vector3(-0.05122184f, 3.867148e-09f, 2.793968e-08f); rotation = new Quaternion(-4.961744e-08f, 1.583249e-08f, -4.441694e-15f, 1f); scale = new Vector3(1f, 1f, 1f); break; //HeadAdjust
				case 45: position = new Vector3(0.02003501f, -3.408349e-09f, 0.07601107f); rotation = new Quaternion(-1.876854e-05f, 0.7009441f, 1.840291e-05f, 0.7132162f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //InnerMouthUpperAdjust
				case 46: position = new Vector3(0.03893473f, -5.844255e-09f, -0.0006115008f); rotation = new Quaternion(-1.35655e-08f, 0.007815709f, 1.267646e-08f, 0.9999695f); scale = new Vector3(1f, 1f, 0.9999999f); break; //UpperNeckAdjust
				case 47: position = new Vector3(-0.07357531f, -0.03163493f, 0.1118161f); rotation = new Quaternion(0.6685909f, 0.2419274f, -0.660424f, -0.2414488f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftCheek
				case 48: position = new Vector3(7.485505e-08f, 6.053597e-09f, 1.535736e-07f); rotation = new Quaternion(-5.360926e-08f, -2.740911e-06f, 1.187883e-07f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftCheekAdjust
				case 49: position = new Vector3(-0.07184277f, -0.07856885f, 0.04034142f); rotation = new Quaternion(-0.1366631f, -0.5509136f, 0.1176816f, 0.8148426f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftEar
				case 50: position = new Vector3(-6.891787e-08f, 1.862645e-09f, 1.629815e-07f); rotation = new Quaternion(-1.18278e-07f, 1.927838e-07f, -6.286427e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEarAdjust
				case 51: position = new Vector3(-0.09909851f, -0.03493076f, 0.1163927f); rotation = new Quaternion(0.7124566f, -3.096531e-06f, -0.7017162f, -4.228132e-06f); scale = new Vector3(1f, 1f, 1f); break; //LeftEye
				case 52: position = new Vector3(1.240848e-07f, -1.322174e-07f, -9.934786e-07f); rotation = new Quaternion(2.147789e-06f, 3.591571e-06f, 1.243719e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeAdjust
				case 53: position = new Vector3(4.212374e-08f, -8.005618e-08f, -3.974333e-07f); rotation = new Quaternion(4.362492e-06f, -3.746247e-06f, 3.239956e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeGlobe
				case 54: position = new Vector3(4.212374e-08f, -8.005618e-08f, -3.974333e-07f); rotation = new Quaternion(0.000275448f, -0.1797692f, 1.183601e-05f, 0.9837088f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftLowerLid
				case 55: position = new Vector3(1.272072e-07f, 5.74801e-10f, 1.22307e-07f); rotation = new Quaternion(0.0002783754f, -0.000468514f, -3.686532e-05f, 0.9999999f); scale = new Vector3(1f, 1f, 1f); break; //LeftLowerLidAdjust
				case 56: position = new Vector3(4.212374e-08f, -8.005618e-08f, -3.974333e-07f); rotation = new Quaternion(-0.0003550898f, 0.1202657f, 1.449449e-05f, 0.9927417f); scale = new Vector3(1f, 1f, 1f); break; //LeftUpperLid
				case 57: position = new Vector3(-3.106607e-08f, -5.192305e-09f, -1.021363e-08f); rotation = new Quaternion(-0.000349355f, 0.0003599033f, -3.604178e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftUpperLidAdjust
				case 58: position = new Vector3(-0.1060048f, -0.0170085f, 0.08253036f); rotation = new Quaternion(0.762741f, -2.120911e-07f, -0.6467041f, 2.197782e-07f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftEyebrowLow
				case 59: position = new Vector3(-9.223863e-09f, 1.84478e-10f, 1.020914e-07f); rotation = new Quaternion(-7.424768e-08f, 7.785858e-07f, -1.963213e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftEyebrowLowAdjust
				case 60: position = new Vector3(-0.1106329f, -0.03322385f, 0.08008374f); rotation = new Quaternion(0.756873f, -1.676738e-07f, -0.6535621f, 1.681009e-07f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftEyebrowMiddle
				case 61: position = new Vector3(1.411626e-08f, 8.085321e-09f, -4.599152e-09f); rotation = new Quaternion(-3.736196e-08f, 1.66893e-06f, 4.800712e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyebrowMiddleAdjust
				case 62: position = new Vector3(-0.1100774f, -0.05020974f, 0.0698568f); rotation = new Quaternion(0.7581918f, -0.000482097f, -0.6520314f, 0.0003127548f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftEyebrowUp
				case 63: position = new Vector3(-1.17554e-07f, -4.82032e-10f, -2.11684e-07f); rotation = new Quaternion(-3.685528e-07f, 1.020942e-06f, 2.107572e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftEyebrowUpAdjust
				case 64: position = new Vector3(-0.02797573f, -0.01024674f, 0.1459551f); rotation = new Quaternion(0.7940922f, 0.05977351f, -0.6030684f, -0.04640192f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLipsSuperiorMiddle
				case 65: position = new Vector3(-1.001172e-07f, -7.450581e-09f, -1.179433e-08f); rotation = new Quaternion(-0.0001272981f, 1.57873e-05f, 4.556496e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftLipsSuperiorMiddleAdjust
				case 66: position = new Vector3(-0.02599579f, -0.03721036f, 0.1100256f); rotation = new Quaternion(-0.6591189f, -0.2391571f, 0.6702259f, 0.2432355f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftLowCheek
				case 67: position = new Vector3(-4.470348e-08f, -1.303852e-08f, -1.402782e-08f); rotation = new Quaternion(-2.788202e-07f, -4.787581e-09f, -1.382869e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftLowCheekAdjust
				case 68: position = new Vector3(-0.05234227f, -0.0268392f, 0.1018849f); rotation = new Quaternion(0.7088718f, 0.001019317f, -0.7053237f, 0.004262295f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftNose
				case 69: position = new Vector3(8.125789e-08f, 2.208981e-08f, 2.514571e-08f); rotation = new Quaternion(-0.0003554253f, -1.413369e-06f, -3.710738e-10f, 0.9999999f); scale = new Vector3(1f, 1f, 1f); break; //LeftNoseAdjust
				case 70: position = new Vector3(-0.02857737f, -9.259574e-05f, 0.148388f); rotation = new Quaternion(0.7874869f, -2.754394e-07f, -0.6163315f, -1.925385e-07f); scale = new Vector3(1f, 1f, 1f); break; //LipsSuperior
				case 71: position = new Vector3(2.979479e-08f, 3.0063e-11f, 4.465657e-08f); rotation = new Quaternion(-7.338064e-07f, -6.295742e-06f, 1.251337e-09f, 1f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LipsSuperiorAdjust
				case 72: position = new Vector3(-0.04665299f, -9.260827e-05f, 0.04581964f); rotation = new Quaternion(-0.4972367f, 1.315577e-07f, 0.867615f, -3.804977e-08f); scale = new Vector3(1f, 1f, 1f); break; //Mandible
				case 73: position = new Vector3(-0.04735408f, -9.325772e-05f, -0.01850047f); rotation = new Quaternion(0.9627837f, 3.899632e-06f, -0.2702733f, 1.321468e-05f); scale = new Vector3(0.9999998f, 1f, 1f); break; //InnerMouthLowerAdjust
				case 74: position = new Vector3(-0.08324832f, 0.02638567f, 0.02120601f); rotation = new Quaternion(-0.02273114f, 0.2682542f, -0.08131652f, 0.9596409f); scale = new Vector3(1f, 1f, 1f); break; //LeftLips
				case 75: position = new Vector3(-7.53522e-09f, -6.008804e-09f, -1.246026e-07f); rotation = new Quaternion(2.632683e-07f, -7.450572e-08f, -2.533197e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsAdjust
				case 76: position = new Vector3(-0.09703344f, 0.01035993f, 0.02782829f); rotation = new Quaternion(-0.0144366f, 0.180118f, -0.08480807f, 0.9798759f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftLipsInferior
				case 77: position = new Vector3(9.080395e-09f, 3.72529e-09f, 5.534275e-08f); rotation = new Quaternion(2.614688e-06f, 5.487163e-07f, -8.987261e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsInferiorAdjust
				case 78: position = new Vector3(-0.03670135f, 0.05756952f, -0.02171992f); rotation = new Quaternion(0.02974185f, 0.1056295f, 0.2273467f, 0.9676111f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftLowMaxilar
				case 79: position = new Vector3(1.955777e-08f, 2.468005e-08f, 5.937181e-09f); rotation = new Quaternion(-3.704336e-07f, -1.147855e-07f, -1.024455e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftLowMaxilarAdjust
				case 80: position = new Vector3(-0.09825906f, -2.134807e-05f, 0.02751286f); rotation = new Quaternion(-3.542938e-08f, 0.1730407f, -4.849679e-08f, 0.9849147f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LipsInferior
				case 81: position = new Vector3(7.443742e-09f, 3.643e-12f, -7.444322e-09f); rotation = new Quaternion(-3.623294e-07f, -6.966293e-06f, 4.555583e-10f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LipsInferiorAdjust
				case 82: position = new Vector3(2.796098e-09f, 1.867e-12f, 1.967685e-08f); rotation = new Quaternion(-8.661769e-08f, -4.321337e-07f, 4.55327e-11f, 1f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //MandibleAdjust
				case 83: position = new Vector3(-0.0832483f, -0.0265878f, 0.021206f); rotation = new Quaternion(0.02273111f, 0.2682542f, 0.0813167f, 0.9596409f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLips
				case 84: position = new Vector3(-1.498535e-08f, 1.369187e-09f, -2.317582e-08f); rotation = new Quaternion(-1.63426e-07f, -1.639127e-07f, 1.341105e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightLipsAdjust
				case 85: position = new Vector3(-0.09703344f, -0.01056204f, 0.02782829f); rotation = new Quaternion(0.01443662f, 0.1801181f, 0.08480798f, 0.9798759f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLipsInferior
				case 86: position = new Vector3(-2.712477e-08f, 2.793968e-09f, 1.10198e-07f); rotation = new Quaternion(-2.654444e-06f, 4.273097e-07f, 6.91507e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightLipsInferiorAdjust
				case 87: position = new Vector3(-0.03670148f, -0.05777172f, -0.02171984f); rotation = new Quaternion(-0.0297418f, 0.1056291f, -0.2273471f, 0.9676111f); scale = new Vector3(1f, 1f, 1f); break; //RightLowMaxilar
				case 88: position = new Vector3(4.097819e-08f, -4.563481e-08f, 1.088483e-08f); rotation = new Quaternion(3.664754e-07f, -4.621688e-08f, 8.288771e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightLowMaxilarAdjust
				case 89: position = new Vector3(-0.03220025f, -9.31353e-05f, -0.01816181f); rotation = new Quaternion(2.000553e-06f, 0.2488658f, -7.697171e-07f, 0.968538f); scale = new Vector3(1f, 1f, 1f); break; //Tongue01
				case 90: position = new Vector3(-0.02827538f, 2.134978e-09f, -1.052374e-07f); rotation = new Quaternion(0.998992f, 5.490011e-08f, -0.04489022f, 7.208658e-06f); scale = new Vector3(1f, 1f, 1f); break; //Tongue02
				case 91: position = new Vector3(-0.06001535f, -9.258853e-05f, 0.1482238f); rotation = new Quaternion(0.709553f, -1.558046e-08f, -0.7046521f, 2.297982e-07f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //NoseBase
				case 92: position = new Vector3(-4.376433e-08f, 2.566e-12f, -1.287779e-07f); rotation = new Quaternion(-3.296439e-08f, -0.0006734828f, 6.408466e-09f, 0.9999998f); scale = new Vector3(1f, 1f, 1f); break; //NoseBaseAdjust
				case 93: position = new Vector3(-0.08333418f, 4.124544e-05f, 0.1413952f); rotation = new Quaternion(-0.7055192f, -0.001155261f, 0.7086875f, 0.001804468f); scale = new Vector3(1f, 1f, 0.9999999f); break; //NoseMiddle
				case 94: position = new Vector3(-1.368144e-08f, -8.276402e-09f, -2.588541e-08f); rotation = new Quaternion(-0.0001302077f, 0.0004997691f, -0.0009101607f, 0.9999995f); scale = new Vector3(1f, 0.9999999f, 1f); break; //NoseMiddleAdjust
				case 95: position = new Vector3(-0.1149874f, -8.635249e-06f, 0.11935f); rotation = new Quaternion(0.7095862f, 0.006209562f, -0.7045734f, 0.0050349f); scale = new Vector3(0.9999997f, 0.9999999f, 0.9999998f); break; //NoseTop
				case 96: position = new Vector3(9.871454e-08f, -3.395144e-09f, 8.663432e-08f); rotation = new Quaternion(3.415419e-05f, 3.701826e-08f, 7.421478e-10f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //NoseTopAdjust
				case 97: position = new Vector3(-0.0735753f, 0.03163495f, 0.1118161f); rotation = new Quaternion(0.6685912f, -0.241927f, -0.6604242f, 0.241448f); scale = new Vector3(1f, 1f, 1f); break; //RightCheek
				case 98: position = new Vector3(4.435424e-08f, -1.024455e-08f, -3.232126e-07f); rotation = new Quaternion(2.150191e-07f, -2.761168e-06f, -6.707536e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightCheekAdjust
				case 99: position = new Vector3(-0.07184263f, 0.07856906f, 0.04034138f); rotation = new Quaternion(0.1366626f, -0.5509138f, -0.1176813f, 0.8148426f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightEar
				case 100: position = new Vector3(-1.532026e-07f, -7.21775e-09f, 1.599547e-07f); rotation = new Quaternion(2.477318e-07f, 2.542511e-07f, 8.149073e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEarAdjust
				case 101: position = new Vector3(-0.0990985f, 0.03493082f, 0.1163927f); rotation = new Quaternion(0.7124566f, 3.100918e-06f, -0.7017162f, 4.016433e-06f); scale = new Vector3(1f, 1f, 1f); break; //RightEye
				case 102: position = new Vector3(1.063466e-07f, 7.381744e-08f, -9.936199e-07f); rotation = new Quaternion(-2.138464e-06f, 3.595294e-06f, 1.843402e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyeAdjust
				case 103: position = new Vector3(8.399031e-08f, 9.243664e-08f, -3.975734e-07f); rotation = new Quaternion(-4.377739e-06f, -3.742517e-06f, -1.411583e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyeGlobe
				case 104: position = new Vector3(6.163852e-08f, 4.773317e-08f, -3.97574e-07f); rotation = new Quaternion(-0.0002752239f, -0.1797691f, -1.187016e-05f, 0.9837088f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightLowerLid
				case 105: position = new Vector3(5.488209e-08f, -1.681656e-09f, -9.544237e-09f); rotation = new Quaternion(-0.0002784951f, -0.0004620694f, 3.697521e-05f, 0.9999999f); scale = new Vector3(1f, 1f, 1f); break; //RightLowerLidAdjust
				case 106: position = new Vector3(8.399031e-08f, 9.243664e-08f, -3.975734e-07f); rotation = new Quaternion(0.0003551356f, 0.1202651f, -1.506854e-05f, 0.9927418f); scale = new Vector3(1f, 1f, 1f); break; //RightUpperLid
				case 107: position = new Vector3(2.916954e-08f, -4.110916e-09f, -2.018533e-08f); rotation = new Quaternion(0.0003495334f, 0.0003604438f, 3.613241e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightUpperLidAdjust
				case 108: position = new Vector3(-0.1060048f, 0.01700856f, 0.08253039f); rotation = new Quaternion(0.762741f, -7.892403e-08f, -0.6467041f, -2.51674e-08f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //RightEyebrowLow
				case 109: position = new Vector3(1.00024e-07f, -1.863278e-09f, 1.141359e-07f); rotation = new Quaternion(-5.188788e-08f, 6.556511e-07f, -6.063924e-09f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyebrowLowAdjust
				case 110: position = new Vector3(-0.1106329f, 0.03322391f, 0.08008374f); rotation = new Quaternion(0.756873f, 3.398653e-08f, -0.653562f, 2.093258e-08f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightEyebrowMiddle
				case 111: position = new Vector3(2.416935e-09f, 4.078415e-09f, -2.177955e-08f); rotation = new Quaternion(1.686618e-07f, 1.601875e-06f, 2.032358e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyebrowMiddleAdjust
				case 112: position = new Vector3(-0.1100773f, 0.05020979f, 0.06985677f); rotation = new Quaternion(0.7581919f, 0.0004819017f, -0.6520312f, -0.0003130192f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightEyebrowUp
				case 113: position = new Vector3(-4.300819e-08f, -3.026798e-09f, 5.780294e-08f); rotation = new Quaternion(-2.608649e-07f, 8.976455e-07f, 2.080196e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyebrowUpAdjust
				case 114: position = new Vector3(-0.02797572f, 0.01024682f, 0.1459551f); rotation = new Quaternion(0.7940924f, -0.05977327f, -0.6030683f, 0.04640193f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightLipsSuperiorMiddle
				case 115: position = new Vector3(-5.483162e-08f, 2.793968e-09f, 1.105527e-07f); rotation = new Quaternion(0.0001277119f, 1.577448e-05f, -3.511086e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightLipsSuperiorMiddleAdjust
				case 116: position = new Vector3(-0.02599578f, 0.03721041f, 0.1100256f); rotation = new Quaternion(-0.6591188f, 0.239158f, 0.6702256f, -0.2432357f); scale = new Vector3(1f, 1f, 0.9999998f); break; //RightLowCheek
				case 117: position = new Vector3(-3.166497e-08f, 5.587935e-09f, -2.129786e-08f); rotation = new Quaternion(1.881445e-07f, -2.4359e-08f, -2.264133e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightLowCheekAdjust
				case 118: position = new Vector3(-0.05234226f, 0.02683925f, 0.1018849f); rotation = new Quaternion(0.7088719f, -0.001019284f, -0.7053236f, -0.004261738f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //RightNose
				case 119: position = new Vector3(1.159788e-08f, -1.800072e-08f, 1.772423e-08f); rotation = new Quaternion(0.0003646002f, -1.413865e-06f, 4.696631e-09f, 0.9999999f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightNoseAdjust
				case 120: position = new Vector3(-0.04010044f, -9.259868e-05f, 0.1175899f); rotation = new Quaternion(0.7071069f, 5.338666e-08f, -0.7071066f, 5.338668e-08f); scale = new Vector3(1f, 1f, 1f); break; //UpperLips
				case 121: position = new Vector3(2.235875e-08f, -1.0672e-11f, 1.072329e-07f); rotation = new Quaternion(2.15296e-09f, 1.522712e-06f, -9.582325e-11f, 1f); scale = new Vector3(1f, 1f, 1f); break; //UpperLipsAdjust
				case 122: position = new Vector3(-0.03506036f, 2.646989e-09f, 5.587935e-08f); rotation = new Quaternion(4.329001e-07f, 2.235172e-08f, 3.489092e-14f, 1f); scale = new Vector3(1f, 1f, 1f); break; //NeckAdjust
				case 123: position = new Vector3(-0.03042252f, 0.06953072f, 0.1136089f); rotation = new Quaternion(-0.1452049f, 0.6784655f, -0.1333828f, 0.7076787f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightOuterBreast
				case 124: position = new Vector3(-0.02736982f, 5.587935e-09f, 2.933666e-08f); rotation = new Quaternion(-0.02537196f, -0.05112286f, -0.008944045f, 0.99833f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //RightInnerBreast
				case 125: position = new Vector3(4.097819e-08f, 1.862645e-08f, -5.960464e-08f); rotation = new Quaternion(-7.636845e-08f, 1.862645e-08f, -4.889444e-09f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightInnerBreastAdjust
				case 126: position = new Vector3(-5.587935e-09f, 4.656613e-09f, 8.6613e-08f); rotation = new Quaternion(-3.559981e-07f, 9.313226e-09f, -2.847082e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightOuterBreastAdjust
				case 127: position = new Vector3(-0.1819358f, 0.05235633f, 0.003283256f); rotation = new Quaternion(-0.04116481f, -0.03522681f, -0.8212652f, 0.5679684f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightShoulder
				case 128: position = new Vector3(-0.1421482f, -7.574446e-05f, 1.796521e-05f); rotation = new Quaternion(0.0149906f, -0.03559918f, -0.1911934f, 0.9807921f); scale = new Vector3(1f, 1f, 1f); break; //RightArm
				case 129: position = new Vector3(-0.06878752f, 3.241003e-07f, -1.527369e-07f); rotation = new Quaternion(-0.2379599f, -2.910159e-07f, -2.598527e-07f, 0.971275f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightArmAdjust
				case 130: position = new Vector3(-0.2838206f, -0.0003529694f, 0.001950223f); rotation = new Quaternion(-0.04842841f, 0.1092364f, 0.02532771f, 0.9925123f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightForeArm
				case 131: position = new Vector3(-0.02799003f, -1.626322e-07f, -1.136214e-07f); rotation = new Quaternion(-1.575099e-07f, 1.396984e-08f, 4.118192e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightForeArmAdjust
				case 132: position = new Vector3(-0.1475397f, -1.132139e-07f, -7.636845e-08f); rotation = new Quaternion(-3.485475e-07f, 1.350418e-08f, 2.745655e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightForeArmTwist
				case 133: position = new Vector3(-0.02798363f, -3.328896e-07f, -3.539026e-08f); rotation = new Quaternion(8.009373e-08f, 1.350418e-08f, -3.774548e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightForeArmTwistAdjust
				case 134: position = new Vector3(-0.280726f, 0.0004453477f, 9.782799e-05f); rotation = new Quaternion(0.7203866f, 0.004555989f, -0.01648099f, 0.693362f); scale = new Vector3(0.9999998f, 0.9999999f, 0.9999999f); break; //RightHand
				case 135: position = new Vector3(-0.08573787f, -0.03069644f, 0.0004206332f); rotation = new Quaternion(0.2053065f, -0.0611782f, 0.1396822f, 0.9667447f); scale = new Vector3(0.9999998f, 0.9999999f, 0.9999999f); break; //RightHandFinger01_01
				case 136: position = new Vector3(-0.03693618f, 3.72529e-08f, 3.72529e-09f); rotation = new Quaternion(-0.014953f, -0.1447614f, 0.02117512f, 0.989127f); scale = new Vector3(0.9999999f, 0.9999998f, 1f); break; //RightHandFinger01_02
				case 137: position = new Vector3(-0.02210765f, 4.470348e-08f, 1.490116e-08f); rotation = new Quaternion(0.03146788f, -0.04711929f, -0.003357269f, 0.9983879f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightHandFinger01_03
				case 138: position = new Vector3(-0.09297277f, -0.0126206f, 0.006803986f); rotation = new Quaternion(0.1347938f, -0.07683947f, 0.09632996f, 0.9831821f); scale = new Vector3(0.9999999f, 0.9999998f, 0.9999999f); break; //RightHandFinger02_01
				case 139: position = new Vector3(-0.03892579f, -1.490116e-08f, -2.980232e-08f); rotation = new Quaternion(-0.004608932f, -0.1096316f, -0.008167295f, 0.9939281f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightHandFinger02_02
				case 140: position = new Vector3(-0.03213905f, -3.72529e-08f, 7.450581e-08f); rotation = new Quaternion(-0.009086292f, -0.06614655f, 0.01302745f, 0.9976835f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightHandFinger02_03
				case 141: position = new Vector3(-0.09300845f, 0.009580871f, 0.008817063f); rotation = new Quaternion(0.07886448f, -0.05687004f, 0.02577983f, 0.9949279f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightHandFinger03_01
				case 142: position = new Vector3(-0.04725767f, 1.490116e-08f, 7.078052e-08f); rotation = new Quaternion(-0.002872095f, -0.09684271f, 0.01627373f, 0.9951625f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger03_02
				case 143: position = new Vector3(-0.04042114f, 7.450581e-09f, 5.7742e-08f); rotation = new Quaternion(-0.01665002f, -0.01604594f, -0.0131781f, 0.9996458f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightHandFinger03_03
				case 144: position = new Vector3(-0.09163298f, 0.03711057f, 0.003349839f); rotation = new Quaternion(0.03604885f, -0.02278478f, -0.0249821f, 0.9987779f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightHandFinger04_01
				case 145: position = new Vector3(-0.05416498f, 1.862645e-08f, 8.707866e-08f); rotation = new Quaternion(-0.002677881f, -0.0798529f, 0.004389258f, 0.9967934f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger04_02
				case 146: position = new Vector3(-0.03353675f, 2.235174e-08f, -5.261973e-08f); rotation = new Quaternion(0.001482348f, -0.07270246f, 0.01461356f, 0.9972456f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger04_03
				case 147: position = new Vector3(-0.03816158f, 0.04425694f, -0.02005056f); rotation = new Quaternion(-0.5593044f, -0.08592293f, -0.432848f, 0.7017396f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightHandFinger05_01
				case 148: position = new Vector3(-0.02932127f, 1.525041e-08f, -4.842877e-08f); rotation = new Quaternion(0.04490698f, -0.03217191f, 0.02465365f, 0.9981686f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightHandFinger05_02
				case 149: position = new Vector3(-0.03140654f, -3.608875e-08f, 1.583248e-08f); rotation = new Quaternion(-0.02490251f, -0.05538729f, 0.03426434f, 0.9975661f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightHandFinger05_03
				case 150: position = new Vector3(-0.03552739f, 1.245644e-08f, -1.955777e-08f); rotation = new Quaternion(-0.1465887f, 5.407701e-08f, -2.130422e-07f, 0.9891976f); scale = new Vector3(1f, 1f, 1f); break; //RightShoulderAdjust
				case 151: position = new Vector3(-0.03261711f, -0.02943395f, -0.03281876f); rotation = new Quaternion(-0.690668f, -0.08797559f, 0.1647321f, 0.6986426f); scale = new Vector3(1f, 1f, 1f); break; //RightTrapezius
				case 152: position = new Vector3(-0.04138738f, 3.124661e-09f, 2.289311e-09f); rotation = new Quaternion(3.164379e-08f, 1.678777e-08f, -2.808998e-14f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //Spine1Adjust
				case 153: position = new Vector3(-0.03617224f, 2.773968e-09f, -9.341165e-05f); rotation = new Quaternion(-2.704098e-08f, -1.490116e-08f, -4.129601e-14f, 1f); scale = new Vector3(1f, 1f, 1f); break; //SpineAdjust
				case 154: position = new Vector3(0.01727388f, 0.11026f, 0.002575199f); rotation = new Quaternion(-0.05604125f, 0.07568738f, 0.9947098f, -0.04102697f); scale = new Vector3(1f, 1.000007f, 1.00001f); break; //RightUpLeg
				case 155: position = new Vector3(-1.008812e-07f, -2.793968e-09f, -3.870809e-08f); rotation = new Quaternion(-0.06176885f, -0.8543704f, 0.01431938f, 0.5157819f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightGluteus
				case 156: position = new Vector3(-0.4368429f, 3.748573e-08f, -2.386514e-09f); rotation = new Quaternion(0.04350514f, -0.02888321f, 0.03022131f, 0.9981782f); scale = new Vector3(1f, 0.9999999f, 0.9999998f); break; //RightLeg
				case 157: position = new Vector3(-0.4971314f, 7.357448e-08f, -6.053597e-09f); rotation = new Quaternion(0.8610122f, 0.01867051f, -0.502549f, -0.07585528f); scale = new Vector3(1f, 0.9999998f, 1f); break; //RightFoot
				case 158: position = new Vector3(-0.1806838f, 6.286427e-08f, 9.313226e-09f); rotation = new Quaternion(0.06246535f, -0.2067475f, 0.02363342f, 0.9761122f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightToeBase
				case 159: position = new Vector3(-0.1242825f, 6.984919e-08f, -2.235174e-08f); rotation = new Quaternion(8.090865e-08f, -8.381903e-09f, 1.164153e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightLegAdjust
				case 160: position = new Vector3(-0.1092115f, 5.331822e-08f, -2.054148e-07f); rotation = new Quaternion(0.03335006f, -8.852489e-09f, -1.726818e-08f, 0.9994438f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightUpLegAdjust

				default: position = Vector3.zero; rotation = Quaternion.identity; scale = Vector3.one; break;
			}
			transform.localPosition = position;
			transform.localRotation = rotation;
			transform.localScale = scale;
		}

		#endregion BindPose

		#region MasculineFemale
		private static void MasculineFemaleLocalTranslation(int index, Transform transform)
		{
			Vector3 position; Quaternion rotation; Vector3 scale;

			switch (index)
			{
				case 0: position = new Vector3(-2.846697e-07f, 1.058908f, -0.02514136f); rotation = new Quaternion(0.0390729f, 0.03907251f, -0.7060263f, 0.7060266f); scale = new Vector3(1.000001f, 1.000001f, 1f); break; //Hips
				case 1: position = new Vector3(0.01667708f, -0.1110136f, 0.006990808f); rotation = new Quaternion(-0.05640528f, -0.0871884f, 0.9937741f, 0.04037121f); scale = new Vector3(0.9889368f, 0.9889438f, 0.9889538f); break; //LeftUpLeg
				case 2: position = new Vector3(0.02272917f, 0.01146722f, -0.02099032f); rotation = new Quaternion(0.6402189f, -0.01229505f, 0.7646895f, -0.07223904f); scale = new Vector3(0.727194f, 0.9741541f, 0.9692709f); break; //LeftGluteus
				case 3: position = new Vector3(-0.44173f, -3.001187e-07f, 2.542511e-07f); rotation = new Quaternion(-0.021962f, -0.02890434f, -0.03019587f, 0.9988846f); scale = new Vector3(0.9687354f, 0.9687353f, 0.9687352f); break; //LeftLeg
				case 4: position = new Vector3(-0.5208459f, 0.002261421f, -0.008169513f); rotation = new Quaternion(0.8585603f, -0.01113608f, -0.5031602f, 0.09787761f); scale = new Vector3(1.072011f, 1.075886f, 1.043083f); break; //LeftFoot
				case 5: position = new Vector3(-0.1767465f, 3.112946e-07f, 5.779788e-06f); rotation = new Quaternion(-0.05929935f, -0.2054773f, -0.03158632f, 0.9763529f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftToeBase
				case 6: position = new Vector3(-0.1379873f, -0.009514811f, -0.001625451f); rotation = new Quaternion(-0.01121611f, -0.004265329f, -0.006370736f, 0.9999077f); scale = new Vector3(1.019955f, 1.159788f, 1.174806f); break; //LeftLegAdjust
				case 7: position = new Vector3(-0.1114385f, -0.003985205f, -0.001168436f); rotation = new Quaternion(-0.01930385f, -0.01404301f, -0.006626708f, 0.9996931f); scale = new Vector3(0.9645034f, 1.109929f, 1.13632f); break; //LeftUpLegAdjust
				case 8: position = new Vector3(-0.05387862f, 4.067737e-09f, 3.725294e-09f); rotation = new Quaternion(5.679579e-08f, -0.07920223f, 3.527967e-08f, 0.9968586f); scale = new Vector3(1.052621f, 1.052621f, 1.052621f); break; //LowerBack
				case 9: position = new Vector3(-0.04922519f, 1.08275e-08f, 0.01481906f); rotation = new Quaternion(2.394799e-08f, 0.01441482f, -1.208363e-09f, 0.9998961f); scale = new Vector3(1.041756f, 0.9993726f, 1.189234f); break; //LowerBackAdjust
				case 10: position = new Vector3(-0.03519755f, 5.550585e-09f, 0.01406183f); rotation = new Quaternion(6.362588e-08f, 0.06053469f, 1.727907e-08f, 0.9981661f); scale = new Vector3(0.9135285f, 1.238478f, 0.715107f); break; //LowerBackBelly
				case 11: position = new Vector3(-0.174404f, 1.050973e-08f, -0.0001134668f); rotation = new Quaternion(-5.439265e-07f, -0.08683421f, 1.428569e-08f, 0.9962228f); scale = new Vector3(1.036588f, 1.036588f, 1.036588f); break; //Spine
				case 12: position = new Vector3(-0.1691506f, 1.426286e-08f, -0.001764618f); rotation = new Quaternion(4.740344e-07f, 0.1107159f, -5.385501e-10f, 0.9938521f); scale = new Vector3(0.9306153f, 0.9306152f, 0.9306154f); break; //Spine1
				case 13: position = new Vector3(-0.02193857f, -0.0757897f, 0.1081132f); rotation = new Quaternion(0.1469383f, 0.6428149f, 0.1455775f, 0.7375672f); scale = new Vector3(0.3856082f, 1.097818f, 1.270819f); break; //LeftOuterBreast
				case 14: position = new Vector3(-0.2049582f, 0.04001472f, 0.01422096f); rotation = new Quaternion(-0.01610614f, -0.002123198f, 0.01010945f, 0.999817f); scale = new Vector3(0.7945859f, 0.8309759f, 0.7348408f); break; //LeftInnerBreast
				case 15: position = new Vector3(2.980232e-08f, -7.450581e-09f, 3.810419e-08f); rotation = new Quaternion(9.8953e-10f, -8.090865e-09f, -2.956949e-08f, 1f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftInnerBreastAdjust
				case 16: position = new Vector3(5.215406e-08f, 2.793968e-09f, -2.095476e-09f); rotation = new Quaternion(1.173467e-07f, 2.735761e-08f, 2.916204e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftOuterBreastAdjust
				case 17: position = new Vector3(-0.1722403f, -0.05202853f, 0.004816214f); rotation = new Quaternion(0.04498522f, -0.02975989f, 0.821438f, 0.5677415f); scale = new Vector3(1.138851f, 1.138851f, 1.138851f); break; //LeftShoulder
				case 18: position = new Vector3(-0.1227644f, -8.500917e-05f, 1.312792e-05f); rotation = new Quaternion(0.008696645f, -0.02748674f, 0.1921951f, 0.9809332f); scale = new Vector3(0.8991768f, 0.8991346f, 0.8991346f); break; //LeftArm
				case 19: position = new Vector3(-0.06991178f, 0.0002714845f, 0.007745858f); rotation = new Quaternion(0.05768595f, -0.01659649f, 0.004332666f, 0.9981874f); scale = new Vector3(0.9705108f, 1.184445f, 1.235866f); break; //LeftArmAdjust
				case 20: position = new Vector3(-0.2641163f, 1.143198e-06f, 6.631017e-07f); rotation = new Quaternion(0.03062795f, 0.1073075f, -0.03016624f, 0.9932961f); scale = new Vector3(0.9159915f, 0.9444525f, 0.9444524f); break; //LeftForeArm
				case 21: position = new Vector3(-0.03214792f, 0.0008034543f, 0.001881566f); rotation = new Quaternion(2.232194e-07f, 1.930745e-08f, -2.256564e-07f, 1f); scale = new Vector3(1.061199f, 1.19743f, 1.174267f); break; //LeftForeArmAdjust
				case 22: position = new Vector3(-0.1499469f, 2.151355e-07f, -4.656613e-08f); rotation = new Quaternion(4.656613e-09f, 4.656605e-10f, -1.646113e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftForeArmTwist
				case 23: position = new Vector3(-0.03944023f, 0.002140049f, -0.0001319405f); rotation = new Quaternion(7.325462e-08f, -1.047736e-08f, 2.535526e-07f, 1f); scale = new Vector3(0.9999999f, 1.137858f, 1.061686f); break; //LeftForeArmTwistAdjust
				case 24: position = new Vector3(-0.3015364f, 0.00147482f, 0.0004148651f); rotation = new Quaternion(-0.7405879f, 0.009598641f, 0.01689095f, 0.6716785f); scale = new Vector3(1.091871f, 1.091871f, 1.091871f); break; //LeftHand
				case 25: position = new Vector3(-0.0865436f, 0.02997063f, -0.004861064f); rotation = new Quaternion(-0.1417513f, -0.04926161f, -0.1481943f, 0.9775062f); scale = new Vector3(1.028362f, 1.028362f, 1.028362f); break; //LeftHandFinger01_01
				case 26: position = new Vector3(-0.03186322f, 0f, -5.215406e-08f); rotation = new Quaternion(0.01071072f, -0.1463589f, 0.01475273f, 0.9890636f); scale = new Vector3(1.000176f, 1.000176f, 1.000176f); break; //LeftHandFinger01_02
				case 27: position = new Vector3(-0.01907145f, 0f, 1.862645e-08f); rotation = new Quaternion(0.01442601f, -0.04638769f, -0.004240946f, 0.9988104f); scale = new Vector3(1.000204f, 1.000204f, 1.000205f); break; //LeftHandFinger01_03
				case 28: position = new Vector3(-0.09005131f, 0.01246238f, 0.005587657f); rotation = new Quaternion(-0.1282919f, -0.07860928f, -0.08554392f, 0.9849082f); scale = new Vector3(0.9943995f, 0.9943992f, 0.9943994f); break; //LeftHandFinger02_01
				case 29: position = new Vector3(-0.03838415f, 4.470348e-08f, -1.117587e-07f); rotation = new Quaternion(0.01291339f, -0.1053193f, -0.01559799f, 0.9942323f); scale = new Vector3(0.9998523f, 0.9998525f, 0.9998525f); break; //LeftHandFinger02_02
				case 30: position = new Vector3(-0.03169183f, 2.235174e-08f, 9.313226e-08f); rotation = new Quaternion(0.04728564f, -0.06059604f, 0.005238486f, 0.997028f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger02_03
				case 31: position = new Vector3(-0.09417438f, -0.01178299f, 0.009794239f); rotation = new Quaternion(-0.02601908f, -0.08666971f, -0.05351731f, 0.9944583f); scale = new Vector3(1.048254f, 1.048254f, 1.048254f); break; //LeftHandFinger03_01
				case 32: position = new Vector3(-0.044654f, 0f, -1.769513e-08f); rotation = new Quaternion(-0.02352907f, -0.09909908f, 0.008658403f, 0.9947617f); scale = new Vector3(0.9429798f, 0.9429798f, 0.9429799f); break; //LeftHandFinger03_02
				case 33: position = new Vector3(-0.04047918f, -7.450581e-09f, 3.259629e-08f); rotation = new Quaternion(0.0292228f, -0.0103846f, 0.008469327f, 0.9994831f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftHandFinger03_03
				case 34: position = new Vector3(-0.09299804f, -0.03392569f, -0.0002732044f); rotation = new Quaternion(0.07530575f, -0.0206867f, 0.02235606f, 0.9966952f); scale = new Vector3(1.03907f, 1.03907f, 1.03907f); break; //LeftHandFinger04_01
				case 35: position = new Vector3(-0.04944691f, 1.862645e-08f, -5.681068e-08f); rotation = new Quaternion(-0.001837477f, -0.07886351f, 0.01541563f, 0.9967645f); scale = new Vector3(0.9706062f, 0.9706063f, 0.9706063f); break; //LeftHandFinger04_02
				case 36: position = new Vector3(-0.03154153f, 0f, 1.452863e-07f); rotation = new Quaternion(0.03192984f, -0.06776746f, 0.006441969f, 0.9971693f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger04_03
				case 37: position = new Vector3(-0.0370079f, -0.03903358f, -0.01876462f); rotation = new Quaternion(0.6142228f, -0.05370131f, 0.4295118f, 0.6598228f); scale = new Vector3(1.001943f, 1.001943f, 1.001943f); break; //LeftHandFinger05_01
				case 38: position = new Vector3(-0.02941717f, -4.65661e-10f, 3.166497e-08f); rotation = new Quaternion(-0.004367676f, -0.03808264f, -0.01369601f, 0.9991712f); scale = new Vector3(1.10048f, 1.10048f, 1.100481f); break; //LeftHandFinger05_02
				case 39: position = new Vector3(-0.02863678f, 6.61239e-08f, -8.381903e-08f); rotation = new Quaternion(0.01651916f, -0.06393301f, -0.01514903f, 0.9977025f); scale = new Vector3(0.895111f, 0.8951111f, 0.8951109f); break; //LeftHandFinger05_03
				case 40: position = new Vector3(-0.0183631f, -0.003484444f, 0.009763553f); rotation = new Quaternion(0.1491432f, 2.461838e-07f, 2.002454e-06f, 0.9888156f); scale = new Vector3(1.003469f, 0.9282638f, 1.084446f); break; //LeftShoulderAdjust
				case 41: position = new Vector3(-0.03316488f, 0.03210037f, -0.02310953f); rotation = new Quaternion(0.7066576f, 0.00244602f, -0.07185738f, 0.7038932f); scale = new Vector3(0.7991538f, 0.7981593f, 1.118487f); break; //LeftTrapezius
				case 42: position = new Vector3(-0.1861733f, 1.963939e-08f, 0.005971134f); rotation = new Quaternion(-3.061654e-07f, 0.09072293f, -2.622888e-08f, 0.9958762f); scale = new Vector3(1f, 1f, 1f); break; //Neck
				case 43: position = new Vector3(-0.09690081f, 7.763199e-09f, -0.001946066f); rotation = new Quaternion(7.059523e-08f, -0.08316047f, 1.76383e-08f, 0.9965362f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //Head
				case 44: position = new Vector3(-0.0574149f, 4.369404e-09f, 0.002351737f); rotation = new Quaternion(1.133697e-10f, 4.66974e-10f, 1.057908e-10f, 1f); scale = new Vector3(0.9972056f, 1f, 0.983569f); break; //HeadAdjust
				case 45: position = new Vector3(0.02103109f, -1.539067e-07f, 0.07043059f); rotation = new Quaternion(0.0002322808f, 0.7071077f, -0.0002322102f, 0.7071058f); scale = new Vector3(1.014366f, 1.032738f, 1.051974f); break; //InnerMouthUpperAdjust
				case 46: position = new Vector3(0.03879738f, -4.176541e-09f, 0.006161639f); rotation = new Quaternion(6.384386e-15f, 2.727612e-08f, 1.408779e-09f, 1f); scale = new Vector3(1.024328f, 1.062555f, 1.099637f); break; //UpperNeckAdjust
				case 47: position = new Vector3(-0.0780457f, -0.03129971f, 0.1106585f); rotation = new Quaternion(0.6689441f, 0.2419002f, -0.6600444f, -0.2415359f); scale = new Vector3(0.9018037f, 0.9018035f, 0.9018035f); break; //LeftCheek
				case 48: position = new Vector3(1.145527e-07f, -1.303852e-08f, 1.296576e-08f); rotation = new Quaternion(-3.868543e-07f, 2.11066e-08f, 1.052579e-07f, 1f); scale = new Vector3(1f, 1.049035f, 0.9999999f); break; //LeftCheekAdjust
				case 49: position = new Vector3(-0.07099877f, -0.07883729f, 0.03942096f); rotation = new Quaternion(-0.1418404f, -0.5394924f, 0.1070928f, 0.8230191f); scale = new Vector3(0.9878622f, 0.9878622f, 0.9878622f); break; //LeftEar
				case 50: position = new Vector3(-0.001608327f, -0.0002166552f, 0.001346582f); rotation = new Quaternion(0.007689404f, 0.04023706f, 0.07968822f, 0.9959778f); scale = new Vector3(1.046445f, 1.352299f, 1.06648f); break; //LeftEarAdjust
				case 51: position = new Vector3(-0.09813529f, -0.0345047f, 0.1145755f); rotation = new Quaternion(0.7124541f, 3.939824e-07f, -0.7017187f, -3.695646e-07f); scale = new Vector3(1f, 1f, 1f); break; //LeftEye
				case 52: position = new Vector3(8.753975e-08f, -3.212073e-08f, -1.227251e-07f); rotation = new Quaternion(2.957463e-08f, -4.330652e-08f, 1.919279e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeAdjust
				case 53: position = new Vector3(8.753975e-08f, -3.212073e-08f, -1.227251e-07f); rotation = new Quaternion(2.957464e-08f, -4.330651e-08f, 5.254993e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeGlobe
				case 54: position = new Vector3(8.753975e-08f, -3.212073e-08f, -1.227251e-07f); rotation = new Quaternion(0.001177739f, -0.1789785f, -4.841698e-07f, 0.9838523f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftLowerLid
				case 55: position = new Vector3(-4.515186e-08f, 1.882654e-09f, 1.148801e-07f); rotation = new Quaternion(0.001155805f, 0.0003536972f, -0.0002105075f, 0.9999993f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftLowerLidAdjust
				case 56: position = new Vector3(8.753975e-08f, -3.212073e-08f, -1.227251e-07f); rotation = new Quaternion(0.000368452f, 0.1218889f, 1.109482e-05f, 0.9925437f); scale = new Vector3(0.970511f, 0.9705109f, 0.970511f); break; //LeftUpperLid
				case 57: position = new Vector3(6.895789e-09f, -8.4583e-10f, 4.152025e-08f); rotation = new Quaternion(-0.001250552f, -9.121953e-05f, -0.0001901483f, 0.9999992f); scale = new Vector3(1f, 1f, 1f); break; //LeftUpperLidAdjust
				case 58: position = new Vector3(-0.1070219f, -0.01558768f, 0.08294234f); rotation = new Quaternion(0.7627301f, -0.000732962f, -0.646716f, -0.0008643796f); scale = new Vector3(0.9807073f, 0.9807074f, 0.9807073f); break; //LeftEyebrowLow
				case 59: position = new Vector3(-3.850536e-08f, -3.76531e-10f, 6.68515e-08f); rotation = new Quaternion(-0.001204582f, -0.0002008687f, -0.0007789172f, 0.999999f); scale = new Vector3(1f, 0.9999999f, 0.9287882f); break; //LeftEyebrowLowAdjust
				case 60: position = new Vector3(-0.1111191f, -0.02982208f, 0.08106004f); rotation = new Quaternion(0.7568955f, -0.001771958f, -0.6535303f, -0.002052118f); scale = new Vector3(0.9906586f, 0.9906591f, 0.9906592f); break; //LeftEyebrowMiddle
				case 61: position = new Vector3(5.111856e-08f, -6.4756e-10f, -6.089977e-08f); rotation = new Quaternion(-0.002645191f, -0.0008325811f, 0.0005660658f, 0.999996f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftEyebrowMiddleAdjust
				case 62: position = new Vector3(-0.1085083f, -0.04728955f, 0.0713907f); rotation = new Quaternion(0.7581962f, -0.0008158847f, -0.652026f, 3.193659e-05f); scale = new Vector3(0.9884933f, 0.9884934f, 0.9884935f); break; //LeftEyebrowUp
				case 63: position = new Vector3(5.321635e-08f, -2.815796e-09f, -9.006908e-08f); rotation = new Quaternion(-0.0005620896f, -0.001434197f, 0.0002555657f, 0.9999988f); scale = new Vector3(0.9999999f, 0.9999998f, 0.9999998f); break; //LeftEyebrowUpAdjust
				case 64: position = new Vector3(-0.02714531f, -0.008342996f, 0.1455809f); rotation = new Quaternion(0.7494496f, 0.1137962f, -0.6484532f, -0.06988721f); scale = new Vector3(1.275559f, 1.275559f, 1.275559f); break; //LeftLipsSuperiorMiddle
				case 65: position = new Vector3(9.460072e-05f, -5.287584e-06f, -4.373127e-05f); rotation = new Quaternion(0.6634001f, -1.826033e-06f, -4.289592e-06f, 0.7482648f); scale = new Vector3(0.6460699f, 0.7980126f, 1.028382f); break; //LeftLipsSuperiorMiddleAdjust
				case 66: position = new Vector3(-0.03012329f, -0.03783859f, 0.1097343f); rotation = new Quaternion(-0.6591192f, -0.2391572f, 0.6702254f, 0.2432358f); scale = new Vector3(1f, 1f, 1f); break; //LeftLowCheek
				case 67: position = new Vector3(-9.313226e-09f, 0f, 9.722999e-08f); rotation = new Quaternion(-4.311478e-07f, 1.441367e-07f, 8.23057e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftLowCheekAdjust
				case 68: position = new Vector3(-0.05648642f, -0.02229144f, 0.09510447f); rotation = new Quaternion(0.708872f, 0.001042196f, -0.7053234f, 0.004285209f); scale = new Vector3(1f, 1f, 1f); break; //LeftNose
				case 69: position = new Vector3(5.734182e-08f, 1.200533e-08f, -2.251763e-07f); rotation = new Quaternion(3.470832e-06f, 1.095559e-07f, 2.888175e-09f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999998f); break; //LeftNoseAdjust
				case 70: position = new Vector3(-0.02821436f, -9.102943e-05f, 0.1459801f); rotation = new Quaternion(0.736658f, -1.029088e-08f, -0.6762655f, 6.767394e-08f); scale = new Vector3(1.307185f, 1.307185f, 1.307184f); break; //LipsSuperior
				case 71: position = new Vector3(2.979858e-08f, 5.10959e-10f, 0.0002632868f); rotation = new Quaternion(-3.160779e-07f, 7.627134e-06f, 9.090542e-09f, 1f); scale = new Vector3(0.8044375f, 0.9999999f, 0.7174739f); break; //LipsSuperiorAdjust
				case 72: position = new Vector3(-0.04585495f, -9.103015e-05f, 0.04503627f); rotation = new Quaternion(-0.4972368f, 4.95439e-08f, 0.8676149f, -4.918982e-08f); scale = new Vector3(0.9229567f, 0.9229567f, 0.9229567f); break; //Mandible
				case 73: position = new Vector3(-0.03954179f, -9.082983e-05f, -0.02162966f); rotation = new Quaternion(0.9650966f, 3.217841e-06f, -0.2618941f, 1.230172e-05f); scale = new Vector3(1.207566f, 1.186974f, 1.06913f); break; //InnerMouthLowerAdjust
				case 74: position = new Vector3(-0.0839424f, 0.02631745f, 0.02488147f); rotation = new Quaternion(-0.02269288f, 0.2682574f, -0.0811802f, 0.9596524f); scale = new Vector3(0.9365085f, 0.9365084f, 0.9365085f); break; //LeftLips
				case 75: position = new Vector3(7.172513e-09f, -2.698272e-09f, 2.727172e-09f); rotation = new Quaternion(1.315932e-07f, 2.980232e-08f, 1.490116e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsAdjust
				case 76: position = new Vector3(-0.1050873f, 0.00990143f, 0.02910359f); rotation = new Quaternion(-0.01277853f, 0.1766182f, -0.08500175f, 0.980519f); scale = new Vector3(1.380224f, 1.380224f, 1.380224f); break; //LeftLipsInferior
				case 77: position = new Vector3(3.608875e-08f, -7.450581e-09f, -1.273584e-07f); rotation = new Quaternion(-0.0002782434f, -4.385498e-06f, 1.877783e-07f, 1f); scale = new Vector3(0.6935698f, 0.9999999f, 0.7490043f); break; //LeftLipsInferiorAdjust
				case 78: position = new Vector3(-0.0456085f, 0.06035602f, -0.01447152f); rotation = new Quaternion(0.0294226f, 0.1057043f, 0.2273329f, 0.967616f); scale = new Vector3(1.091121f, 1.091121f, 1.091121f); break; //LeftLowMaxilar
				case 79: position = new Vector3(0.002364181f, -0.0001489446f, 8.515781e-08f); rotation = new Quaternion(-1.215376e-07f, -6.056845e-08f, 3.418949e-08f, 1f); scale = new Vector3(0.7899607f, 1f, 1f); break; //LeftLowMaxilarAdjust
				case 80: position = new Vector3(-0.105571f, -0.0001925605f, 0.03014802f); rotation = new Quaternion(0.005549725f, 0.1731371f, -0.0008596308f, 0.9848817f); scale = new Vector3(1.301443f, 1.301443f, 1.301443f); break; //LipsInferior
				case 81: position = new Vector3(-9.036773e-09f, -7.2032e-10f, 1.324e-07f); rotation = new Quaternion(-0.005615263f, 7.969782e-06f, 5.062315e-08f, 0.9999843f); scale = new Vector3(0.8825451f, 0.9999999f, 0.8397934f); break; //LipsInferiorAdjust
				case 82: position = new Vector3(1.562316e-07f, -3.537e-12f, -1.74965e-07f); rotation = new Quaternion(-6.392425e-08f, 2.868401e-07f, 4.883465e-10f, 1f); scale = new Vector3(0.987038f, 0.9999999f, 1.177008f); break; //MandibleAdjust
				case 83: position = new Vector3(-0.08394239f, -0.02649952f, 0.02488146f); rotation = new Quaternion(0.022693f, 0.2682572f, 0.08118033f, 0.9596525f); scale = new Vector3(0.9365085f, 0.9365084f, 0.9365085f); break; //RightLips
				case 84: position = new Vector3(-9.16259e-10f, 5.02942e-09f, -2.093441e-07f); rotation = new Quaternion(-1.308768e-07f, 1.639128e-07f, -5.960463e-08f, 1f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightLipsAdjust
				case 85: position = new Vector3(-0.1050873f, -0.01008348f, 0.02910359f); rotation = new Quaternion(0.01277821f, 0.176618f, 0.08500192f, 0.980519f); scale = new Vector3(1.380224f, 1.380224f, 1.380224f); break; //RightLipsInferior
				case 86: position = new Vector3(-7.683411e-09f, 1.862645e-09f, -4.105823e-08f); rotation = new Quaternion(0.0002795838f, -4.486218e-06f, -7.595649e-08f, 1f); scale = new Vector3(0.6935698f, 0.9999999f, 0.7490044f); break; //RightLipsInferiorAdjust
				case 87: position = new Vector3(-0.04560856f, -0.06053808f, -0.01447149f); rotation = new Quaternion(-0.02942248f, 0.1057042f, -0.2273331f, 0.967616f); scale = new Vector3(1.091121f, 1.091121f, 1.091121f); break; //RightLowMaxilar
				case 88: position = new Vector3(0.002364282f, 0.0001489641f, -9.546056e-09f); rotation = new Quaternion(3.715976e-07f, 2.151583e-08f, 6.837897e-08f, 1f); scale = new Vector3(0.7899607f, 1f, 1f); break; //RightLowMaxilarAdjust
				case 89: position = new Vector3(-0.0342917f, -9.918324e-05f, -0.01934158f); rotation = new Quaternion(1.767927e-06f, 0.2488666f, -8.207751e-07f, 0.9685378f); scale = new Vector3(1.090866f, 1.090866f, 1.090866f); break; //Tongue01
				case 90: position = new Vector3(-0.02759464f, 2.082889e-09f, 8.16714e-10f); rotation = new Quaternion(0.998992f, 7.397148e-08f, -0.04489001f, 7.172107e-06f); scale = new Vector3(1f, 0.9999999f, 1f); break; //Tongue02
				case 91: position = new Vector3(-0.05873431f, -9.102163e-05f, 0.1463085f); rotation = new Quaternion(-0.689188f, 1.051567e-08f, 0.7245826f, -4.517344e-08f); scale = new Vector3(1.095513f, 1.095513f, 1.060976f); break; //NoseBase
				case 92: position = new Vector3(-0.0009008329f, -1.960741e-08f, 1.252787e-07f); rotation = new Quaternion(-3.148613e-08f, 3.935338e-09f, 5.191054e-10f, 1f); scale = new Vector3(0.9466268f, 0.9652827f, 0.9285398f); break; //NoseBaseAdjust
				case 93: position = new Vector3(-0.08142406f, 3.688707e-05f, 0.1395358f); rotation = new Quaternion(-0.7055458f, -0.001463851f, 0.7086594f, 0.002193328f); scale = new Vector3(1.118672f, 1.118672f, 1.118672f); break; //NoseMiddle
				case 94: position = new Vector3(-6.413866e-08f, -1.063017e-08f, 8.867573e-09f); rotation = new Quaternion(-4.014888e-05f, 0.000557386f, 0.0001170802f, 0.9999999f); scale = new Vector3(0.9999999f, 1f, 1f); break; //NoseMiddleAdjust
				case 95: position = new Vector3(-0.1188535f, -0.000154785f, 0.1152329f); rotation = new Quaternion(0.70983f, 0.005499175f, -0.7043285f, 0.005708798f); scale = new Vector3(1f, 1f, 1f); break; //NoseTop
				case 96: position = new Vector3(3.190316e-08f, -1.509034e-08f, -0.00243892f); rotation = new Quaternion(4.952792e-08f, 6.353086e-08f, 1.0265e-09f, 1f); scale = new Vector3(0.8257637f, 0.8257637f, 0.5697101f); break; //NoseTopAdjust
				case 97: position = new Vector3(-0.0780457f, 0.03129977f, 0.1106585f); rotation = new Quaternion(0.6689442f, -0.2418998f, -0.6600446f, 0.2415352f); scale = new Vector3(0.901804f, 0.9018034f, 0.9018035f); break; //RightCheek
				case 98: position = new Vector3(4.969115e-08f, 3.352761e-08f, 1.145236e-08f); rotation = new Quaternion(1.754494e-07f, 2.463005e-08f, 1.329828e-07f, 1f); scale = new Vector3(1f, 1.049036f, 1f); break; //RightCheekAdjust
				case 99: position = new Vector3(-0.07099875f, 0.07883732f, 0.03942095f); rotation = new Quaternion(0.1418401f, -0.5394925f, -0.1070927f, 0.8230191f); scale = new Vector3(0.9878622f, 0.9878623f, 0.9878622f); break; //RightEar
				case 100: position = new Vector3(-0.001608363f, 0.0002165872f, 0.001346358f); rotation = new Quaternion(-0.007689414f, 0.04023714f, -0.07968827f, 0.9959778f); scale = new Vector3(1.046445f, 1.352299f, 1.06648f); break; //RightEarAdjust
				case 101: position = new Vector3(-0.09813528f, 0.03450473f, 0.1145755f); rotation = new Quaternion(0.7124541f, -1.437862e-08f, -0.7017187f, -1.248242e-07f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightEye
				case 102: position = new Vector3(2.506898e-08f, 1.756045e-08f, -1.151685e-07f); rotation = new Quaternion(-6.850654e-08f, -7.916224e-09f, 1.920079e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyeAdjust
				case 103: position = new Vector3(1.76184e-08f, 1.756045e-08f, -1.151685e-07f); rotation = new Quaternion(-6.850654e-08f, -7.916225e-09f, 1.920078e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyeGlobe
				case 104: position = new Vector3(1.76184e-08f, 1.756045e-08f, -1.151685e-07f); rotation = new Quaternion(-0.001177439f, -0.1789784f, 8.093681e-08f, 0.9838523f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightLowerLid
				case 105: position = new Vector3(-7.505969e-08f, -1.822627e-09f, 1.307926e-07f); rotation = new Quaternion(-0.001155926f, 0.0003536687f, 0.0002109269f, 0.9999993f); scale = new Vector3(0.9999999f, 0.9999998f, 0.9999999f); break; //RightLowerLidAdjust
				case 106: position = new Vector3(2.506898e-08f, 1.756045e-08f, -1.151685e-07f); rotation = new Quaternion(-0.0003686323f, 0.1218891f, -1.129598e-05f, 0.9925437f); scale = new Vector3(0.9705111f, 0.9705109f, 0.970511f); break; //RightUpperLid
				case 107: position = new Vector3(1.559192e-08f, 3.597961e-09f, -1.043682e-07f); rotation = new Quaternion(0.001250403f, -9.121145e-05f, 0.0001900537f, 0.9999992f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightUpperLidAdjust
				case 108: position = new Vector3(-0.1070219f, 0.01558773f, 0.0829424f); rotation = new Quaternion(0.7627301f, 0.0007327966f, -0.6467161f, 0.0008645533f); scale = new Vector3(0.9807075f, 0.9807074f, 0.9807074f); break; //RightEyebrowLow
				case 109: position = new Vector3(3.922252e-09f, -1.392436e-09f, 6.842311e-08f); rotation = new Quaternion(0.001204581f, -0.0002008316f, 0.000778921f, 0.999999f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9287882f); break; //RightEyebrowLowAdjust
				case 110: position = new Vector3(-0.1111191f, 0.02982212f, 0.08106001f); rotation = new Quaternion(0.7568954f, 0.001771676f, -0.6535304f, 0.002052181f); scale = new Vector3(0.990659f, 0.9906591f, 0.9906592f); break; //RightEyebrowMiddle
				case 111: position = new Vector3(-5.486515e-08f, 2.41926e-10f, 5.250331e-08f); rotation = new Quaternion(0.002645805f, -0.0008325297f, -0.0005660788f, 0.999996f); scale = new Vector3(1f, 1f, 1f); break; //RightEyebrowMiddleAdjust
				case 112: position = new Vector3(-0.1085083f, 0.0472896f, 0.07139069f); rotation = new Quaternion(0.7581961f, 0.0008156773f, -0.6520261f, -3.195652e-05f); scale = new Vector3(0.9884935f, 0.9884934f, 0.9884934f); break; //RightEyebrowUp
				case 113: position = new Vector3(-1.179069e-07f, 5.918992e-09f, -1.017324e-07f); rotation = new Quaternion(0.0005620497f, -0.001434115f, -0.0002554669f, 0.9999988f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightEyebrowUpAdjust
				case 114: position = new Vector3(-0.02714531f, 0.008343042f, 0.1455809f); rotation = new Quaternion(0.7494496f, -0.1137964f, -0.6484532f, 0.0698875f); scale = new Vector3(1.275559f, 1.275559f, 1.275559f); break; //RightLipsSuperiorMiddle
				case 115: position = new Vector3(9.465171e-05f, 5.280133e-06f, -4.385057e-05f); rotation = new Quaternion(-0.6633998f, -1.868108e-06f, 4.383771e-06f, 0.7482651f); scale = new Vector3(0.6460699f, 0.7980126f, 1.028382f); break; //RightLipsSuperiorMiddleAdjust
				case 116: position = new Vector3(-0.03012328f, 0.03783858f, 0.1097343f); rotation = new Quaternion(-0.6591191f, 0.2391574f, 0.6702253f, -0.2432359f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightLowCheek
				case 117: position = new Vector3(-3.72529e-08f, -7.450581e-09f, -1.498099e-07f); rotation = new Quaternion(1.974477e-07f, 1.168773e-07f, -3.176101e-07f, 1f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightLowCheekAdjust
				case 118: position = new Vector3(-0.05649345f, 0.02230488f, 0.09510444f); rotation = new Quaternion(0.7088687f, -0.001544186f, -0.7053225f, -0.004789365f); scale = new Vector3(0.9999996f, 1f, 1f); break; //RightNose
				case 119: position = new Vector3(2.022716e-09f, -1.17725e-08f, -2.048037e-07f); rotation = new Quaternion(1.267167e-05f, 1.125227e-07f, 4.171335e-09f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightNoseAdjust
				case 120: position = new Vector3(-0.04053232f, -9.102896e-05f, 0.1142123f); rotation = new Quaternion(0.7071076f, 0.0005282959f, -0.7071056f, 0.0005284572f); scale = new Vector3(1f, 1f, 1f); break; //UpperLips
				case 121: position = new Vector3(1.30454e-08f, 4.996e-12f, 2.179369e-08f); rotation = new Quaternion(5.635086e-08f, -2.933667e-08f, -8.171964e-11f, 1f); scale = new Vector3(1f, 1f, 1f); break; //UpperLipsAdjust
				case 122: position = new Vector3(-0.01907434f, 5.222601e-09f, 0.001625076f); rotation = new Quaternion(4.339246e-07f, 4.632103e-08f, 5.213811e-09f, 1f); scale = new Vector3(1.125926f, 1.281438f, 1.268641f); break; //NeckAdjust
				case 123: position = new Vector3(-0.02193855f, 0.07578982f, 0.1081131f); rotation = new Quaternion(-0.1469386f, 0.6428148f, -0.1455778f, 0.7375671f); scale = new Vector3(0.3856083f, 1.097818f, 1.270819f); break; //RightOuterBreast
				case 124: position = new Vector3(-0.2049584f, -0.04001473f, 0.01422096f); rotation = new Quaternion(0.01610609f, -0.002123184f, -0.01010939f, 0.999817f); scale = new Vector3(0.7945861f, 0.8309759f, 0.7348408f); break; //RightInnerBreast
				case 125: position = new Vector3(-2.235174e-08f, 1.490116e-08f, -8.955249e-08f); rotation = new Quaternion(-9.313225e-10f, -1.67347e-09f, 2.689194e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightInnerBreastAdjust
				case 126: position = new Vector3(-4.470348e-08f, -3.72529e-09f, -4.656613e-09f); rotation = new Quaternion(-1.639128e-07f, 2.409797e-08f, 1.688023e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightOuterBreastAdjust
				case 127: position = new Vector3(-0.1722403f, 0.05202856f, 0.004816149f); rotation = new Quaternion(-0.04498555f, -0.02976042f, -0.821438f, 0.5677414f); scale = new Vector3(1.138851f, 1.138851f, 1.138851f); break; //RightShoulder
				case 128: position = new Vector3(-0.1227644f, 8.489199e-05f, 1.314096e-05f); rotation = new Quaternion(-0.008696565f, -0.02748671f, -0.1921947f, 0.9809332f); scale = new Vector3(0.899177f, 0.8991349f, 0.8991346f); break; //RightArm
				case 129: position = new Vector3(-0.06991167f, -0.0002714156f, 0.007745873f); rotation = new Quaternion(-0.05768599f, -0.01659651f, -0.004332553f, 0.9981874f); scale = new Vector3(0.970511f, 1.184445f, 1.235866f); break; //RightArmAdjust
				case 130: position = new Vector3(-0.2641162f, -1.032837e-06f, 6.519258e-07f); rotation = new Quaternion(-0.03062793f, 0.1073074f, 0.0301662f, 0.9932961f); scale = new Vector3(0.9159917f, 0.9444525f, 0.9444524f); break; //RightForeArm
				case 131: position = new Vector3(-0.03214762f, -0.0008035237f, 0.001881583f); rotation = new Quaternion(-1.493314e-07f, -2.588962e-08f, 1.088241e-07f, 1f); scale = new Vector3(1.061199f, 1.19743f, 1.174267f); break; //RightForeArmAdjust
				case 132: position = new Vector3(-0.1499468f, -1.341105e-07f, -5.215406e-08f); rotation = new Quaternion(-6.984917e-09f, -2.328307e-08f, 1.012813e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightForeArmTwist
				case 133: position = new Vector3(-0.03944022f, -0.002139877f, -0.0001319628f); rotation = new Quaternion(-5.442941e-08f, 3.026804e-09f, -9.25502e-08f, 1f); scale = new Vector3(0.9999999f, 1.137858f, 1.061686f); break; //RightForeArmTwistAdjust
				case 134: position = new Vector3(-0.3015362f, -0.001474739f, 0.000414866f); rotation = new Quaternion(0.7405879f, 0.009598094f, -0.0168911f, 0.6716785f); scale = new Vector3(1.091871f, 1.091871f, 1.091871f); break; //RightHand
				case 135: position = new Vector3(-0.08654371f, -0.02997058f, -0.004860781f); rotation = new Quaternion(0.1417517f, -0.04926156f, 0.1481943f, 0.9775061f); scale = new Vector3(1.028362f, 1.028362f, 1.028362f); break; //RightHandFinger01_01
				case 136: position = new Vector3(-0.03186321f, 3.72529e-08f, -1.117587e-08f); rotation = new Quaternion(-0.01071105f, -0.146359f, -0.01475291f, 0.9890636f); scale = new Vector3(1.000176f, 1.000176f, 1.000176f); break; //RightHandFinger01_02
				case 137: position = new Vector3(-0.01907147f, 0f, -8.195639e-08f); rotation = new Quaternion(-0.01442604f, -0.04638774f, 0.004240978f, 0.9988104f); scale = new Vector3(1.000204f, 1.000204f, 1.000205f); break; //RightHandFinger01_03
				case 138: position = new Vector3(-0.09005141f, -0.01246235f, 0.005587764f); rotation = new Quaternion(0.1282917f, -0.07860824f, 0.0855441f, 0.9849082f); scale = new Vector3(0.9943994f, 0.9943993f, 0.9943993f); break; //RightHandFinger02_01
				case 139: position = new Vector3(-0.03838407f, 0f, 0f); rotation = new Quaternion(-0.01291352f, -0.1053205f, 0.01559762f, 0.9942322f); scale = new Vector3(0.9998524f, 0.9998525f, 0.9998525f); break; //RightHandFinger02_02
				case 140: position = new Vector3(-0.03169182f, 0f, 4.470348e-08f); rotation = new Quaternion(-0.04728552f, -0.06059596f, -0.005238158f, 0.997028f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger02_03
				case 141: position = new Vector3(-0.09417446f, 0.01178301f, 0.00979384f); rotation = new Quaternion(0.02601366f, -0.08666522f, 0.05351748f, 0.9944589f); scale = new Vector3(1.048254f, 1.048255f, 1.048255f); break; //RightHandFinger03_01
				case 142: position = new Vector3(-0.04465385f, -3.72529e-09f, 4.656613e-08f); rotation = new Quaternion(0.02353405f, -0.09910367f, -0.008659026f, 0.9947611f); scale = new Vector3(0.9429799f, 0.9429798f, 0.9429799f); break; //RightHandFinger03_02
				case 143: position = new Vector3(-0.04047918f, 0f, -1.294538e-07f); rotation = new Quaternion(-0.02922287f, -0.01038508f, -0.008469339f, 0.9994831f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger03_03
				case 144: position = new Vector3(-0.09299814f, 0.03392572f, -0.0002735115f); rotation = new Quaternion(-0.07531101f, -0.02068188f, -0.02235529f, 0.9966949f); scale = new Vector3(1.03907f, 1.03907f, 1.03907f); break; //RightHandFinger04_01
				case 145: position = new Vector3(-0.04944686f, -5.587935e-09f, 4.377216e-08f); rotation = new Quaternion(0.001842739f, -0.078868f, -0.01541751f, 0.9967641f); scale = new Vector3(0.9706063f, 0.9706062f, 0.9706061f); break; //RightHandFinger04_02
				case 146: position = new Vector3(-0.0315415f, 1.117587e-08f, -4.93601e-08f); rotation = new Quaternion(-0.03192974f, -0.06776739f, -0.00644207f, 0.9971693f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger04_03
				case 147: position = new Vector3(-0.03700791f, 0.03903358f, -0.01876438f); rotation = new Quaternion(-0.6142231f, -0.05370096f, -0.429511f, 0.6598231f); scale = new Vector3(1.001943f, 1.001943f, 1.001943f); break; //RightHandFinger05_01
				case 148: position = new Vector3(-0.02941709f, -4.65661e-10f, 1.136214e-07f); rotation = new Quaternion(0.004958639f, -0.03799782f, 0.01392623f, 0.9991685f); scale = new Vector3(1.10048f, 1.100481f, 1.100481f); break; //RightHandFinger05_02
				case 149: position = new Vector3(-0.02863691f, 6.239861e-08f, -3.72529e-09f); rotation = new Quaternion(-0.01607755f, -0.09737744f, 0.01579131f, 0.9949924f); scale = new Vector3(0.8951109f, 0.8951108f, 0.8951111f); break; //RightHandFinger05_03
				case 150: position = new Vector3(-0.01836311f, 0.003484322f, 0.009763556f); rotation = new Quaternion(-0.1491432f, 2.74651e-07f, -2.041842e-06f, 0.9888156f); scale = new Vector3(1.003469f, 0.9282641f, 1.084446f); break; //RightShoulderAdjust
				case 151: position = new Vector3(-0.03316488f, -0.03210048f, -0.02310951f); rotation = new Quaternion(-0.7066576f, 0.002446171f, 0.07185731f, 0.7038932f); scale = new Vector3(0.7991538f, 0.7981592f, 1.118487f); break; //RightTrapezius


				case 152: position = new Vector3(-0.0435915f, -1.51973e-08f, 0.009657817f); rotation = new Quaternion(3.290855e-08f, 0.02206786f, 7.778296e-09f, 0.9997565f); scale = new Vector3(0.9686336f, 1.150009f, 1.2192903f); break; //Spine1Adjust
				case 153: position = new Vector3(-0.04072032f, 8.150174e-09f, -0.01918612f); rotation = new Quaternion(-2.684148e-08f, 3.170541e-08f, 6.115442e-09f, 1f); scale = new Vector3(0.9399757f, 1.092255f, 1.149668f); break; //SpineAdjust
				case 154: position = new Vector3(0.01667711f, 0.1110135f, 0.006990565f); rotation = new Quaternion(-0.05640496f, 0.0871789f, 0.9937748f, -0.04037174f); scale = new Vector3(0.9889364f, 0.9889369f, 0.9889377f); break; //RightUpLeg
				case 155: position = new Vector3(0.02272932f, -0.01146723f, -0.02099046f); rotation = new Quaternion(0.6402682f, 0.01229545f, 0.7646461f, 0.07226268f); scale = new Vector3(0.7271941f, 0.9741541f, 0.9692711f); break; //RightGluteus
				case 156: position = new Vector3(-0.4417297f, 3.252644e-07f, 2.58442e-07f); rotation = new Quaternion(0.02196218f, -0.02890557f, 0.03019614f, 0.9988846f); scale = new Vector3(0.9687352f, 0.9687354f, 0.9687353f); break; //RightLeg
				case 157: position = new Vector3(-0.5208466f, -0.002261322f, -0.008169506f); rotation = new Quaternion(0.8585583f, 0.01113601f, -0.5031638f, -0.09787661f); scale = new Vector3(1.072011f, 1.075886f, 1.043083f); break; //RightFoot
				case 158: position = new Vector3(-0.1767478f, -3.121095e-07f, 5.804002e-06f); rotation = new Quaternion(0.05929996f, -0.2054762f, 0.03158585f, 0.9763531f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightToeBase
				case 159: position = new Vector3(-0.1379875f, 0.009514896f, -0.001625478f); rotation = new Quaternion(0.01121597f, -0.004265372f, 0.006370801f, 0.9999077f); scale = new Vector3(1.019955f, 1.159788f, 1.174806f); break; //RightLegAdjust
				case 160: position = new Vector3(-0.1114379f, 0.003985205f, -0.001168349f); rotation = new Quaternion(0.01930375f, -0.01404312f, 0.006626697f, 0.9996931f); scale = new Vector3(0.9645033f, 1.109929f, 1.136319f); break; //RightUpLegAdjust

				default: position = Vector3.zero; rotation = Quaternion.identity; scale = Vector3.one; break;
			}
			transform.localPosition = position;
			transform.localRotation = rotation;
			transform.localScale = scale;
		}
		#endregion MasculineFemale

		#region FeminineMale
		private static void FeminineMaleLocalTranslation(int index, Transform transform)
		{
			Vector3 position; Quaternion rotation; Vector3 scale;

			switch (index)
			{
				case 0: position = new Vector3(-2.84666e-07f, 1.058908f, -0.02514136f); rotation = new Quaternion(0.0390729f, 0.03907251f, -0.7060263f, 0.7060266f); scale = new Vector3(1.000001f, 1.000001f, 1f); break; //Hips
				case 1: position = new Vector3(0.01727386f, -0.1102599f, 0.002575318f); rotation = new Quaternion(-0.05593707f, -0.07565564f, 0.9947171f, 0.04105299f); scale = new Vector3(1.011323f, 1.011327f, 1.011327f); break; //LeftUpLeg
				case 2: position = new Vector3(-0.01492867f, -0.0001192284f, -0.02402664f); rotation = new Quaternion(-0.0862052f, 0.8699773f, 0.03441336f, -0.4842767f); scale = new Vector3(1.410761f, 1.069324f, 1.551869f); break; //LeftGluteus
				case 3: position = new Vector3(-0.4319516f, -3.059045e-05f, -1.431035e-06f); rotation = new Quaternion(-0.04355213f, -0.0289659f, -0.03016892f, 0.9981754f); scale = new Vector3(1.033334f, 1.033334f, 1.033334f); break; //LeftLeg
				case 4: position = new Vector3(-0.4746962f, -0.001138987f, 0.0002549351f); rotation = new Quaternion(0.8610626f, -0.0186274f, -0.5024652f, 0.07584853f); scale = new Vector3(0.9530379f, 0.9366615f, 0.9705341f); break; //LeftFoot
				case 5: position = new Vector3(-0.1844991f, -0.0002450089f, 0.000647746f); rotation = new Quaternion(-0.06246676f, -0.2067474f, -0.02363327f, 0.9761121f); scale = new Vector3(0.9727218f, 0.9639587f, 0.9516407f); break; //LeftToeBase
				case 6: position = new Vector3(-0.1255213f, 0.005277484f, 8.195639e-08f); rotation = new Quaternion(4.278104e-08f, 1.887352e-09f, -1.792985e-08f, 1f); scale = new Vector3(0.9869091f, 0.8707808f, 0.9315838f); break; //LeftLegAdjust
				case 7: position = new Vector3(-0.1096654f, 0.003118356f, -0.001990527f); rotation = new Quaternion(-0.03335004f, -1.110762e-09f, 1.313322e-08f, 0.9994438f); scale = new Vector3(1.039189f, 0.9277276f, 0.9026474f); break; //LeftUpLegAdjust
				case 8: position = new Vector3(-0.05387875f, 4.067734e-09f, -7.450577e-09f); rotation = new Quaternion(5.486468e-08f, -0.07920212f, 3.437416e-08f, 0.9968586f); scale = new Vector3(0.9523803f, 0.9523803f, 0.9523803f); break; //LowerBack
				case 9: position = new Vector3(-0.04756127f, 1.920376e-08f, 0.01992907f); rotation = new Quaternion(2.010184e-08f, -0.02673777f, -1.251544e-09f, 0.9996425f); scale = new Vector3(1f, 0.9826012f, 0.896964f); break; //LowerBackAdjust
				case 10: position = new Vector3(-0.03201709f, 1.471317e-08f, -0.03498593f); rotation = new Quaternion(6.011978e-08f, -0.02695128f, -3.988234e-09f, 0.9996368f); scale = new Vector3(1.024438f, 0.9619883f, 0.9689989f); break; //LowerBackBelly
				case 11: position = new Vector3(-0.1927606f, 1.161762e-08f, -0.0001254007f); rotation = new Quaternion(-5.358099e-07f, -0.08683414f, 1.383217e-08f, 0.9962228f); scale = new Vector3(0.95f, 0.9499999f, 0.95f); break; //Spine
				case 12: position = new Vector3(-0.2016589f, 1.472073e-08f, 0.001094021f); rotation = new Quaternion(4.719642e-07f, 0.1107154f, -6.31566e-10f, 0.9938522f); scale = new Vector3(1.038002f, 1.038002f, 1.038002f); break; //Spine1
				case 13: position = new Vector3(-0.01989083f, -0.09842897f, 0.1631323f); rotation = new Quaternion(0.1190809f, 0.7327527f, 0.1218164f, 0.6588278f); scale = new Vector3(1.936667f, 0.9143337f, 0.6627707f); break; //LeftOuterBreast
				case 14: position = new Vector3(-0.02239034f, -0.03788922f, -0.02441116f); rotation = new Quaternion(0.02552453f, -0.04449615f, 0.005808174f, 0.9986666f); scale = new Vector3(1.550123f, 1.337827f, 1.829876f); break; //LeftInnerBreast
				case 15: position = new Vector3(-7.450581e-09f, 0f, -3.981404e-08f); rotation = new Quaternion(4.656613e-09f, 9.313224e-10f, -6.239862e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftInnerBreastAdjust
				case 16: position = new Vector3(-3.352761e-08f, 1.862645e-09f, -6.98492e-10f); rotation = new Quaternion(1.220032e-07f, -5.029141e-08f, 6.37956e-08f, 1f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftOuterBreastAdjust
				case 17: position = new Vector3(-0.1891434f, -0.05591699f, 0.003545439f); rotation = new Quaternion(0.04118672f, -0.03523577f, 0.8212634f, 0.5679688f); scale = new Vector3(0.9328521f, 0.9328522f, 0.9328523f); break; //LeftShoulder
				case 18: position = new Vector3(-0.1617436f, 8.598762e-05f, 2.810732e-05f); rotation = new Quaternion(-0.01501915f, -0.03559877f, 0.1911983f, 0.9807907f); scale = new Vector3(1.126348f, 1.126348f, 1.126348f); break; //LeftArm
				case 19: position = new Vector3(-0.06132666f, 0.001922886f, -0.005222812f); rotation = new Quaternion(0.2319012f, 0.01308873f, -0.001799718f, 0.9726496f); scale = new Vector3(1.038634f, 0.8678839f, 0.8439389f); break; //LeftArmAdjust
				case 20: position = new Vector3(-0.2779636f, 1.199543e-06f, -1.061708e-06f); rotation = new Quaternion(0.04841924f, 0.1092787f, -0.0254202f, 0.9925057f); scale = new Vector3(1.043344f, 1.043344f, 1.043344f); break; //LeftForeArm
				case 21: position = new Vector3(-0.03506916f, 0.0001569331f, 0.0006006956f); rotation = new Quaternion(2.391509e-07f, 1.955778e-08f, 6.1118e-09f, 1f); scale = new Vector3(0.9999999f, 0.8333778f, 0.8576512f); break; //LeftForeArmAdjust
				case 22: position = new Vector3(-0.1402762f, -9.976793e-08f, -8.009374e-08f); rotation = new Quaternion(3.837049e-07f, 1.583252e-08f, 9.551876e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftForeArmTwist
				case 23: position = new Vector3(-0.02623652f, -0.003286118f, 0.002732521f); rotation = new Quaternion(-5.403449e-08f, 1.589875e-08f, 9.964321e-08f, 1f); scale = new Vector3(1.05441f, 0.8617852f, 1.040645f); break; //LeftForeArmTwistAdjust
				case 24: position = new Vector3(-0.2802435f, -0.0006940143f, 0.002079759f); rotation = new Quaternion(-0.6817607f, 0.01714288f, 0.00921896f, 0.7313162f); scale = new Vector3(0.8802606f, 0.9096622f, 0.8802603f); break; //LeftHand
				case 25: position = new Vector3(-0.09374909f, 0.03802259f, -0.009213493f); rotation = new Quaternion(-0.2352607f, -0.07237107f, -0.1193147f, 0.9618622f); scale = new Vector3(1.026957f, 1.026957f, 1.026957f); break; //LeftHandFinger01_01
				case 26: position = new Vector3(-0.04097405f, 0f, 1.564622e-07f); rotation = new Quaternion(0.01458864f, -0.1553527f, -0.0172368f, 0.987601f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftHandFinger01_02
				case 27: position = new Vector3(-0.02452452f, 1.490116e-08f, 1.043081e-07f); rotation = new Quaternion(-0.03153313f, -0.04711502f, 0.003371943f, 0.998386f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftHandFinger01_03
				case 28: position = new Vector3(-0.1019953f, 0.01784287f, 0.004323957f); rotation = new Quaternion(-0.1513647f, -0.09511004f, -0.08219054f, 0.9804527f); scale = new Vector3(1.037381f, 1.037381f, 1.037381f); break; //LeftHandFinger02_01
				case 29: position = new Vector3(-0.0428219f, 7.450581e-09f, -1.545995e-07f); rotation = new Quaternion(0.005524056f, -0.1104277f, 0.003074965f, 0.9938641f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger02_02
				case 30: position = new Vector3(-0.03535588f, -7.450581e-09f, 1.695007e-07f); rotation = new Quaternion(0.00781147f, -0.07867061f, -0.001130257f, 0.9968694f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger02_03
				case 31: position = new Vector3(-0.1032225f, -0.009252488f, 0.008609849f); rotation = new Quaternion(-0.07659466f, -0.07660012f, -0.02700821f, 0.9937486f); scale = new Vector3(1.033955f, 1.033955f, 1.033955f); break; //LeftHandFinger03_01
				case 32: position = new Vector3(-0.05342074f, 9.313226e-09f, -1.289882e-07f); rotation = new Quaternion(0.009352637f, -0.07377303f, -0.02966156f, 0.99679f); scale = new Vector3(0.9999999f, 0.9999998f, 0.9999999f); break; //LeftHandFinger03_02
				case 33: position = new Vector3(-0.04290801f, -7.450581e-09f, -6.519258e-09f); rotation = new Quaternion(0.01737409f, -0.04760422f, 0.01748182f, 0.9985622f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftHandFinger03_03
				case 34: position = new Vector3(-0.1025106f, -0.03571585f, 0.005880898f); rotation = new Quaternion(-0.03593553f, -0.0251815f, 0.02583689f, 0.9987027f); scale = new Vector3(1.026442f, 1.026442f, 1.026442f); break; //LeftHandFinger04_01
				case 35: position = new Vector3(-0.06006591f, 2.235174e-08f, -1.763692e-08f); rotation = new Quaternion(0.001562129f, -0.08490789f, 0.007714472f, 0.9963577f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger04_02
				case 36: position = new Vector3(-0.03607317f, 7.450581e-09f, -1.909211e-08f); rotation = new Quaternion(-0.00203133f, -0.0726887f, -0.007082315f, 0.9973275f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger04_03
				case 37: position = new Vector3(-0.0416078f, -0.04500376f, -0.01705294f); rotation = new Quaternion(0.5758775f, -0.06057817f, 0.4394075f, 0.6867434f); scale = new Vector3(1.00706f, 1.00706f, 1.00706f); break; //LeftHandFinger05_01
				case 38: position = new Vector3(-0.03329906f, -1.583248e-08f, -3.352761e-08f); rotation = new Quaternion(-0.04481915f, -0.02040773f, -0.02157341f, 0.9985536f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger05_02
				case 39: position = new Vector3(-0.03892253f, 5.261973e-08f, -6.146729e-08f); rotation = new Quaternion(0.02565612f, -0.03165369f, -0.02530968f, 0.998849f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftHandFinger05_03
				case 40: position = new Vector3(-0.05277037f, -0.01022575f, -0.00747193f); rotation = new Quaternion(0.1465888f, 5.032324e-08f, 1.681956e-07f, 0.9891975f); scale = new Vector3(1.019397f, 1.141855f, 1.028448f); break; //LeftShoulderAdjust
				case 41: position = new Vector3(-0.03766095f, 0.03578182f, -0.03694517f); rotation = new Quaternion(0.668507f, -0.1638424f, -0.2393992f, 0.6847935f); scale = new Vector3(0.7940578f, 0.9999998f, 0.8216048f); break; //LeftTrapezius
				case 42: position = new Vector3(-0.2087728f, 1.596152e-08f, 0.000422655f); rotation = new Quaternion(-3.055475e-07f, 0.09072298f, -2.617269e-08f, 0.9958762f); scale = new Vector3(1f, 1f, 1f); break; //Neck
				case 43: position = new Vector3(-0.101601f, 7.803427e-09f, 0.0007252954f); rotation = new Quaternion(1.246556e-07f, -0.08316029f, 2.214956e-08f, 0.9965362f); scale = new Vector3(1.053043f, 1.053043f, 1.053043f); break; //Head
				case 44: position = new Vector3(-0.0500035f, 3.261588e-09f, -0.004129222f); rotation = new Quaternion(-4.961742e-08f, 3.297208e-09f, 1.981236e-10f, 1f); scale = new Vector3(0.9886068f, 0.9937958f, 1.020746f); break; //HeadAdjust
				case 45: position = new Vector3(0.02563071f, 5.396079e-07f, 0.08506858f); rotation = new Quaternion(-1.874842e-05f, 0.7089475f, 1.731631e-05f, 0.7052612f); scale = new Vector3(0.948566f, 1f, 1.001526f); break; //InnerMouthUpperAdjust
				case 46: position = new Vector3(0.03632145f, -5.796908e-09f, -0.004185734f); rotation = new Quaternion(-1.359001e-08f, 0.007815707f, 1.374127e-08f, 0.9999695f); scale = new Vector3(0.8651752f, 0.8895745f, 0.885771f); break; //UpperNeckAdjust
				case 47: position = new Vector3(-0.06712677f, -0.04223561f, 0.1172129f); rotation = new Quaternion(0.6685793f, 0.2419585f, -0.6604133f, -0.2414791f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftCheek
				case 48: position = new Vector3(5.413312e-08f, 0f, 1.57117e-07f); rotation = new Quaternion(-8.661299e-08f, -1.720469e-07f, 9.69876e-08f, 1f); scale = new Vector3(0.6204871f, 0.9999999f, 0.9999999f); break; //LeftCheekAdjust
				case 49: position = new Vector3(-0.06976902f, -0.07958012f, 0.04123047f); rotation = new Quaternion(-0.1128115f, -0.5704384f, 0.08211996f, 0.8094009f); scale = new Vector3(1.011096f, 1f, 0.9743308f); break; //LeftEar
				case 50: position = new Vector3(0.001100927f, -0.001185407f, -0.0005229791f); rotation = new Quaternion(-0.0004224796f, -0.0553925f, -0.03735125f, 0.9977657f); scale = new Vector3(0.9131733f, 0.8439585f, 0.9999996f); break; //LeftEarAdjust
				case 51: position = new Vector3(-0.09854173f, -0.03542722f, 0.1176764f); rotation = new Quaternion(0.7124565f, -3.147971e-06f, -0.7017163f, -4.264394e-06f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftEye
				case 52: position = new Vector3(1.280799e-07f, -1.311241e-07f, -9.790049e-07f); rotation = new Quaternion(2.13677e-06f, 3.661946e-06f, 1.409023e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftEyeAdjust
				case 53: position = new Vector3(4.611891e-08f, -7.896472e-08f, -3.829595e-07f); rotation = new Quaternion(4.351459e-06f, -3.573018e-06f, 3.377357e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftEyeGlobe
				case 54: position = new Vector3(4.611891e-08f, -7.896472e-08f, -3.829595e-07f); rotation = new Quaternion(0.0002753912f, -0.1797683f, 1.186911e-05f, 0.983709f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftLowerLid
				case 55: position = new Vector3(9.79403e-08f, 2.523848e-09f, -1.004792e-07f); rotation = new Quaternion(0.0002784707f, -0.0004666685f, -3.696972e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftLowerLidAdjust
				case 56: position = new Vector3(4.611891e-08f, -7.896472e-08f, -3.829595e-07f); rotation = new Quaternion(-0.0003551272f, 0.1202976f, 1.443579e-05f, 0.9927378f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftUpperLid
				case 57: position = new Vector3(-3.329797e-08f, -5.071342e-09f, 9.946234e-08f); rotation = new Quaternion(-0.000349275f, 0.0003588478f, -3.607667e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftUpperLidAdjust
				case 58: position = new Vector3(-0.1012276f, -0.01901268f, 0.08096836f); rotation = new Quaternion(0.7628015f, -1.756795e-07f, -0.6466327f, 2.51221e-07f); scale = new Vector3(1.019987f, 1.019987f, 1.019987f); break; //LeftEyebrowLow
				case 59: position = new Vector3(-0.0002564131f, 0.0003370339f, 0.0002020517f); rotation = new Quaternion(-3.918497e-08f, -0.02080574f, -6.489576e-09f, 0.9997836f); scale = new Vector3(0.9999999f, 1f, 1.040408f); break; //LeftEyebrowLowAdjust
				case 60: position = new Vector3(-0.1086634f, -0.03694652f, 0.07669835f); rotation = new Quaternion(0.757008f, -2.367996e-07f, -0.6534057f, 1.297468e-07f); scale = new Vector3(1.011523f, 1.011523f, 1.011523f); break; //LeftEyebrowMiddle
				case 61: position = new Vector3(-0.001747962f, 0.0005534627f, -0.0002089657f); rotation = new Quaternion(-1.564781e-08f, -0.008941519f, 2.048631e-08f, 0.9999601f); scale = new Vector3(1f, 1f, 1.12416f); break; //LeftEyebrowMiddleAdjust
				case 62: position = new Vector3(-0.1079068f, -0.050924f, 0.07034407f); rotation = new Quaternion(0.7581763f, -0.0005242468f, -0.6520492f, 0.0004017456f); scale = new Vector3(1.011969f, 1.011969f, 1.011969f); break; //LeftEyebrowUp
				case 63: position = new Vector3(-7.999185e-05f, 0.001335719f, 0.0006049377f); rotation = new Quaternion(-2.682713e-07f, -0.01504107f, 2.624041e-08f, 0.9998869f); scale = new Vector3(1f, 1f, 1.082309f); break; //LeftEyebrowUpAdjust
				case 64: position = new Vector3(-0.02661069f, -0.01138672f, 0.1483579f); rotation = new Quaternion(0.7941854f, 0.06683576f, -0.6017596f, -0.05184562f); scale = new Vector3(0.821725f, 0.8217245f, 0.8217244f); break; //LeftLipsSuperiorMiddle
				case 65: position = new Vector3(-5.075708e-08f, -9.31323e-10f, 1.776643e-07f); rotation = new Quaternion(-0.0001274614f, 7.157346e-06f, 2.202388e-07f, 1f); scale = new Vector3(1f, 1.12709f, 1.658196f); break; //LeftLipsSuperiorMiddleAdjust
				case 66: position = new Vector3(-0.01962298f, -0.03845807f, 0.112337f); rotation = new Quaternion(-0.6591184f, -0.2391575f, 0.6702262f, 0.2432355f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLowCheek
				case 67: position = new Vector3(-2.235174e-08f, -7.450581e-09f, -2.790237e-07f); rotation = new Quaternion(-4.107441e-07f, -1.554409e-07f, 1.067419e-07f, 1f); scale = new Vector3(0.7697079f, 1f, 1f); break; //LeftLowCheekAdjust
				case 68: position = new Vector3(-0.04907912f, -0.03004789f, 0.1015634f); rotation = new Quaternion(0.7088718f, 0.001014975f, -0.7053238f, 0.004257867f); scale = new Vector3(1f, 1f, 1f); break; //LeftNose
				case 69: position = new Vector3(7.558265e-08f, 2.197339e-08f, 6.91216e-09f); rotation = new Quaternion(-0.0003549638f, 7.694769e-08f, 1.311676e-08f, 0.9999999f); scale = new Vector3(1f, 1f, 1f); break; //LeftNoseAdjust
				case 70: position = new Vector3(-0.02660465f, -9.391321e-05f, 0.1504496f); rotation = new Quaternion(0.7872863f, -2.404934e-07f, -0.6165876f, -1.478183e-07f); scale = new Vector3(0.8076372f, 0.807637f, 0.807637f); break; //LipsSuperior
				case 71: position = new Vector3(-5.96141e-08f, 1.15273e-10f, 0.0002114903f); rotation = new Quaternion(-7.740047e-07f, -6.072222e-06f, 7.125929e-10f, 1f); scale = new Vector3(1f, 1f, 1.330045f); break; //LipsSuperiorAdjust
				case 72: position = new Vector3(-0.04598131f, -9.392465e-05f, 0.04652536f); rotation = new Quaternion(-0.4972367f, 1.622358e-07f, 0.8676149f, 1.539469e-08f); scale = new Vector3(1.091083f, 1.091083f, 1.091083f); break; //Mandible
				case 73: position = new Vector3(-0.0532118f, -9.420927e-05f, -0.01785251f); rotation = new Quaternion(0.9679447f, 4.193859e-06f, -0.2511635f, 1.322958e-05f); scale = new Vector3(0.8260153f, 0.8367497f, 0.9323453f); break; //InnerMouthLowerAdjust
				case 74: position = new Vector3(-0.08022933f, 0.0252738f, 0.01297972f); rotation = new Quaternion(-0.02191469f, 0.268322f, -0.0783958f, 0.9598839f); scale = new Vector3(1.068304f, 1.068304f, 1.068304f); break; //LeftLips
				case 75: position = new Vector3(-8.932311e-09f, -2.840068e-09f, 1.804498e-08f); rotation = new Quaternion(2.27663e-07f, 7.450581e-08f, -5.960467e-08f, 1f); scale = new Vector3(1f, 1f, 0.854512f); break; //LeftLipsAdjust
				case 76: position = new Vector3(-0.09094825f, 0.01034905f, 0.02346897f); rotation = new Quaternion(-0.01227464f, 0.1773465f, -0.08519589f, 0.9803771f); scale = new Vector3(0.7832115f, 0.7832115f, 0.7832114f); break; //LeftLipsInferior
				case 77: position = new Vector3(-0.0002290404f, -1.210719e-08f, 2.008164e-08f); rotation = new Quaternion(2.661022e-06f, 7.243945e-07f, 2.55921e-09f, 1f); scale = new Vector3(0.9999998f, 0.9999998f, 1.125218f); break; //LeftLipsInferiorAdjust
				case 78: position = new Vector3(-0.02979268f, 0.05343488f, -0.02262446f); rotation = new Quaternion(0.02706345f, 0.106347f, 0.2028247f, 0.9730468f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftLowMaxilar
				case 79: position = new Vector3(1.396984e-08f, -3.72529e-09f, -1.571607e-09f); rotation = new Quaternion(-3.348105e-07f, -1.040986e-07f, -5.172634e-09f, 1f); scale = new Vector3(1.080295f, 1f, 1f); break; //LeftLowMaxilarAdjust
				case 80: position = new Vector3(-0.09165317f, -1.984354e-05f, 0.02356796f); rotation = new Quaternion(1.408716e-08f, 0.1732539f, -5.764143e-08f, 0.9848772f); scale = new Vector3(0.8121401f, 0.8121401f, 0.81214f); break; //LipsInferior
				case 81: position = new Vector3(2.234385e-08f, -8.913e-12f, -2.36557e-07f); rotation = new Quaternion(-3.619625e-07f, -6.057323e-06f, 3.977081e-10f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 1.23365f); break; //LipsInferiorAdjust
				case 82: position = new Vector3(-0.0006158142f, 4.60591e-10f, 0.002391047f); rotation = new Quaternion(-8.658634e-08f, 2.980232e-08f, -2.853586e-09f, 1f); scale = new Vector3(1f, 0.9243894f, 0.9999999f); break; //MandibleAdjust
				case 83: position = new Vector3(-0.08022933f, -0.02546169f, 0.01297972f); rotation = new Quaternion(0.02191471f, 0.2683221f, 0.0783958f, 0.9598839f); scale = new Vector3(1.068304f, 1.068304f, 1.068304f); break; //RightLips
				case 84: position = new Vector3(-1.504482e-08f, 1.784031e-09f, 1.212171e-07f); rotation = new Quaternion(-2.349054e-07f, -2.980228e-08f, 1.266599e-07f, 1f); scale = new Vector3(0.9999999f, 1f, 0.854512f); break; //RightLipsAdjust
				case 85: position = new Vector3(-0.09094818f, -0.01053693f, 0.02346896f); rotation = new Quaternion(0.01227469f, 0.1773464f, 0.08519588f, 0.9803771f); scale = new Vector3(0.7832115f, 0.7832115f, 0.7832114f); break; //RightLipsInferior
				case 86: position = new Vector3(-0.0002290385f, 1.210719e-08f, 1.581357e-07f); rotation = new Quaternion(-2.664747e-06f, 8.237549e-07f, 5.774421e-08f, 1f); scale = new Vector3(0.9999998f, 0.9999999f, 1.125218f); break; //RightLipsInferiorAdjust
				case 87: position = new Vector3(-0.0297928f, -0.05362285f, -0.0226244f); rotation = new Quaternion(-0.02706339f, 0.1063467f, -0.2028251f, 0.9730468f); scale = new Vector3(1f, 1f, 1f); break; //RightLowMaxilar
				case 88: position = new Vector3(7.450581e-08f, -8.428469e-08f, 1.245644e-07f); rotation = new Quaternion(3.525056e-07f, -7.392507e-08f, -2.758717e-08f, 1f); scale = new Vector3(1.080295f, 1f, 1f); break; //RightLowMaxilarAdjust
				case 89: position = new Vector3(-0.03030576f, -9.377696e-05f, -0.01622993f); rotation = new Quaternion(1.994335e-06f, 0.2488658f, -7.683257e-07f, 0.968538f); scale = new Vector3(0.9169153f, 0.9128436f, 0.9303862f); break; //Tongue01
				case 90: position = new Vector3(-0.02847365f, 2.150826e-09f, -3.034972e-08f); rotation = new Quaternion(0.9989919f, 5.379464e-08f, -0.04489065f, 7.171199e-06f); scale = new Vector3(1f, 0.9999999f, 1f); break; //Tongue02
				case 91: position = new Vector3(-0.05818778f, -9.390469e-05f, 0.1508375f); rotation = new Quaternion(0.7542147f, 3.351152e-08f, -0.6566279f, 2.722515e-07f); scale = new Vector3(0.9200171f, 0.920017f, 0.9200169f); break; //NoseBase
				case 92: position = new Vector3(0.0006432459f, -2.4257e-10f, 0.0002285885f); rotation = new Quaternion(1.682526e-08f, -0.0006736514f, 9.098084e-09f, 0.9999998f); scale = new Vector3(0.9332315f, 1f, 1.10448f); break; //NoseBaseAdjust
				case 93: position = new Vector3(-0.08152758f, 4.117944e-05f, 0.143144f); rotation = new Quaternion(0.7430959f, 0.001057928f, -0.6691815f, -0.00186233f); scale = new Vector3(0.9056129f, 0.9056131f, 0.9056129f); break; //NoseMiddle
				case 94: position = new Vector3(5.864294e-09f, -9.476935e-09f, -1.658336e-07f); rotation = new Quaternion(-0.0001308797f, 0.0005024356f, -0.0009101599f, 0.9999995f); scale = new Vector3(1f, 1.029289f, 1.019319f); break; //NoseMiddleAdjust
				case 95: position = new Vector3(-0.105655f, 0.0002149012f, 0.124284f); rotation = new Quaternion(0.7096485f, 0.006206985f, -0.7045107f, 0.0050313f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //NoseTop
				case 96: position = new Vector3(8.687522e-08f, 7.406925e-08f, -0.0005947979f); rotation = new Quaternion(5.819657e-05f, 2.077254e-06f, 3.921456e-08f, 1f); scale = new Vector3(1.003103f, 1.166767f, 1.900944f); break; //NoseTopAdjust
				case 97: position = new Vector3(-0.06712676f, 0.04223564f, 0.1172129f); rotation = new Quaternion(0.6685793f, -0.2419582f, -0.6604136f, 0.2414785f); scale = new Vector3(1f, 1f, 1f); break; //RightCheek
				case 98: position = new Vector3(4.284084e-08f, -3.259629e-09f, -4.463072e-08f); rotation = new Quaternion(6.554183e-08f, 6.022577e-08f, -1.690316e-07f, 1f); scale = new Vector3(0.6204872f, 1f, 0.9999999f); break; //RightCheekAdjust
				case 99: position = new Vector3(-0.06976889f, 0.07958032f, 0.04123044f); rotation = new Quaternion(0.1128111f, -0.5704386f, -0.08211961f, 0.8094009f); scale = new Vector3(1.011096f, 1f, 0.9743308f); break; //RightEar
				case 100: position = new Vector3(0.001100995f, 0.001185446f, -0.000523112f); rotation = new Quaternion(0.0004225573f, -0.05539251f, 0.03735122f, 0.9977657f); scale = new Vector3(0.9131732f, 0.8439586f, 0.9999996f); break; //RightEarAdjust
				case 101: position = new Vector3(-0.09854172f, 0.03542729f, 0.1176764f); rotation = new Quaternion(0.7124565f, 3.104735e-06f, -0.7017163f, 4.008905e-06f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //RightEye
				case 102: position = new Vector3(1.135783e-07f, 7.665227e-08f, -9.789871e-07f); rotation = new Quaternion(-2.130592e-06f, 3.665209e-06f, 2.000773e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightEyeAdjust
				case 103: position = new Vector3(9.122203e-08f, 9.527329e-08f, -3.829407e-07f); rotation = new Quaternion(-4.36986e-06f, -3.569756e-06f, -1.373475e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightEyeGlobe
				case 104: position = new Vector3(6.887025e-08f, 5.056982e-08f, -3.829413e-07f); rotation = new Quaternion(-0.0002752538f, -0.1797682f, -1.182058e-05f, 0.983709f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightLowerLid
				case 105: position = new Vector3(3.396917e-08f, 4.42014e-10f, -1.252101e-07f); rotation = new Quaternion(-0.0002784835f, -0.0004600523f, 3.697371e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightLowerLidAdjust
				case 106: position = new Vector3(9.122203e-08f, 9.527329e-08f, -3.829407e-07f); rotation = new Quaternion(0.0003551593f, 0.1202971f, -1.501938e-05f, 0.9927379f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightUpperLid
				case 107: position = new Vector3(3.586138e-08f, -2.783054e-09f, -4.69845e-08f); rotation = new Quaternion(0.0003495013f, 0.0003592868f, 3.616857e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightUpperLidAdjust
				case 108: position = new Vector3(-0.1012276f, 0.01901274f, 0.08096838f); rotation = new Quaternion(0.7628015f, -1.028009e-07f, -0.6466327f, -5.856323e-08f); scale = new Vector3(1.019987f, 1.019987f, 1.019987f); break; //RightEyebrowLow
				case 109: position = new Vector3(-0.0002562777f, -0.0003370334f, 0.0002021817f); rotation = new Quaternion(1.371521e-09f, -0.02080587f, -7.265487e-09f, 0.9997835f); scale = new Vector3(0.9999999f, 1f, 1.040408f); break; //RightEyebrowLowAdjust
				case 110: position = new Vector3(-0.1086634f, 0.03694658f, 0.07669834f); rotation = new Quaternion(0.7570081f, 1.085359e-07f, -0.6534056f, 1.193849e-07f); scale = new Vector3(1.011523f, 1.011523f, 1.011523f); break; //RightEyebrowMiddle
				case 111: position = new Vector3(-0.001747987f, -0.0005534569f, -0.0002089831f); rotation = new Quaternion(1.538638e-07f, -0.008941543f, 1.880142e-08f, 0.9999601f); scale = new Vector3(1f, 1f, 1.12416f); break; //RightEyebrowMiddleAdjust
				case 112: position = new Vector3(-0.1079067f, 0.05092406f, 0.07034405f); rotation = new Quaternion(0.7581764f, 0.0005241366f, -0.6520492f, -0.00040192f); scale = new Vector3(1.011969f, 1.011969f, 1.011969f); break; //RightEyebrowUp
				case 113: position = new Vector3(-7.990273e-05f, -0.001335717f, 0.0006048386f); rotation = new Quaternion(-2.068411e-07f, -0.01504113f, 2.282661e-08f, 0.9998869f); scale = new Vector3(1f, 1f, 1.082309f); break; //RightEyebrowUpAdjust
				case 114: position = new Vector3(-0.0269868f, 0.01162439f, 0.1496955f); rotation = new Quaternion(0.7941854f, -0.06683597f, -0.6017596f, 0.05184587f); scale = new Vector3(0.8217247f, 0.8217245f, 0.8217244f); break; //RightLipsSuperiorMiddle
				case 115: position = new Vector3(2.863817e-08f, 4.656613e-09f, 2.637171e-08f); rotation = new Quaternion(0.0001276073f, 7.145171e-06f, -1.797257e-07f, 1f); scale = new Vector3(1f, 1.12709f, 1.658196f); break; //RightLipsSuperiorMiddleAdjust
				case 116: position = new Vector3(-0.01962297f, 0.03845811f, 0.112337f); rotation = new Quaternion(-0.6591182f, 0.239158f, 0.6702261f, -0.2432358f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightLowCheek
				case 117: position = new Vector3(-2.421439e-08f, 3.72529e-09f, -5.091397e-08f); rotation = new Quaternion(2.533924e-07f, -2.00501e-07f, -2.844968e-07f, 1f); scale = new Vector3(0.8966534f, 1f, 1f); break; //RightLowCheekAdjust
				case 118: position = new Vector3(-0.049079f, 0.03004794f, 0.1015633f); rotation = new Quaternion(0.7088718f, -0.001028659f, -0.7053237f, -0.004271158f); scale = new Vector3(0.9999994f, 1f, 0.9999999f); break; //RightNose
				case 119: position = new Vector3(3.361492e-09f, -1.762237e-08f, 1.633889e-07f); rotation = new Quaternion(0.0003562416f, 7.696725e-08f, 1.627073e-08f, 0.9999999f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightNoseAdjust
				case 120: position = new Vector3(-0.03420704f, -9.391519e-05f, 0.1198201f); rotation = new Quaternion(0.7343492f, -1.02508e-07f, -0.6787719f, 1.33107e-07f); scale = new Vector3(1f, 1f, 0.9999999f); break; //UpperLips
				case 121: position = new Vector3(2.235768e-08f, 2.419e-12f, 9.966608e-08f); rotation = new Quaternion(2.272027e-09f, 1.508743e-06f, 2.408042e-11f, 1f); scale = new Vector3(0.9999999f, 1f, 0.8275772f); break; //UpperLipsAdjust
				case 122: position = new Vector3(-0.032709f, 1.409616e-09f, -0.00312411f); rotation = new Quaternion(4.341309e-07f, 0.01427352f, 1.930307e-09f, 0.9998981f); scale = new Vector3(1.009204f, 0.8957027f, 0.9492784f); break; //NeckAdjust
				case 123: position = new Vector3(-0.01989081f, 0.09842917f, 0.1631322f); rotation = new Quaternion(-0.1190811f, 0.7327527f, -0.1218169f, 0.6588277f); scale = new Vector3(1.936667f, 0.9143336f, 0.6627707f); break; //RightOuterBreast
				case 124: position = new Vector3(-0.02239029f, 0.03788918f, -0.02441147f); rotation = new Quaternion(-0.0255248f, -0.04449616f, -0.005808187f, 0.9986665f); scale = new Vector3(1.550123f, 1.337827f, 1.829876f); break; //RightInnerBreast
				case 125: position = new Vector3(1.862645e-09f, -1.490116e-08f, -4.982576e-08f); rotation = new Quaternion(1.862645e-09f, 2.793968e-09f, 4.889444e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightInnerBreastAdjust
				case 126: position = new Vector3(5.587935e-09f, -1.490116e-08f, -4.377216e-08f); rotation = new Quaternion(-3.75323e-07f, -3.911554e-08f, -2.945309e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightOuterBreastAdjust
				case 127: position = new Vector3(-0.1891434f, 0.05591702f, 0.00354537f); rotation = new Quaternion(-0.04118712f, -0.03523624f, -0.8212634f, 0.5679688f); scale = new Vector3(0.9328521f, 0.932852f, 0.9328523f); break; //RightShoulder
				case 128: position = new Vector3(-0.1617435f, -8.58577e-05f, 2.812222e-05f); rotation = new Quaternion(0.01501919f, -0.03559877f, -0.1911983f, 0.9807907f); scale = new Vector3(1.126348f, 1.126348f, 1.126348f); break; //RightArm
				case 129: position = new Vector3(-0.06132673f, -0.001922927f, -0.005222794f); rotation = new Quaternion(-0.2319019f, 0.01308846f, 0.001799662f, 0.9726495f); scale = new Vector3(1.038634f, 0.867884f, 0.8439389f); break; //RightArmAdjust
				case 130: position = new Vector3(-0.2779636f, -1.107343e-06f, -1.057982e-06f); rotation = new Quaternion(-0.04841929f, 0.1092787f, 0.02542005f, 0.9925057f); scale = new Vector3(1.043345f, 1.043344f, 1.043344f); break; //RightForeArm
				case 131: position = new Vector3(-0.03506903f, -0.0001568897f, 0.0006007068f); rotation = new Quaternion(-2.170797e-07f, 1.39698e-08f, 2.057787e-07f, 1f); scale = new Vector3(0.9999998f, 0.8333778f, 0.8576511f); break; //RightForeArmAdjust
				case 132: position = new Vector3(-0.140276f, 5.314359e-08f, -6.891787e-08f); rotation = new Quaternion(-4.391187e-07f, 9.313188e-09f, 8.656936e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightForeArmTwist
				case 133: position = new Vector3(-0.02623666f, 0.003286261f, 0.002732508f); rotation = new Quaternion(1.469738e-07f, 1.236569e-08f, -6.525112e-08f, 1f); scale = new Vector3(1.05441f, 0.8617853f, 1.040645f); break; //RightForeArmTwistAdjust
				case 134: position = new Vector3(-0.2802432f, 0.000694056f, 0.002079757f); rotation = new Quaternion(0.6817607f, 0.017143f, -0.009218701f, 0.7313162f); scale = new Vector3(0.8802605f, 0.9096621f, 0.8802603f); break; //RightHand
				case 135: position = new Vector3(-0.09374934f, -0.03802225f, -0.009213243f); rotation = new Quaternion(0.2352607f, -0.0723712f, 0.1193144f, 0.9618623f); scale = new Vector3(1.026957f, 1.026957f, 1.026957f); break; //RightHandFinger01_01
				case 136: position = new Vector3(-0.04097405f, 1.490116e-08f, -5.215406e-08f); rotation = new Quaternion(-0.0145886f, -0.1553528f, 0.01723684f, 0.9876009f); scale = new Vector3(1f, 0.9999998f, 1f); break; //RightHandFinger01_02
				case 137: position = new Vector3(-0.02452462f, 1.490116e-08f, -5.215406e-08f); rotation = new Quaternion(0.03153281f, -0.047115f, -0.003371915f, 0.998386f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999998f); break; //RightHandFinger01_03
				case 138: position = new Vector3(-0.1019954f, -0.0178425f, 0.004324333f); rotation = new Quaternion(0.1513645f, -0.09511019f, 0.08218994f, 0.9804528f); scale = new Vector3(1.037381f, 1.037381f, 1.037381f); break; //RightHandFinger02_01
				case 139: position = new Vector3(-0.04282186f, 7.450581e-09f, -1.862645e-08f); rotation = new Quaternion(-0.005523528f, -0.1104278f, -0.003074885f, 0.9938641f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger02_02
				case 140: position = new Vector3(-0.03535582f, -7.450581e-09f, -2.421439e-08f); rotation = new Quaternion(-0.007811418f, -0.07866933f, 0.001129819f, 0.9968696f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger02_03
				case 141: position = new Vector3(-0.1032228f, 0.00925285f, 0.008609957f); rotation = new Quaternion(0.07659483f, -0.07660024f, 0.02700766f, 0.9937486f); scale = new Vector3(1.033955f, 1.033955f, 1.033956f); break; //RightHandFinger03_01
				case 142: position = new Vector3(-0.0534207f, 3.72529e-09f, -1.452863e-07f); rotation = new Quaternion(-0.009352577f, -0.0737729f, 0.02966169f, 0.99679f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger03_02
				case 143: position = new Vector3(-0.04290807f, -3.72529e-09f, -7.636845e-08f); rotation = new Quaternion(-0.0173743f, -0.04760429f, -0.01748179f, 0.9985622f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger03_03
				case 144: position = new Vector3(-0.1025108f, 0.03571627f, 0.005881312f); rotation = new Quaternion(0.0359356f, -0.02518155f, -0.02583733f, 0.9987026f); scale = new Vector3(1.026442f, 1.026442f, 1.026442f); break; //RightHandFinger04_01
				case 145: position = new Vector3(-0.06006599f, -1.862645e-09f, -1.302105e-07f); rotation = new Quaternion(-0.001561863f, -0.08490787f, -0.007714412f, 0.9963577f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightHandFinger04_02
				case 146: position = new Vector3(-0.03607316f, 7.450581e-09f, 2.28174e-08f); rotation = new Quaternion(0.002031396f, -0.07268871f, 0.007082297f, 0.9973275f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightHandFinger04_03
				case 147: position = new Vector3(-0.04160774f, 0.04500407f, -0.0170526f); rotation = new Quaternion(-0.5758773f, -0.06057756f, -0.4394079f, 0.6867434f); scale = new Vector3(1.00706f, 1.00706f, 1.00706f); break; //RightHandFinger05_01
				case 148: position = new Vector3(-0.03329912f, 0f, -4.470348e-08f); rotation = new Quaternion(0.04481697f, -0.02040781f, 0.02157381f, 0.9985538f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger05_02
				case 149: position = new Vector3(-0.03892269f, -1.018634e-07f, -5.401671e-08f); rotation = new Quaternion(-0.0256543f, -0.03165287f, 0.02530853f, 0.9988491f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightHandFinger05_03
				case 150: position = new Vector3(-0.05277018f, 0.01022581f, -0.007471911f); rotation = new Quaternion(-0.1465887f, 8.146697e-08f, -1.259446e-07f, 0.9891976f); scale = new Vector3(1.019397f, 1.141855f, 1.028448f); break; //RightShoulderAdjust
				case 151: position = new Vector3(-0.03766088f, -0.03578168f, -0.03694516f); rotation = new Quaternion(-0.668507f, -0.1638423f, 0.2393992f, 0.6847935f); scale = new Vector3(0.7940578f, 0.9999996f, 0.8216049f); break; //RightTrapezius
				case 152: position = new Vector3(-0.04670746f, -2.449114e-09f, -0.002234854f); rotation = new Quaternion(3.164382e-08f, 1.276717e-08f, -2.102628e-09f, 1f); scale = new Vector3(0.9862821f, 0.9313464f, 0.8511607f); break; //Spine1Adjust
				case 153: position = new Vector3(-0.0317933f, -1.073057e-08f, 0.0142984f); rotation = new Quaternion(-2.38266e-08f, 0.03991292f, -2.544793e-09f, 0.9992031f); scale = new Vector3(0.9999999f, 0.9613994f, 1.053595f); break; //SpineAdjust
				case 154: position = new Vector3(0.01727388f, 0.11026f, 0.002575177f); rotation = new Quaternion(-0.05593695f, 0.07565493f, 0.9947171f, -0.04105304f); scale = new Vector3(1.011323f, 1.011326f, 1.011325f); break; //RightUpLeg
				case 155: position = new Vector3(-0.01492859f, 0.0001192498f, -0.02402661f); rotation = new Quaternion(0.08620527f, 0.8699771f, -0.03441303f, -0.484277f); scale = new Vector3(1.410761f, 1.069324f, 1.551869f); break; //RightGluteus
				case 156: position = new Vector3(-0.4319516f, 3.063632e-05f, -1.426786e-06f); rotation = new Quaternion(0.04355231f, -0.02896579f, 0.0301689f, 0.9981754f); scale = new Vector3(1.033334f, 1.033334f, 1.033334f); break; //RightLeg
				case 157: position = new Vector3(-0.4746964f, 0.001139034f, 0.000254943f); rotation = new Quaternion(0.8610623f, 0.01862771f, -0.5024656f, -0.07584838f); scale = new Vector3(0.9530382f, 0.9366617f, 0.9705341f); break; //RightFoot
				case 158: position = new Vector3(-0.1844993f, 0.0002449686f, 0.0006477516f); rotation = new Quaternion(0.0624668f, -0.206747f, 0.02363304f, 0.9761122f); scale = new Vector3(0.9727217f, 0.9639587f, 0.9516408f); break; //RightToeBase
				case 159: position = new Vector3(-0.1255218f, -0.005277444f, -4.656613e-09f); rotation = new Quaternion(9.144444e-08f, -7.549408e-09f, -2.831027e-09f, 1f); scale = new Vector3(0.9869093f, 0.870781f, 0.9315838f); break; //RightLegAdjust
				case 160: position = new Vector3(-0.1096653f, -0.003118347f, -0.001990629f); rotation = new Quaternion(0.03335007f, -1.958594e-09f, -4.950562e-09f, 0.9994438f); scale = new Vector3(1.039189f, 0.9277276f, 0.9026475f); break; //RightUpLegAdjust

				default: position = Vector3.zero; rotation = Quaternion.identity; scale = Vector3.one; break;
			}
			transform.localPosition = position;
			transform.localRotation = rotation;
			transform.localScale = scale;
		}

		#endregion FeminineMale

		#region AndroToMale

		private static void AndroToMaleLocalTranslation(int index, Transform transform)
		{
			Vector3 position; Quaternion rotation; Vector3 scale;

			switch (index)
			{
				case 0: position = new Vector3(-2.869633e-07f, 1.035215f, -0.01893938f); rotation = new Quaternion(0.0390729f, 0.03907251f, -0.7060263f, 0.7060266f); scale = new Vector3(1.000001f, 1.000001f, 1f); break; //Hips
				case 1: position = new Vector3(0.01757943f, -0.1097341f, 0.003165536f); rotation = new Quaternion(-0.06121299f, -0.07603502f, 0.9937666f, 0.053848f); scale = new Vector3(0.9888186f, 0.98882f, 0.9888206f); break; //LeftUpLeg
				case 2: position = new Vector3(0.008598798f, -0.01052568f, -0.0116877f); rotation = new Quaternion(-0.08817404f, 0.8840569f, 0.03239201f, -0.4578423f); scale = new Vector3(0.9442272f, 0.6027607f, 0.6096552f); break; //LeftGluteus
				case 3: position = new Vector3(-0.4368477f, 3.026798e-08f, -2.793968e-09f); rotation = new Quaternion(-0.04395427f, -0.04038906f, -0.02174824f, 0.9979799f); scale = new Vector3(0.967737f, 0.9677368f, 0.9677371f); break; //LeftLeg
				case 4: position = new Vector3(-0.4971371f, 2.328306e-08f, 2.328306e-09f); rotation = new Quaternion(0.8604598f, -0.01496631f, -0.5039513f, 0.07360759f); scale = new Vector3(1.060915f, 1.063377f, 1.063377f); break; //LeftFoot
				case 5: position = new Vector3(-0.1806862f, 3.026798e-09f, 5.587935e-09f); rotation = new Quaternion(-0.06246547f, -0.2067474f, -0.02363384f, 0.9761122f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftToeBase
				case 6: position = new Vector3(-0.1187908f, -0.004398271f, 7.078052e-08f); rotation = new Quaternion(1.359534e-07f, -3.352762e-08f, 2.048911e-08f, 1f); scale = new Vector3(0.9999998f, 1.078923f, 0.9999998f); break; //LeftLegAdjust
				case 7: position = new Vector3(-0.1092126f, 0.0002387969f, 0.003574749f); rotation = new Quaternion(-0.03335004f, -6.898272e-10f, 3.5451e-08f, 0.9994438f); scale = new Vector3(0.9448369f, 1.096577f, 1.112463f); break; //LeftUpLegAdjust
				case 8: position = new Vector3(-0.05387873f, 4.067731e-09f, -7.450576e-09f); rotation = new Quaternion(5.486466e-08f, -0.07920212f, 3.437415e-08f, 0.9968586f); scale = new Vector3(1.05f, 1.05f, 1.05f); break; //LowerBack
				case 9: position = new Vector3(-0.05296207f, -8.01089e-10f, 0.005279436f); rotation = new Quaternion(2.218179e-08f, 9.313246e-09f, 1.084116e-09f, 1f); scale = new Vector3(1f, 1.028719f, 1.112141f); break; //LowerBackAdjust
				case 10: position = new Vector3(-0.04439138f, 4.009746e-09f, 0.00624337f); rotation = new Quaternion(6.584373e-08f, 0.04690221f, 2.925361e-09f, 0.9988995f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LowerBackBelly
				case 11: position = new Vector3(-0.1835814f, 1.106442e-08f, -0.000119444f); rotation = new Quaternion(-5.358099e-07f, -0.08683414f, 1.383216e-08f, 0.9962228f); scale = new Vector3(0.9876471f, 0.9876471f, 0.9876471f); break; //Spine
				case 12: position = new Vector3(-0.1771947f, 1.338729e-08f, -2.054125e-05f); rotation = new Quaternion(4.719643e-07f, 0.1107154f, -6.31588e-10f, 0.9938522f); scale = new Vector3(0.9779912f, 0.9779912f, 0.9779912f); break; //Spine1
				case 13: position = new Vector3(-0.05188591f, -0.0644872f, 0.1195359f); rotation = new Quaternion(0.197523f, 0.6651203f, 0.1880069f, 0.6951641f); scale = new Vector3(0.4285056f, 1.130439f, 1.340025f); break; //LeftOuterBreast
				case 14: position = new Vector3(0.06317735f, 0.04393394f, 0.003867473f); rotation = new Quaternion(0.02537104f, -0.0511229f, 0.008944011f, 0.99833f); scale = new Vector3(0.845012f, 0.9999999f, 0.7998482f); break; //LeftInnerBreast
				case 15: position = new Vector3(-0.07539923f, -0.02949737f, 0.01545639f); rotation = new Quaternion(-0.00521056f, 0.01911828f, -0.07283685f, 0.997147f); scale = new Vector3(1.476293f, 1.541877f, 1.13733f); break; //LeftInnerBreastAdjust
				case 16: position = new Vector3(-0.05254603f, -0.03157478f, 0.01708928f); rotation = new Quaternion(-0.0002255912f, -0.001902677f, -0.1232787f, 0.9923702f); scale = new Vector3(1.763428f, 0.7010266f, 0.6821644f); break; //LeftOuterBreastAdjust
				case 17: position = new Vector3(-0.1870942f, -0.05235646f, 0.003283408f); rotation = new Quaternion(0.03935621f, -0.03185342f, 0.8037987f, 0.5927429f); scale = new Vector3(1.122026f, 1.122026f, 1.122026f); break; //LeftShoulder
				case 18: position = new Vector3(-0.1421482f, 7.581778e-05f, 1.795776e-05f); rotation = new Quaternion(-0.01612188f, -0.03699319f, 0.2370079f, 0.9706693f); scale = new Vector3(0.8876686f, 0.8876686f, 0.8876687f); break; //LeftArm
				case 19: position = new Vector3(-0.08155768f, -0.002074815f, 0.002231427f); rotation = new Quaternion(0.2379592f, -2.0038e-07f, 3.399201e-07f, 0.9712752f); scale = new Vector3(0.9363551f, 1.206221f, 1.254584f); break; //LeftArmAdjust
				case 20: position = new Vector3(-0.2838204f, 0.0003530206f, 0.001950186f); rotation = new Quaternion(0.04546796f, 0.1122502f, -0.0527983f, 0.9912341f); scale = new Vector3(0.9412726f, 0.9412727f, 0.9412727f); break; //LeftForeArm
				case 21: position = new Vector3(-0.03402921f, -0.002182027f, -0.002496926f); rotation = new Quaternion(4.91734e-05f, -4.216838e-08f, -2.51419e-07f, 1f); scale = new Vector3(1.104614f, 1.240167f, 1.252761f); break; //LeftForeArmAdjust
				case 22: position = new Vector3(-0.1475396f, 7.339986e-08f, -7.264316e-08f); rotation = new Quaternion(3.530877e-07f, 2.328296e-08f, -2.838642e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftForeArmTwist
				case 23: position = new Vector3(-0.02798362f, 3.241585e-07f, -3.72529e-08f); rotation = new Quaternion(4.881795e-05f, -4.189247e-08f, 3.49117e-07f, 1f); scale = new Vector3(1f, 1.284209f, 1.127772f); break; //LeftForeArmTwistAdjust
				case 24: position = new Vector3(-0.2899117f, 0.002017419f, -0.002999064f); rotation = new Quaternion(-0.7203866f, 0.004556083f, 0.01648103f, 0.693362f); scale = new Vector3(1.076636f, 1.076636f, 1.076636f); break; //LeftHand
				case 25: position = new Vector3(-0.08562455f, 0.0288644f, -0.004819877f); rotation = new Quaternion(-0.2131754f, -0.1088874f, -0.1204924f, 0.9634218f); scale = new Vector3(1.051542f, 1.051541f, 1.051541f); break; //LeftHandFinger01_01
				case 26: position = new Vector3(-0.03693631f, 2.235174e-08f, 2.607703e-08f); rotation = new Quaternion(0.01591757f, -0.09879856f, -0.02045996f, 0.9947698f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger01_02
				case 27: position = new Vector3(-0.02210764f, 0f, -3.352761e-08f); rotation = new Quaternion(-0.03146789f, -0.0471193f, 0.003357279f, 0.9983879f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger01_03
				case 28: position = new Vector3(-0.09440732f, 0.01310644f, 0.002081478f); rotation = new Quaternion(-0.1673073f, -0.1065686f, -0.05467334f, 0.9786022f); scale = new Vector3(1.009577f, 1.009577f, 1.009577f); break; //LeftHandFinger02_01
				case 29: position = new Vector3(-0.03892585f, 1.490116e-08f, 9.313226e-08f); rotation = new Quaternion(0.004608929f, -0.1096315f, 0.008167251f, 0.9939281f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger02_02
				case 30: position = new Vector3(-0.03213901f, -2.980232e-08f, 5.587935e-08f); rotation = new Quaternion(0.009086284f, -0.06614654f, -0.01302745f, 0.9976835f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger02_03
				case 31: position = new Vector3(-0.0960136f, -0.01140287f, 0.004487466f); rotation = new Quaternion(-0.07880175f, -0.09420983f, 0.01609626f, 0.9922982f); scale = new Vector3(1.008291f, 1.008291f, 1.008291f); break; //LeftHandFinger03_01
				case 32: position = new Vector3(-0.04725773f, -9.313226e-09f, -2.700835e-08f); rotation = new Quaternion(0.00287209f, -0.0968427f, -0.01627374f, 0.9951625f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger03_02
				case 33: position = new Vector3(-0.04042111f, -2.235174e-08f, -6.519258e-09f); rotation = new Quaternion(0.01665003f, -0.016046f, 0.0131781f, 0.9996458f); scale = new Vector3(0.9999999f, 0.9999998f, 0.9999999f); break; //LeftHandFinger03_03
				case 34: position = new Vector3(-0.09014441f, -0.04207738f, 0.003370518f); rotation = new Quaternion(-0.03526437f, -0.05045871f, 0.03795644f, 0.9973814f); scale = new Vector3(1.036889f, 1.036889f, 1.036889f); break; //LeftHandFinger04_01
				case 35: position = new Vector3(-0.05416486f, 3.72529e-09f, -5.727634e-08f); rotation = new Quaternion(0.00267789f, -0.07985284f, -0.00438922f, 0.9967934f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftHandFinger04_02
				case 36: position = new Vector3(-0.03353674f, 1.676381e-08f, -8.498318e-08f); rotation = new Quaternion(-0.001482372f, -0.0727025f, -0.01461353f, 0.9972456f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftHandFinger04_03
				case 37: position = new Vector3(-0.03973547f, -0.0475435f, -0.01802397f); rotation = new Quaternion(0.5469622f, -0.06623587f, 0.4483427f, 0.7038708f); scale = new Vector3(1.03566f, 1.03566f, 1.03566f); break; //LeftHandFinger05_01
				case 38: position = new Vector3(-0.02932131f, 3.480818e-08f, 5.029142e-08f); rotation = new Quaternion(-0.04490694f, -0.03217196f, -0.02465373f, 0.9981686f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger05_02
				case 39: position = new Vector3(-0.03140649f, -9.324867e-08f, -1.44355e-08f); rotation = new Quaternion(0.0249025f, -0.0553872f, -0.03426434f, 0.9975661f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftHandFinger05_03
				case 40: position = new Vector3(-0.03552751f, -0.003966989f, 0.01309114f); rotation = new Quaternion(0.1465889f, 6.897611e-08f, 2.443101e-07f, 0.9891975f); scale = new Vector3(0.8834071f, 0.9999999f, 1.035636f); break; //LeftShoulderAdjust
				case 41: position = new Vector3(-0.02937959f, 0.03733882f, -0.02771259f); rotation = new Quaternion(0.7010735f, -0.03553775f, -0.1068395f, 0.7041438f); scale = new Vector3(0.9652858f, 0.6090727f, 0.9652857f); break; //LeftTrapezius
				case 42: position = new Vector3(-0.1943359f, 1.467203e-08f, 7.966344e-08f); rotation = new Quaternion(-3.055475e-07f, 0.090723f, -2.617269e-08f, 0.9958762f); scale = new Vector3(0.9999999f, 1f, 1f); break; //Neck
				case 43: position = new Vector3(-0.1008215f, 7.611811e-09f, -1.117587e-08f); rotation = new Quaternion(1.246556e-07f, -0.0831603f, 2.214955e-08f, 0.9965362f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //Head
				case 44: position = new Vector3(-0.05122195f, 4.101578e-09f, 0.002362607f); rotation = new Quaternion(-4.961743e-08f, 1.536684e-08f, 7.624631e-16f, 1f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //HeadAdjust
				case 45: position = new Vector3(0.008456777f, -4.078373e-07f, 0.0653144f); rotation = new Quaternion(-1.87685e-05f, 0.7009441f, 1.840287e-05f, 0.7132162f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //InnerMouthUpperAdjust
				case 46: position = new Vector3(0.04045396f, -5.746359e-09f, 0.005567867f); rotation = new Quaternion(-1.140045e-08f, 0.05074796f, 1.588811e-08f, 0.9987115f); scale = new Vector3(0.9999999f, 1.100628f, 1.103876f); break; //UpperNeckAdjust
				case 47: position = new Vector3(-0.08529599f, -0.02830114f, 0.1092517f); rotation = new Quaternion(0.668591f, 0.2419273f, -0.660424f, -0.2414487f); scale = new Vector3(0.9999997f, 1f, 0.9999999f); break; //LeftCheek
				case 48: position = new Vector3(6.868504e-08f, 0f, 3.274181e-08f); rotation = new Quaternion(-5.308497e-08f, -2.741057e-06f, 1.553691e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftCheekAdjust
				case 49: position = new Vector3(-0.07404087f, -0.08116501f, 0.05017164f); rotation = new Quaternion(-0.1500117f, -0.5308654f, 0.134985f, 0.823078f); scale = new Vector3(0.941514f, 0.9415138f, 0.9415139f); break; //LeftEar
				case 50: position = new Vector3(-1.844019e-07f, -2.561137e-09f, -4.563481e-08f); rotation = new Quaternion(-1.341104e-07f, 1.706732e-07f, -5.872624e-08f, 1f); scale = new Vector3(1.014957f, 1f, 1.110196f); break; //LeftEarAdjust
				case 51: position = new Vector3(-0.1051326f, -0.03445163f, 0.1163351f); rotation = new Quaternion(0.7124566f, -3.090618e-06f, -0.7017161f, -4.222129e-06f); scale = new Vector3(1f, 1f, 1f); break; //LeftEye
				case 52: position = new Vector3(1.279918e-07f, -1.278781e-07f, 0.0002264237f); rotation = new Quaternion(2.147789e-06f, 3.591572e-06f, 1.104331e-08f, 1f); scale = new Vector3(1f, 0.9630773f, 0.8122306f); break; //LeftEyeAdjust
				case 53: position = new Vector3(4.775694e-08f, -7.431987e-08f, -3.123796e-07f); rotation = new Quaternion(4.362493e-06f, -3.746246e-06f, 3.239953e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeGlobe
				case 54: position = new Vector3(4.775694e-08f, -7.431987e-08f, -3.123796e-07f); rotation = new Quaternion(0.000275448f, -0.1797692f, 1.183602e-05f, 0.9837088f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftLowerLid
				case 55: position = new Vector3(6.917571e-08f, 3.688001e-09f, -1.59198e-08f); rotation = new Quaternion(0.0002783753f, -0.0004685013f, -3.686531e-05f, 0.9999999f); scale = new Vector3(1f, 1f, 1f); break; //LeftLowerLidAdjust
				case 56: position = new Vector3(4.775694e-08f, -7.431987e-08f, -3.123796e-07f); rotation = new Quaternion(-0.0003545062f, 0.09078033f, 2.501597e-05f, 0.9958709f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftUpperLid
				case 57: position = new Vector3(1.651711e-08f, 4.797585e-09f, -1.095759e-08f); rotation = new Quaternion(-0.0003493552f, 0.0003598988f, -3.604179e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftUpperLidAdjust
				case 58: position = new Vector3(-0.1132819f, -0.01875659f, 0.08099259f); rotation = new Quaternion(0.776783f, -2.089002e-07f, -0.6297684f, 2.205573e-07f); scale = new Vector3(0.9828497f, 0.9828499f, 0.9828498f); break; //LeftEyebrowLow
				case 59: position = new Vector3(-8.679294e-09f, 9.966959e-10f, 2.139526e-07f); rotation = new Quaternion(-7.424768e-08f, 7.674099e-07f, -1.963216e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 0.8015924f); break; //LeftEyebrowLowAdjust
				case 60: position = new Vector3(-0.1106327f, -0.0361581f, 0.08008403f); rotation = new Quaternion(0.7770706f, -1.324497e-07f, -0.6294134f, 2.13005e-07f); scale = new Vector3(0.989868f, 0.9898683f, 0.9898682f); break; //LeftEyebrowMiddle
				case 61: position = new Vector3(2.21201e-08f, 6.921719e-09f, -8.845474e-08f); rotation = new Quaternion(-3.736196e-08f, 1.66893e-06f, 4.80071e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftEyebrowMiddleAdjust
				case 62: position = new Vector3(-0.1114992f, -0.04866679f, 0.06958223f); rotation = new Quaternion(0.7706838f, -0.0004759134f, -0.6372175f, 0.0003220743f); scale = new Vector3(0.9870015f, 0.9870016f, 0.9870016f); break; //LeftEyebrowUp
				case 63: position = new Vector3(-1.097178e-07f, 3.823516e-09f, 5.699803e-09f); rotation = new Quaternion(-3.685601e-07f, 1.024066e-06f, 2.105336e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftEyebrowUpAdjust
				case 64: position = new Vector3(-0.03237071f, -0.01068394f, 0.1417866f); rotation = new Quaternion(0.7940923f, 0.05977427f, -0.6030683f, -0.04640191f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLipsSuperiorMiddle
				case 65: position = new Vector3(-8.35862e-08f, -5.587935e-09f, 7.139897e-08f); rotation = new Quaternion(-0.0001272972f, 1.57864e-05f, 4.550979e-07f, 1f); scale = new Vector3(1f, 0.9999998f, 1f); break; //LeftLipsSuperiorMiddleAdjust
				case 66: position = new Vector3(-0.0305156f, -0.03721048f, 0.1069717f); rotation = new Quaternion(-0.6591188f, -0.2391572f, 0.6702258f, 0.2432356f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftLowCheek
				case 67: position = new Vector3(-4.097819e-08f, -1.490116e-08f, -7.338667e-08f); rotation = new Quaternion(-2.776596e-07f, -5.602449e-09f, -1.354783e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLowCheekAdjust
				case 68: position = new Vector3(-0.06030155f, -0.02549989f, 0.1017974f); rotation = new Quaternion(0.7088718f, 0.001025832f, -0.7053237f, 0.004268806f); scale = new Vector3(0.9999997f, 1f, 0.9999999f); break; //LeftNose
				case 69: position = new Vector3(8.31933e-08f, 2.319575e-08f, 2.725574e-08f); rotation = new Quaternion(-0.000355426f, -1.413888e-06f, -3.242715e-10f, 0.9999999f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftNoseAdjust
				case 70: position = new Vector3(-0.03299574f, -9.259608e-05f, 0.1435691f); rotation = new Quaternion(0.7874868f, -2.399032e-07f, -0.6163315f, -1.471337e-07f); scale = new Vector3(1f, 1f, 1f); break; //LipsSuperior
				case 71: position = new Vector3(2.234421e-08f, 9.907e-12f, 8.74974e-08f); rotation = new Quaternion(-7.338064e-07f, -6.288291e-06f, 1.251337e-09f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LipsSuperiorAdjust
				case 72: position = new Vector3(-0.04355703f, -9.260806e-05f, 0.05288088f); rotation = new Quaternion(-0.4972367f, 1.621633e-07f, 0.8676149f, 1.535314e-08f); scale = new Vector3(0.9164581f, 0.916458f, 0.9164581f); break; //Mandible
				case 73: position = new Vector3(-0.02885163f, -0.0001008721f, -0.01793071f); rotation = new Quaternion(0.9627837f, 3.954323e-06f, -0.2702732f, 1.339241e-05f); scale = new Vector3(1.139501f, 1f, 0.9999999f); break; //InnerMouthLowerAdjust
				case 74: position = new Vector3(-0.07762001f, 0.02651781f, 0.02348616f); rotation = new Quaternion(-0.02273114f, 0.2682541f, -0.08131648f, 0.9596409f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftLips
				case 75: position = new Vector3(-1.205291e-08f, -9.445671e-09f, -2.470337e-07f); rotation = new Quaternion(2.632683e-07f, -7.450574e-08f, -2.533197e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsAdjust
				case 76: position = new Vector3(-0.09137159f, 0.01119224f, 0.03060827f); rotation = new Quaternion(-0.01443659f, 0.180118f, -0.08480807f, 0.9798759f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftLipsInferior
				case 77: position = new Vector3(1.001172e-08f, 8.381903e-09f, 2.368452e-07f); rotation = new Quaternion(2.614339e-06f, 5.395557e-07f, -9.10382e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsInferiorAdjust
				case 78: position = new Vector3(-0.03433985f, 0.06106211f, -0.0290124f); rotation = new Quaternion(0.03456119f, 0.1041521f, 0.2715414f, 0.9561502f); scale = new Vector3(1.082606f, 1.082606f, 1.082606f); break; //LeftLowMaxilar
				case 79: position = new Vector3(5.401671e-08f, 9.31323e-10f, -1.339358e-07f); rotation = new Quaternion(-3.683381e-07f, -9.716543e-08f, -3.578974e-14f, 1f); scale = new Vector3(0.8866048f, 0.9999999f, 1f); break; //LeftLowMaxilarAdjust
				case 80: position = new Vector3(-0.09283529f, 2.429827e-08f, 0.03085482f); rotation = new Quaternion(-3.542938e-08f, 0.1730407f, -4.849679e-08f, 0.9849147f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LipsInferior
				case 81: position = new Vector3(7.440881e-09f, 9.42e-12f, -1.341016e-07f); rotation = new Quaternion(-3.623293e-07f, -6.966293e-06f, 4.55557e-10f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LipsInferiorAdjust
				case 82: position = new Vector3(-2.607471e-08f, -1.701e-12f, 8.195929e-08f); rotation = new Quaternion(-8.661768e-08f, -4.470348e-07f, 4.553216e-11f, 1f); scale = new Vector3(1f, 1f, 1f); break; //MandibleAdjust
				case 83: position = new Vector3(-0.07761999f, -0.02671994f, 0.02348614f); rotation = new Quaternion(0.0227311f, 0.2682542f, 0.0813167f, 0.9596409f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLips
				case 84: position = new Vector3(-1.65989e-08f, -8.31449e-10f, -2.595594e-08f); rotation = new Quaternion(-1.63426e-07f, -1.639127e-07f, 1.41561e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightLipsAdjust
				case 85: position = new Vector3(-0.09137159f, -0.01139436f, 0.03060827f); rotation = new Quaternion(0.01443661f, 0.1801181f, 0.08480798f, 0.9798759f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLipsInferior
				case 86: position = new Vector3(-7.450581e-09f, 1.862645e-09f, 9.138603e-08f); rotation = new Quaternion(-2.65392e-06f, 4.188476e-07f, 7.450692e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLipsInferiorAdjust
				case 87: position = new Vector3(-0.03433998f, -0.06126432f, -0.02901232f); rotation = new Quaternion(-0.03456114f, 0.1041518f, -0.2715417f, 0.9561502f); scale = new Vector3(1.082606f, 1.082607f, 1.082607f); break; //RightLowMaxilar
				case 88: position = new Vector3(-6.332994e-08f, -8.381903e-08f, -8.934876e-08f); rotation = new Quaternion(3.608875e-07f, -5.514793e-08f, 6.722799e-08f, 1f); scale = new Vector3(0.8866048f, 1f, 1f); break; //RightLowMaxilarAdjust
				case 89: position = new Vector3(-0.02111498f, -0.0001009186f, -0.0186484f); rotation = new Quaternion(2.000554e-06f, 0.2488658f, -7.697171e-07f, 0.968538f); scale = new Vector3(1.074816f, 1.074816f, 1.074816f); break; //Tongue01
				case 90: position = new Vector3(-0.02827539f, 2.133717e-09f, 7.788119e-08f); rotation = new Quaternion(0.998992f, 5.321574e-08f, -0.04489022f, 7.171173e-06f); scale = new Vector3(1f, 1f, 1f); break; //Tongue02
				case 91: position = new Vector3(-0.06618345f, -9.258863e-05f, 0.1471071f); rotation = new Quaternion(-0.6986089f, 1.504492e-07f, 0.7155038f, -9.764695e-08f); scale = new Vector3(1.050627f, 1f, 1.019372f); break; //NoseBase
				case 92: position = new Vector3(-4.096936e-08f, 7.451e-12f, -1.126551e-07f); rotation = new Quaternion(-3.296439e-08f, -0.0006734804f, 6.408464e-09f, 0.9999998f); scale = new Vector3(1f, 1f, 1f); break; //NoseBaseAdjust
				case 93: position = new Vector3(-0.08948416f, 7.007872e-06f, 0.1380562f); rotation = new Quaternion(-0.67835f, -0.001225236f, 0.7347358f, 0.001756625f); scale = new Vector3(1f, 1f, 1f); break; //NoseMiddle
				case 94: position = new Vector3(-7.669062e-09f, -8.370989e-09f, 1.083697e-07f); rotation = new Quaternion(-0.0001302077f, 0.0004997613f, -0.00091017f, 0.9999995f); scale = new Vector3(1.341125f, 0.9999997f, 0.9999999f); break; //NoseMiddleAdjust
				case 95: position = new Vector3(-0.1214713f, -8.390219e-05f, 0.1187488f); rotation = new Quaternion(0.7095863f, 0.00620659f, -0.7045732f, 0.005031887f); scale = new Vector3(0.9999997f, 0.9999999f, 0.9999998f); break; //NoseTop
				case 96: position = new Vector3(9.51475e-08f, 3.776222e-09f, -1.864803e-07f); rotation = new Quaternion(3.415264e-05f, 3.753779e-08f, -2.035214e-08f, 1f); scale = new Vector3(0.9999999f, 0.4417388f, 0.6473341f); break; //NoseTopAdjust
				case 97: position = new Vector3(-0.08529598f, 0.02830117f, 0.1092517f); rotation = new Quaternion(0.6685911f, -0.2419269f, -0.6604241f, 0.2414483f); scale = new Vector3(1f, 1f, 1f); break; //RightCheek
				case 98: position = new Vector3(4.051253e-08f, -1.117587e-08f, -8.562347e-08f); rotation = new Quaternion(2.149025e-07f, -2.761313e-06f, -6.613877e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightCheekAdjust
				case 99: position = new Vector3(-0.07404074f, 0.08116524f, 0.0501716f); rotation = new Quaternion(0.1500112f, -0.5308657f, -0.1349847f, 0.823078f); scale = new Vector3(0.941514f, 0.9415138f, 0.941514f); break; //RightEar
				case 100: position = new Vector3(-9.592623e-08f, 1.559965e-08f, 1.862645e-07f); rotation = new Quaternion(2.458691e-07f, 2.587626e-07f, 7.616056e-08f, 1f); scale = new Vector3(1.014957f, 1f, 1.110196f); break; //RightEarAdjust
				case 101: position = new Vector3(-0.1051326f, 0.03445169f, 0.1163351f); rotation = new Quaternion(0.7124566f, 3.103892e-06f, -0.7017161f, 4.019454e-06f); scale = new Vector3(1f, 1f, 1f); break; //RightEye
				case 102: position = new Vector3(1.115431e-07f, 7.30173e-08f, 0.0002264236f); rotation = new Quaternion(-2.138464e-06f, 3.595294e-06f, 1.704018e-08f, 1f); scale = new Vector3(1f, 0.9630773f, 0.8122306f); break; //RightEyeAdjust
				case 103: position = new Vector3(9.093758e-08f, 9.396844e-08f, -3.124796e-07f); rotation = new Quaternion(-4.377738e-06f, -3.742516e-06f, -1.411582e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyeGlobe
				case 104: position = new Vector3(6.858579e-08f, 4.553969e-08f, -3.124802e-07f); rotation = new Quaternion(-0.0002752239f, -0.1797691f, -1.187015e-05f, 0.9837088f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightLowerLid
				case 105: position = new Vector3(1.131389e-08f, 3.757123e-09f, 8.128882e-08f); rotation = new Quaternion(-0.0002784951f, -0.0004620638f, 3.697522e-05f, 0.9999999f); scale = new Vector3(1f, 0.9999998f, 0.9999999f); break; //RightLowerLidAdjust
				case 106: position = new Vector3(9.093758e-08f, 9.396844e-08f, -3.124796e-07f); rotation = new Quaternion(0.0003545306f, 0.09077971f, -2.559061e-05f, 0.9958709f); scale = new Vector3(1f, 1f, 1f); break; //RightUpperLid
				case 107: position = new Vector3(-5.012942e-08f, -1.594344e-09f, 2.195211e-07f); rotation = new Quaternion(0.0003495335f, 0.0003604421f, 3.613243e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightUpperLidAdjust
				case 108: position = new Vector3(-0.1132819f, 0.01875665f, 0.08099262f); rotation = new Quaternion(0.776783f, -9.974304e-08f, -0.6297684f, -4.541823e-08f); scale = new Vector3(0.9828497f, 0.9828499f, 0.9828498f); break; //RightEyebrowLow
				case 109: position = new Vector3(9.653992e-08f, -2.61493e-09f, 2.297177e-07f); rotation = new Quaternion(-5.188788e-08f, 6.556511e-07f, -6.06393e-09f, 1f); scale = new Vector3(1f, 1f, 0.8015924f); break; //RightEyebrowLowAdjust
				case 110: position = new Vector3(-0.1144877f, 0.03615817f, 0.07926556f); rotation = new Quaternion(0.7770707f, 6.49154e-08f, -0.6294133f, 5.258031e-08f); scale = new Vector3(0.9898681f, 0.9898683f, 0.9898682f); break; //RightEyebrowMiddle
				case 111: position = new Vector3(-3.334123e-08f, 1.311133e-08f, -4.77601e-08f); rotation = new Quaternion(1.686619e-07f, 1.594424e-06f, 2.032359e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.8914973f); break; //RightEyebrowMiddleAdjust
				case 112: position = new Vector3(-0.1114991f, 0.04866685f, 0.06958219f); rotation = new Quaternion(0.7706839f, 0.0004757868f, -0.6372174f, -0.0003222454f); scale = new Vector3(0.9870017f, 0.9870016f, 0.9870017f); break; //RightEyebrowUp
				case 113: position = new Vector3(-2.744855e-08f, 2.408342e-09f, 4.07058e-08f); rotation = new Quaternion(-2.608722e-07f, 9.045534e-07f, 2.074763e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightEyebrowUpAdjust
				case 114: position = new Vector3(-0.0323707f, 0.01068401f, 0.1417866f); rotation = new Quaternion(0.7940922f, -0.05977428f, -0.6030683f, 0.04640234f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightLipsSuperiorMiddle
				case 115: position = new Vector3(-5.972106e-08f, 1.676381e-08f, -4.680624e-08f); rotation = new Quaternion(0.0001277112f, 1.5763e-05f, -3.54053e-07f, 1f); scale = new Vector3(1f, 0.9999998f, 1f); break; //RightLipsSuperiorMiddleAdjust
				case 116: position = new Vector3(-0.03051559f, 0.03721053f, 0.1069717f); rotation = new Quaternion(-0.6591188f, 0.2391578f, 0.6702257f, -0.2432358f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999998f); break; //RightLowCheek
				case 117: position = new Vector3(-2.421439e-08f, 1.676381e-08f, 3.830178e-08f); rotation = new Quaternion(1.882799e-07f, -2.345045e-08f, -2.476445e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightLowCheekAdjust
				case 118: position = new Vector3(-0.06030155f, 0.02549991f, 0.1017973f); rotation = new Quaternion(0.7088718f, -0.001020587f, -0.7053237f, -0.004262988f); scale = new Vector3(0.9999997f, 1f, 0.9999999f); break; //RightNose
				case 119: position = new Vector3(8.083589e-09f, -1.717126e-08f, 2.588786e-08f); rotation = new Quaternion(0.0003646003f, -1.413435e-06f, 4.462546e-09f, 0.9999999f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightNoseAdjust
				case 120: position = new Vector3(-0.04493667f, -9.259889e-05f, 0.1153655f); rotation = new Quaternion(0.7071069f, 9.415695e-08f, -0.7071067f, 9.415697e-08f); scale = new Vector3(0.9999998f, 0.7660732f, 0.9999998f); break; //UpperLips
				case 121: position = new Vector3(2.422353e-08f, -7.14e-12f, 1.014252e-08f); rotation = new Quaternion(2.152962e-09f, 1.522247e-06f, -9.582118e-11f, 1f); scale = new Vector3(1f, 1f, 1f); break; //UpperLipsAdjust
				case 122: position = new Vector3(-0.03352369f, -7.639261e-09f, 0.00444385f); rotation = new Quaternion(4.328999e-07f, 1.575112e-08f, 1.028798e-08f, 1f); scale = new Vector3(0.9460385f, 1.203868f, 1.189637f); break; //NeckAdjust
				case 123: position = new Vector3(-0.05188591f, 0.06448738f, 0.1195358f); rotation = new Quaternion(-0.1975226f, 0.6651201f, -0.188008f, 0.6951641f); scale = new Vector3(0.4285056f, 1.130439f, 1.340025f); break; //RightOuterBreast
				case 124: position = new Vector3(0.06317739f, -0.04393397f, 0.003867495f); rotation = new Quaternion(-0.02537197f, -0.05112289f, -0.008943993f, 0.99833f); scale = new Vector3(0.8450121f, 0.9999997f, 0.7998482f); break; //RightInnerBreast
				case 125: position = new Vector3(-0.07539925f, 0.02949741f, 0.01545613f); rotation = new Quaternion(0.005210661f, 0.01911835f, 0.0728368f, 0.997147f); scale = new Vector3(1.476292f, 1.541876f, 1.137329f); break; //RightInnerBreastAdjust
				case 126: position = new Vector3(-0.05254608f, 0.03157486f, 0.01708918f); rotation = new Quaternion(0.0002246754f, -0.001902549f, 0.1232785f, 0.9923702f); scale = new Vector3(1.763428f, 0.7010266f, 0.6821643f); break; //RightOuterBreastAdjust
				case 127: position = new Vector3(-0.1870941f, 0.05235649f, 0.003283343f); rotation = new Quaternion(-0.0393566f, -0.0318539f, -0.8037986f, 0.5927429f); scale = new Vector3(1.122026f, 1.122026f, 1.122026f); break; //RightShoulder
				case 128: position = new Vector3(-0.1421482f, -7.578905e-05f, 1.795962e-05f); rotation = new Quaternion(0.01612181f, -0.03699322f, -0.237008f, 0.9706693f); scale = new Vector3(0.8876687f, 0.8876687f, 0.8876687f); break; //RightArm
				case 129: position = new Vector3(-0.0815578f, 0.002074976f, 0.002231408f); rotation = new Quaternion(-0.2379599f, -4.476173e-07f, -3.339301e-07f, 0.971275f); scale = new Vector3(0.936355f, 1.20622f, 1.254583f); break; //RightArmAdjust
				case 130: position = new Vector3(-0.2838204f, -0.0003529424f, 0.001950189f); rotation = new Quaternion(-0.04546786f, 0.1122503f, 0.05279843f, 0.9912341f); scale = new Vector3(0.9412726f, 0.9412727f, 0.9412725f); break; //RightForeArm
				case 131: position = new Vector3(-0.03402895f, 0.002182208f, -0.002496701f); rotation = new Quaternion(-1.689672e-07f, 2.023488e-08f, 3.566137e-08f, 1f); scale = new Vector3(1.104614f, 1.240167f, 1.252761f); break; //RightForeArmAdjust
				case 132: position = new Vector3(-0.1475395f, -7.226481e-08f, -7.450581e-08f); rotation = new Quaternion(-3.468013e-07f, 1.490108e-08f, 2.543821e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightForeArmTwist
				case 133: position = new Vector3(-0.02798362f, -2.045999e-07f, -4.284084e-08f); rotation = new Quaternion(7.886671e-08f, 1.862642e-08f, -3.641325e-07f, 1f); scale = new Vector3(1f, 1.284209f, 1.127772f); break; //RightForeArmTwistAdjust
				case 134: position = new Vector3(-0.2899117f, -0.002017566f, -0.002999082f); rotation = new Quaternion(0.7203866f, 0.004555943f, -0.01648104f, 0.693362f); scale = new Vector3(1.076636f, 1.076636f, 1.076636f); break; //RightHand
				case 135: position = new Vector3(-0.08562454f, -0.02886437f, -0.004819821f); rotation = new Quaternion(0.2131754f, -0.1088874f, 0.1204924f, 0.9634218f); scale = new Vector3(1.051542f, 1.051541f, 1.051541f); break; //RightHandFinger01_01
				case 136: position = new Vector3(-0.03693636f, 7.450581e-09f, 7.450581e-08f); rotation = new Quaternion(-0.0159176f, -0.09879858f, 0.02045994f, 0.9947698f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightHandFinger01_02
				case 137: position = new Vector3(-0.02210763f, -2.235174e-08f, -1.490116e-08f); rotation = new Quaternion(0.03146786f, -0.04711928f, -0.00335727f, 0.9983879f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger01_03
				case 138: position = new Vector3(-0.09440731f, -0.01310642f, 0.002081565f); rotation = new Quaternion(0.1673073f, -0.1065687f, 0.05467337f, 0.9786022f); scale = new Vector3(1.009577f, 1.009577f, 1.009577f); break; //RightHandFinger02_01
				case 139: position = new Vector3(-0.03892581f, -7.450581e-09f, 2.235174e-08f); rotation = new Quaternion(-0.00460895f, -0.1096316f, -0.0081673f, 0.9939281f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger02_02
				case 140: position = new Vector3(-0.03213913f, 1.490116e-08f, 7.82311e-08f); rotation = new Quaternion(-0.009086299f, -0.06614654f, 0.01302745f, 0.9976835f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightHandFinger02_03
				case 141: position = new Vector3(-0.09601364f, 0.0114029f, 0.004487523f); rotation = new Quaternion(0.07880175f, -0.09420981f, -0.01609624f, 0.9922982f); scale = new Vector3(1.008291f, 1.008291f, 1.008291f); break; //RightHandFinger03_01
				case 142: position = new Vector3(-0.04725773f, -3.72529e-09f, -3.073364e-08f); rotation = new Quaternion(-0.002872097f, -0.09684272f, 0.01627373f, 0.9951625f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger03_02
				case 143: position = new Vector3(-0.04042111f, -1.117587e-08f, -1.117587e-08f); rotation = new Quaternion(-0.01665001f, -0.01604595f, -0.01317811f, 0.9996458f); scale = new Vector3(1f, 0.9999998f, 0.9999999f); break; //RightHandFinger03_03
				case 144: position = new Vector3(-0.09014445f, 0.04207741f, 0.003370577f); rotation = new Quaternion(0.03526435f, -0.05045868f, -0.03795648f, 0.9973814f); scale = new Vector3(1.036889f, 1.036889f, 1.036889f); break; //RightHandFinger04_01
				case 145: position = new Vector3(-0.05416489f, 1.490116e-08f, 6.146729e-08f); rotation = new Quaternion(-0.002677878f, -0.07985294f, 0.004389261f, 0.9967934f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger04_02
				case 146: position = new Vector3(-0.03353668f, 2.04891e-08f, -2.607703e-08f); rotation = new Quaternion(0.001482344f, -0.0727024f, 0.01461356f, 0.9972456f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger04_03
				case 147: position = new Vector3(-0.03973544f, 0.04754353f, -0.01802388f); rotation = new Quaternion(-0.5469622f, -0.06623568f, -0.4483426f, 0.7038708f); scale = new Vector3(1.03566f, 1.03566f, 1.03566f); break; //RightHandFinger05_01
				case 148: position = new Vector3(-0.02932117f, 3.678724e-08f, -6.705523e-08f); rotation = new Quaternion(0.04490699f, -0.03217189f, 0.02465365f, 0.9981686f); scale = new Vector3(0.9999999f, 0.9999998f, 1f); break; //RightHandFinger05_02
				case 149: position = new Vector3(-0.03140645f, -1.257285e-08f, -2.398156e-08f); rotation = new Quaternion(-0.02490252f, -0.05538733f, 0.03426433f, 0.9975661f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger05_03
				case 150: position = new Vector3(-0.03552739f, 0.00396692f, 0.01309114f); rotation = new Quaternion(-0.1465888f, 3.882349e-08f, -2.080467e-07f, 0.9891976f); scale = new Vector3(0.8834071f, 0.9999999f, 1.035636f); break; //RightShoulderAdjust
				case 151: position = new Vector3(-0.0293796f, -0.0373388f, -0.02771258f); rotation = new Quaternion(-0.7010735f, -0.03553776f, 0.1068396f, 0.7041438f); scale = new Vector3(0.9652857f, 0.6090727f, 0.9652857f); break; //RightTrapezius
				case 152: position = new Vector3(-0.0484516f, 7.97824e-10f, -0.00369499f); rotation = new Quaternion(3.278552e-08f, 0.01854811f, 5.519384e-09f, 0.999828f); scale = new Vector3(0.9999996f, 1.130518f, 1.16626f); break; //Spine1Adjust
				case 153: position = new Vector3(-0.04045773f, -8.76327e-10f, -0.01310641f); rotation = new Quaternion(-2.70409e-08f, -3.725291e-08f, 5.689828e-09f, 1f); scale = new Vector3(0.9999999f, 1.15073f, 1.085501f); break; //SpineAdjust
				case 154: position = new Vector3(0.01757969f, 0.1097342f, 0.003165425f); rotation = new Quaternion(-0.06121454f, 0.07604732f, 0.993755f, -0.05404336f); scale = new Vector3(0.9888187f, 0.9888258f, 0.9888283f); break; //RightUpLeg
				case 155: position = new Vector3(0.008598768f, 0.01052569f, -0.0116876f); rotation = new Quaternion(0.08817308f, 0.8840582f, -0.03239127f, -0.45784f); scale = new Vector3(0.9442273f, 0.6027609f, 0.6096551f); break; //RightGluteus
				case 156: position = new Vector3(-0.4368429f, 5.192123e-08f, -9.31323e-10f); rotation = new Quaternion(0.04395437f, -0.04038834f, 0.02174791f, 0.9979799f); scale = new Vector3(0.967737f, 0.967737f, 0.9677371f); break; //RightLeg
				case 157: position = new Vector3(-0.4971314f, 3.352761e-08f, -2.328306e-09f); rotation = new Quaternion(0.8604607f, 0.01496724f, -0.5039498f, -0.07360798f); scale = new Vector3(1.060915f, 1.063377f, 1.063377f); break; //RightFoot
				case 158: position = new Vector3(-0.1806838f, -2.32831e-10f, 1.862645e-09f); rotation = new Quaternion(0.06246535f, -0.2067474f, 0.02363343f, 0.9761122f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightToeBase
				case 159: position = new Vector3(-0.1187898f, 0.004398314f, -2.514571e-08f); rotation = new Quaternion(4.531779e-08f, -3.352762e-08f, -3.166497e-08f, 1f); scale = new Vector3(0.9999999f, 1.078924f, 0.9999998f); break; //RightLegAdjust
				case 160: position = new Vector3(-0.1092115f, -0.0002387818f, 0.003574651f); rotation = new Quaternion(0.03335009f, -7.09393e-09f, -2.417286e-08f, 0.9994438f); scale = new Vector3(0.9448368f, 1.096577f, 1.112463f); break; //RightUpLegAdjust

				default: position = Vector3.zero; rotation = Quaternion.identity; scale = Vector3.one; break;
			}
			transform.localPosition = position;
			transform.localRotation = rotation;
			transform.localScale = scale;
		}
		#endregion

		#region AndroToFemale
		private static void AndroToFemaleLocalTranslation(int index, Transform transform)
		{
			Vector3 position; Quaternion rotation; Vector3 scale;

			switch (index)
			{
				case 0: position = new Vector3(-2.823687e-07f, 1.082601f, -0.03134346f); rotation = new Quaternion(0.0390729f, 0.03907251f, -0.7060263f, 0.7060266f); scale = new Vector3(1.000001f, 1.000001f, 1f); break; //Hips
				case 1: position = new Vector3(0.01696816f, -0.1107858f, 0.001985138f); rotation = new Quaternion(-0.05064015f, -0.07523417f, 0.9954914f, 0.02778944f); scale = new Vector3(1.011157f, 1.011158f, 1.011159f); break; //LeftUpLeg
				case 2: position = new Vector3(-1.269218e-07f, -2.328306e-09f, -6.798655e-08f); rotation = new Quaternion(0.03654476f, -0.7959164f, 0.00869143f, 0.60424f); scale = new Vector3(1.044366f, 1.026306f, 0.9089744f); break; //LeftGluteus
				case 3: position = new Vector3(-0.4368478f, 5.867332e-08f, -3.026798e-09f); rotation = new Quaternion(-0.04299358f, -0.01749874f, -0.03872577f, 0.9981712f); scale = new Vector3(1.032332f, 1.032332f, 1.032332f); break; //LeftLeg
				case 4: position = new Vector3(-0.497137f, 3.073364e-08f, -5.820766e-09f); rotation = new Quaternion(0.8615909f, -0.02199097f, -0.5011128f, 0.077869f); scale = new Vector3(0.9371329f, 0.9371329f, 0.9371327f); break; //LeftFoot
				case 5: position = new Vector3(-0.1806862f, 1.071021e-08f, -7.450581e-09f); rotation = new Quaternion(-0.06246543f, -0.2067474f, -0.02363382f, 0.9761122f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftToeBase
				case 6: position = new Vector3(-0.1350086f, 0.005499586f, 0.003245557f); rotation = new Quaternion(9.052555e-08f, -9.599336e-09f, -1.487897e-08f, 1f); scale = new Vector3(0.9701949f, 0.8873359f, 0.910664f); break; //LeftLegAdjust
				case 7: position = new Vector3(-0.1118854f, -0.0001738979f, -0.002602394f); rotation = new Quaternion(-0.03335002f, 6.743345e-10f, 6.9267e-09f, 0.9994438f); scale = new Vector3(1.029126f, 0.9414408f, 0.8839046f); break; //LeftUpLegAdjust
				case 8: position = new Vector3(-0.05387852f, 4.067724e-09f, -1.862642e-09f); rotation = new Quaternion(5.486465e-08f, -0.07920212f, 3.437414e-08f, 0.9968586f); scale = new Vector3(0.9500028f, 0.9500027f, 0.9500028f); break; //LowerBack
				case 9: position = new Vector3(-0.05110959f, 3.858676e-09f, -5.587932e-09f); rotation = new Quaternion(2.218178e-08f, 7.4506e-09f, -1.383215e-10f, 1f); scale = new Vector3(1f, 0.9963356f, 1f); break; //LowerBackAdjust
				case 10: position = new Vector3(-0.03330027f, 3.334989e-08f, -0.04927876f); rotation = new Quaternion(6.237156e-08f, -5.587915e-09f, -1.431709e-15f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LowerBackBelly
				case 11: position = new Vector3(-0.1835815f, 1.106441e-08f, -0.0001194421f); rotation = new Quaternion(-5.358099e-07f, -0.08683414f, 1.383217e-08f, 0.9962228f); scale = new Vector3(1.012352f, 1.012352f, 1.012352f); break; //Spine
				case 12: position = new Vector3(-0.177195f, 1.33873e-08f, -2.051145e-05f); rotation = new Quaternion(4.719643e-07f, 0.1107154f, -6.315968e-10f, 0.9938522f); scale = new Vector3(1.022009f, 1.022009f, 1.022009f); break; //Spine1
				case 13: position = new Vector3(-0.00704691f, -0.08380604f, 0.1105439f); rotation = new Quaternion(0.107155f, 0.6737866f, 0.08572523f, 0.7260721f); scale = new Vector3(1.464014f, 1.023973f, 0.4372671f); break; //LeftOuterBreast
				case 14: position = new Vector3(-0.02676996f, -0.03260392f, -0.03301902f); rotation = new Quaternion(0.007080571f, -0.05320038f, 0.1804277f, 0.982123f); scale = new Vector3(1.33238f, 1.103445f, 3.780005f); break; //LeftInnerBreast
				case 15: position = new Vector3(-0.01666059f, 0.008349041f, -0.002991363f); rotation = new Quaternion(-4.238278e-08f, 3.303562e-08f, -2.673437e-07f, 1f); scale = new Vector3(0.380585f, 0.5493518f, 0.4403599f); break; //LeftInnerBreastAdjust
				case 16: position = new Vector3(0.03481421f, 0.01849101f, -0.04036406f); rotation = new Quaternion(-5.940653e-07f, 5.435676e-07f, 3.915392e-08f, 1f); scale = new Vector3(1.168506f, 0.9939286f, 2.162619f); break; //LeftOuterBreastAdjust
				case 17: position = new Vector3(-0.1767762f, -0.05235612f, 0.003283246f); rotation = new Quaternion(0.04308118f, -0.03842258f, 0.8381182f, 0.5424258f); scale = new Vector3(0.8780853f, 0.878085f, 0.8780851f); break; //LeftShoulder
				case 18: position = new Vector3(-0.1421283f, 9.204925e-05f, 2.827123e-05f); rotation = new Quaternion(-0.01361716f, -0.03404342f, 0.1446615f, 0.9888017f); scale = new Vector3(1.112175f, 1.112175f, 1.112175f); break; //LeftArm
				case 19: position = new Vector3(-0.06878776f, 0.002432385f, -0.004666742f); rotation = new Quaternion(0.2379592f, -1.697735e-08f, 2.712036e-07f, 0.9712752f); scale = new Vector3(0.9999996f, 0.9137615f, 0.803792f); break; //LeftArmAdjust
				case 20: position = new Vector3(-0.2838207f, 0.0003530812f, 0.001950197f); rotation = new Quaternion(0.05142077f, 0.1061855f, 0.001919557f, 0.993014f); scale = new Vector3(1.098213f, 1.098213f, 1.098213f); break; //LeftForeArm
				case 21: position = new Vector3(-0.03565873f, 1.77417e-07f, -1.117587e-07f); rotation = new Quaternion(4.915821e-05f, -4.700643e-08f, -2.37057e-07f, 1f); scale = new Vector3(0.9908789f, 0.8339107f, 0.8835804f); break; //LeftForeArmAdjust
				case 22: position = new Vector3(-0.1475396f, 6.251503e-08f, -6.146729e-08f); rotation = new Quaternion(3.5204e-07f, 1.909203e-08f, -2.386223e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftForeArmTwist
				case 23: position = new Vector3(-0.02798359f, -0.001764301f, 0.002337968f); rotation = new Quaternion(4.881775e-05f, -3.49035e-08f, 4.321936e-07f, 1f); scale = new Vector3(0.9999999f, 0.8798943f, 0.9791992f); break; //LeftForeArmTwistAdjust
				case 24: position = new Vector3(-0.280726f, -0.0004454491f, 9.784289e-05f); rotation = new Quaternion(-0.7203786f, 0.01591173f, 0.01711368f, 0.6931874f); scale = new Vector3(0.9626768f, 0.9626767f, 0.9626769f); break; //LeftHand
				case 25: position = new Vector3(-0.08598498f, 0.03237575f, 0.004104163f); rotation = new Quaternion(-0.1992534f, -0.04946491f, -0.1801337f, 0.9619788f); scale = new Vector3(1.027516f, 1.027516f, 1.027515f); break; //LeftHandFinger01_01
				case 26: position = new Vector3(-0.03693629f, 4.470348e-08f, -1.564622e-07f); rotation = new Quaternion(0.01495295f, -0.1447614f, -0.02117516f, 0.989127f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger01_02
				case 27: position = new Vector3(-0.02210765f, -3.72529e-09f, 7.450581e-09f); rotation = new Quaternion(-0.03146791f, -0.0471193f, 0.003357278f, 0.9983879f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftHandFinger01_03
				case 28: position = new Vector3(-0.09408698f, 0.01511253f, 0.01527949f); rotation = new Quaternion(-0.1305101f, -0.08390953f, -0.1485574f, 0.976656f); scale = new Vector3(1.046258f, 1.046258f, 1.046258f); break; //LeftHandFinger02_01
				case 29: position = new Vector3(-0.03892582f, -3.72529e-09f, -1.005828e-07f); rotation = new Quaternion(0.004608926f, -0.1096315f, 0.008167254f, 0.9939281f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger02_02
				case 30: position = new Vector3(-0.03213915f, -3.72529e-09f, 5.960464e-08f); rotation = new Quaternion(0.009086291f, -0.06614652f, -0.01302745f, 0.9976835f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftHandFinger02_03
				case 31: position = new Vector3(-0.09284318f, -0.009587891f, 0.008836692f); rotation = new Quaternion(-0.07259603f, -0.02459152f, -0.08328836f, 0.9935734f); scale = new Vector3(1.047823f, 1.047823f, 1.047823f); break; //LeftHandFinger03_01
				case 32: position = new Vector3(-0.04725775f, -7.450581e-09f, -1.303852e-08f); rotation = new Quaternion(0.002872084f, -0.09684265f, -0.01627372f, 0.9951625f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger03_02
				case 33: position = new Vector3(-0.04042118f, 3.72529e-09f, -1.005828e-07f); rotation = new Quaternion(0.01665003f, -0.01604601f, 0.0131781f, 0.9996458f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftHandFinger03_03
				case 34: position = new Vector3(-0.09677466f, -0.03304061f, 0.002816272f); rotation = new Quaternion(-0.03541414f, -0.02375934f, -0.002262811f, 0.9990877f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger04_01
				case 35: position = new Vector3(-0.05416493f, 4.889444e-09f, 8.987263e-08f); rotation = new Quaternion(0.002677887f, -0.07985286f, -0.004389217f, 0.9967934f); scale = new Vector3(1f, 1f, 1f); break; //LeftHandFinger04_02
				case 36: position = new Vector3(-0.03353683f, 3.143214e-09f, 2.561137e-08f); rotation = new Quaternion(-0.001482369f, -0.07270254f, -0.01461354f, 0.9972456f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftHandFinger04_03
				case 37: position = new Vector3(-0.03360311f, -0.04425458f, -0.02465412f); rotation = new Quaternion(0.5845863f, -0.128429f, 0.3980423f, 0.6952174f); scale = new Vector3(1.045444f, 1.045444f, 1.045444f); break; //LeftHandFinger05_01
				case 38: position = new Vector3(-0.02932129f, 3.236346e-08f, 1.862645e-08f); rotation = new Quaternion(-0.04490694f, -0.03217195f, -0.02465372f, 0.9981686f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftHandFinger05_02
				case 39: position = new Vector3(-0.03140653f, 1.082662e-08f, 5.960464e-08f); rotation = new Quaternion(0.0249025f, -0.05538721f, -0.03426435f, 0.9975661f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftHandFinger05_03
				case 40: position = new Vector3(-0.03552746f, 9.092037e-08f, -3.632158e-08f); rotation = new Quaternion(0.1465888f, 7.930635e-08f, 2.238563e-07f, 0.9891975f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftShoulderAdjust
				case 41: position = new Vector3(-0.02825284f, 0.01946994f, -0.02666573f); rotation = new Quaternion(0.6757174f, -0.1421932f, -0.2180939f, 0.6896537f); scale = new Vector3(1f, 1.222223f, 1f); break; //LeftTrapezius
				case 42: position = new Vector3(-0.1943356f, 1.467203e-08f, 8.753216e-08f); rotation = new Quaternion(-3.055475e-07f, 0.09072299f, -2.61727e-08f, 0.9958762f); scale = new Vector3(0.9999999f, 1f, 1f); break; //Neck
				case 43: position = new Vector3(-0.1008214f, 7.611821e-09f, -5.960464e-08f); rotation = new Quaternion(1.246556e-07f, -0.08316033f, 2.214955e-08f, 0.9965362f); scale = new Vector3(0.9999999f, 1f, 1f); break; //Head
				case 44: position = new Vector3(-0.0512219f, 3.390704e-09f, -0.004338739f); rotation = new Quaternion(-4.961742e-08f, 1.536684e-08f, 4.314208e-15f, 1f); scale = new Vector3(1f, 1f, 1.026994f); break; //HeadAdjust
				case 45: position = new Vector3(0.03008365f, 5.718068e-07f, 0.08773045f); rotation = new Quaternion(-1.899735e-05f, 0.7009441f, 1.863186e-05f, 0.7132162f); scale = new Vector3(0.928337f, 0.9999999f, 1.010953f); break; //InnerMouthUpperAdjust
				case 46: position = new Vector3(0.03425555f, -5.426948e-09f, -0.00550996f); rotation = new Quaternion(-1.349972e-08f, 0.007815712f, 1.000602e-08f, 0.9999695f); scale = new Vector3(0.9999999f, 0.929274f, 0.8009629f); break; //UpperNeckAdjust
				case 47: position = new Vector3(-0.0621063f, -0.03167291f, 0.1119432f); rotation = new Quaternion(0.668591f, 0.2419274f, -0.6604239f, -0.2414487f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //LeftCheek
				case 48: position = new Vector3(7.357448e-08f, 9.31323e-10f, -1.834633e-07f); rotation = new Quaternion(-5.366696e-08f, -2.741057e-06f, 1.851434e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftCheekAdjust
				case 49: position = new Vector3(-0.07177602f, -0.07815026f, 0.02873641f); rotation = new Quaternion(-0.1239046f, -0.5752555f, 0.1082034f, 0.801262f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftEar
				case 50: position = new Vector3(-1.676381e-08f, 2.933666e-08f, 1.797453e-07f); rotation = new Quaternion(-1.172561e-07f, 2.104789e-07f, -5.075706e-08f, 1f); scale = new Vector3(0.9999999f, 0.8578046f, 0.9576882f); break; //LeftEarAdjust
				case 51: position = new Vector3(-0.09306289f, -0.03540988f, 0.11645f); rotation = new Quaternion(0.7124566f, -3.090618e-06f, -0.7017161f, -4.222129e-06f); scale = new Vector3(1f, 1f, 1f); break; //LeftEye
				case 52: position = new Vector3(1.252373e-07f, -1.270706e-07f, -9.576958e-07f); rotation = new Quaternion(2.147789e-06f, 3.59157e-06f, 1.243715e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeAdjust
				case 53: position = new Vector3(4.327629e-08f, -7.118411e-08f, -3.616505e-07f); rotation = new Quaternion(4.362492e-06f, -3.746247e-06f, 3.239952e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyeGlobe
				case 54: position = new Vector3(4.327629e-08f, -7.118411e-08f, -3.616505e-07f); rotation = new Quaternion(0.0002754479f, -0.1797692f, 1.1836e-05f, 0.9837088f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLowerLid
				case 55: position = new Vector3(-1.026365e-09f, 5.005859e-09f, 4.260437e-08f); rotation = new Quaternion(0.0002783754f, -0.0004685162f, -3.686532e-05f, 0.9999999f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLowerLidAdjust
				case 56: position = new Vector3(4.327629e-08f, -7.118411e-08f, -3.616505e-07f); rotation = new Quaternion(-0.0003553574f, 0.1486991f, 4.300326e-06f, 0.9888824f); scale = new Vector3(1f, 1f, 1f); break; //LeftUpperLid
				case 57: position = new Vector3(-5.695688e-08f, 5.041784e-09f, 1.009266e-07f); rotation = new Quaternion(-0.000349355f, 0.0003598895f, -3.60418e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftUpperLidAdjust
				case 58: position = new Vector3(-0.106003f, -0.01795713f, 0.08253001f); rotation = new Quaternion(0.7311099f, -1.730622e-07f, -0.6822597f, 2.660502e-07f); scale = new Vector3(0.9999998f, 1f, 1f); break; //LeftEyebrowLow
				case 59: position = new Vector3(-1.794989e-08f, 1.146701e-09f, 2.01101e-07f); rotation = new Quaternion(-7.424769e-08f, 7.804484e-07f, -1.963213e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftEyebrowLowAdjust
				case 60: position = new Vector3(-0.1113975f, -0.03246113f, 0.08004237f); rotation = new Quaternion(0.7257723f, -1.657847e-07f, -0.6879351f, 1.667204e-07f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftEyebrowMiddle
				case 61: position = new Vector3(-2.686454e-09f, 1.171622e-08f, 6.821052e-08f); rotation = new Quaternion(-3.736196e-08f, 1.667067e-06f, 4.800714e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftEyebrowMiddleAdjust
				case 62: position = new Vector3(-0.1100762f, -0.0513125f, 0.06985872f); rotation = new Quaternion(0.7432758f, -0.0004890394f, -0.6689849f, 0.0003017884f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //LeftEyebrowUp
				case 63: position = new Vector3(-8.918141e-08f, -6.85759e-10f, -1.720182e-07f); rotation = new Quaternion(-3.685564e-07f, 1.013977e-06f, 2.107973e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftEyebrowUpAdjust
				case 64: position = new Vector3(-0.02419438f, -0.01066655f, 0.1498123f); rotation = new Quaternion(0.7940922f, 0.0597742f, -0.6030683f, -0.04640201f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsSuperiorMiddle
				case 65: position = new Vector3(-3.748573e-08f, -7.450581e-09f, -1.021053e-07f); rotation = new Quaternion(-0.0001272968f, 1.579278e-05f, 4.406633e-07f, 1f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLipsSuperiorMiddleAdjust
				case 66: position = new Vector3(-0.02162369f, -0.03721064f, 0.1099524f); rotation = new Quaternion(-0.6591188f, -0.2391572f, 0.6702258f, 0.2432356f); scale = new Vector3(1f, 1f, 0.9999999f); break; //LeftLowCheek
				case 67: position = new Vector3(-4.097819e-08f, -1.117587e-08f, 1.958369e-09f); rotation = new Quaternion(-2.782399e-07f, -4.378269e-09f, -1.356529e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLowCheekAdjust
				case 68: position = new Vector3(-0.04580854f, -0.02959668f, 0.1019304f); rotation = new Quaternion(0.7088718f, 0.001025927f, -0.7053237f, 0.0042689f); scale = new Vector3(0.9999999f, 1f, 1f); break; //LeftNose
				case 69: position = new Vector3(8.742791e-08f, 1.917942e-08f, -1.512526e-07f); rotation = new Quaternion(-0.0003554256f, -1.413417e-06f, -2.768097e-10f, 0.9999999f); scale = new Vector3(1f, 0.9999999f, 1f); break; //LeftNoseAdjust
				case 70: position = new Vector3(-0.02469261f, -9.259796e-05f, 0.1521645f); rotation = new Quaternion(0.7874869f, -2.399031e-07f, -0.6163315f, -1.471336e-07f); scale = new Vector3(1f, 1f, 1f); break; //LipsSuperior
				case 71: position = new Vector3(-5.216159e-08f, 1.4657e-11f, -9.690446e-08f); rotation = new Quaternion(-7.338064e-07f, -6.29574e-06f, 1.251339e-09f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LipsSuperiorAdjust
				case 72: position = new Vector3(-0.04974716f, -9.260845e-05f, 0.03875849f); rotation = new Quaternion(-0.4972367f, 1.621632e-07f, 0.8676149f, 1.535315e-08f); scale = new Vector3(1.078754f, 1.078754f, 1.078754f); break; //Mandible
				case 73: position = new Vector3(-0.0621346f, -9.345217e-05f, -0.01742905f); rotation = new Quaternion(0.9627837f, 3.906085e-06f, -0.2702732f, 1.326077e-05f); scale = new Vector3(0.858115f, 1f, 0.9999999f); break; //InnerMouthLowerAdjust
				case 74: position = new Vector3(-0.08738229f, 0.02388479f, 0.01521285f); rotation = new Quaternion(-0.02273115f, 0.2682542f, -0.08131651f, 0.9596409f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //LeftLips
				case 75: position = new Vector3(-3.576304e-09f, -2.690172e-09f, 1.099197e-07f); rotation = new Quaternion(2.632683e-07f, -7.450574e-08f, -2.607703e-07f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //LeftLipsAdjust
				case 76: position = new Vector3(-0.1010863f, 0.01080502f, 0.0241915f); rotation = new Quaternion(-0.01443659f, 0.180118f, -0.08480804f, 0.9798759f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //LeftLipsInferior
				case 77: position = new Vector3(-4.773028e-09f, -3.72529e-09f, 1.603694e-07f); rotation = new Quaternion(2.616551e-06f, 5.386826e-07f, -9.266801e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LeftLipsInferiorAdjust
				case 78: position = new Vector3(-0.0396845f, 0.05439252f, -0.01616827f); rotation = new Quaternion(0.02029481f, 0.1078437f, 0.1409843f, 0.9839112f); scale = new Vector3(0.9200175f, 0.9200175f, 0.9200173f); break; //LeftLowMaxilar
				case 79: position = new Vector3(1.396984e-08f, 1.97906e-08f, 9.051291e-08f); rotation = new Quaternion(-3.71132e-07f, -1.121724e-07f, 1.044394e-08f, 1f); scale = new Vector3(1.159251f, 1f, 1f); break; //LeftLowMaxilarAdjust
				case 80: position = new Vector3(-0.1021187f, -2.134784e-05f, 0.02429919f); rotation = new Quaternion(-3.542938e-08f, 0.1730407f, -4.849678e-08f, 0.9849147f); scale = new Vector3(1f, 1f, 1f); break; //LipsInferior
				case 81: position = new Vector3(4.469729e-08f, -1.584e-12f, -5.959898e-08f); rotation = new Quaternion(-3.623293e-07f, -6.966293e-06f, 4.555588e-10f, 1f); scale = new Vector3(1f, 1f, 1f); break; //LipsInferiorAdjust
				case 82: position = new Vector3(-0.0012281f, 3.19982e-10f, 0.002828551f); rotation = new Quaternion(-8.661767e-08f, -3.874302e-07f, 4.552467e-11f, 1f); scale = new Vector3(1f, 1f, 1f); break; //MandibleAdjust
				case 83: position = new Vector3(-0.08738234f, -0.02408692f, 0.01521272f); rotation = new Quaternion(0.02273111f, 0.2682542f, 0.08131669f, 0.9596409f); scale = new Vector3(1f, 1f, 1f); break; //RightLips
				case 84: position = new Vector3(-2.547085e-08f, 8.67885e-10f, -1.434e-07f); rotation = new Quaternion(-1.63426e-07f, -1.639128e-07f, 1.341104e-07f, 1f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightLipsAdjust
				case 85: position = new Vector3(-0.1010863f, -0.01100714f, 0.0241915f); rotation = new Quaternion(0.01443662f, 0.1801181f, 0.08480796f, 0.9798759f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLipsInferior
				case 86: position = new Vector3(9.19681e-09f, 4.656613e-09f, -4.913454e-08f); rotation = new Quaternion(-2.649496e-06f, 4.328465e-07f, 8.382018e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLipsInferiorAdjust
				case 87: position = new Vector3(-0.03968463f, -0.05459472f, -0.0161682f); rotation = new Quaternion(-0.02029479f, 0.1078434f, -0.1409846f, 0.9839112f); scale = new Vector3(0.9200177f, 0.9200175f, 0.9200174f); break; //RightLowMaxilar
				case 88: position = new Vector3(5.494803e-08f, -5.261973e-08f, -1.81084e-07f); rotation = new Quaternion(3.688038e-07f, -2.60095e-08f, 7.069772e-08f, 1f); scale = new Vector3(1.159251f, 0.9999999f, 1f); break; //RightLowMaxilarAdjust
				case 89: position = new Vector3(-0.04092126f, -9.310582e-05f, -0.01724901f); rotation = new Quaternion(2.000554e-06f, 0.2488658f, -7.697174e-07f, 0.968538f); scale = new Vector3(0.9382043f, 0.9382044f, 0.9382045f); break; //Tongue01
				case 90: position = new Vector3(-0.02827538f, 2.136466e-09f, 1.14236e-07f); rotation = new Quaternion(0.998992f, 5.321574e-08f, -0.04489022f, 7.171173e-06f); scale = new Vector3(1f, 0.9999999f, 1f); break; //Tongue02
				case 91: position = new Vector3(-0.05437493f, -9.258858e-05f, 0.1528943f); rotation = new Quaternion(0.7354584f, -1.260978e-08f, -0.67757f, 2.253409e-07f); scale = new Vector3(1f, 1f, 1f); break; //NoseBase
				case 92: position = new Vector3(-6.705007e-08f, 1.3173e-11f, -4.327089e-08f); rotation = new Quaternion(-3.294859e-08f, -0.0006734832f, 1.537267e-08f, 0.9999998f); scale = new Vector3(0.8081012f, 1f, 1f); break; //NoseBaseAdjust
				case 93: position = new Vector3(-0.07899981f, 2.447798e-05f, 0.1444507f); rotation = new Quaternion(-0.7055193f, -0.001159922f, 0.7086876f, 0.001799856f); scale = new Vector3(1f, 1f, 1f); break; //NoseMiddle
				case 94: position = new Vector3(-0.001058223f, 1.922246e-06f, 2.757029e-05f); rotation = new Quaternion(-0.0001302074f, 0.0004997691f, -0.0009101477f, 0.9999995f); scale = new Vector3(0.6913782f, 0.9299322f, 0.9999999f); break; //NoseMiddleAdjust
				case 95: position = new Vector3(-0.1073343f, 0.0001092739f, 0.1217429f); rotation = new Quaternion(0.7095863f, 0.006206567f, -0.7045732f, 0.005031863f); scale = new Vector3(0.9999998f, 1f, 0.9999999f); break; //NoseTop
				case 96: position = new Vector3(8.519365e-08f, 3.543391e-09f, 3.001503e-08f); rotation = new Quaternion(3.415212e-05f, 3.721837e-08f, 7.117728e-10f, 1f); scale = new Vector3(1f, 1f, 1.520226f); break; //NoseTopAdjust
				case 97: position = new Vector3(-0.06210629f, 0.03167294f, 0.1119432f); rotation = new Quaternion(0.6685911f, -0.241927f, -0.6604241f, 0.2414484f); scale = new Vector3(1f, 1f, 1f); break; //RightCheek
				case 98: position = new Vector3(4.959293e-08f, -1.14087e-08f, 1.736626e-07f); rotation = new Quaternion(2.14786e-07f, -2.761255e-06f, -9.59113e-08f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightCheekAdjust
				case 99: position = new Vector3(-0.07177576f, 0.07815047f, 0.02873637f); rotation = new Quaternion(0.1239043f, -0.5752558f, -0.1082029f, 0.801262f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightEar
				case 100: position = new Vector3(-9.685755e-08f, 2.619345e-09f, 1.825392e-07f); rotation = new Quaternion(2.486264e-07f, 2.663583e-07f, 7.310877e-08f, 1f); scale = new Vector3(0.9999999f, 0.8578045f, 0.9576882f); break; //RightEarAdjust
				case 101: position = new Vector3(-0.09306289f, 0.03540993f, 0.11645f); rotation = new Quaternion(0.7124566f, 3.103892e-06f, -0.7017161f, 4.019454e-06f); scale = new Vector3(1f, 1f, 1f); break; //RightEye
				case 102: position = new Vector3(1.05121e-07f, 8.018899e-08f, -8.386697e-07f); rotation = new Quaternion(-2.138464e-06f, 3.595294e-06f, 1.843405e-08f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightEyeAdjust
				case 103: position = new Vector3(8.276378e-08f, 9.881001e-08f, -2.426233e-07f); rotation = new Quaternion(-4.377739e-06f, -3.742517e-06f, -1.411583e-07f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightEyeGlobe
				case 104: position = new Vector3(6.041199e-08f, 5.038125e-08f, -2.426239e-07f); rotation = new Quaternion(-0.0002752239f, -0.1797691f, -1.187016e-05f, 0.9837088f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightLowerLid
				case 105: position = new Vector3(9.845144e-08f, 3.051355e-09f, -8.173629e-08f); rotation = new Quaternion(-0.0002784951f, -0.0004620787f, 3.697521e-05f, 0.9999999f); scale = new Vector3(1f, 1f, 1f); break; //RightLowerLidAdjust
				case 106: position = new Vector3(8.276378e-08f, 9.881001e-08f, -2.426233e-07f); rotation = new Quaternion(0.000355424f, 0.1486984f, -4.873361e-06f, 0.9888825f); scale = new Vector3(1f, 1f, 1f); break; //RightUpperLid
				case 107: position = new Vector3(2.933484e-08f, -6.23459e-10f, 2.089946e-07f); rotation = new Quaternion(0.0003495335f, 0.0003604552f, 3.613244e-05f, 0.9999999f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightUpperLidAdjust
				case 108: position = new Vector3(-0.106003f, 0.01795719f, 0.08253005f); rotation = new Quaternion(0.7311099f, -1.696474e-07f, -0.6822597f, -1.331622e-07f); scale = new Vector3(0.9999998f, 1f, 1f); break; //RightEyebrowLow
				case 109: position = new Vector3(9.860194e-08f, -3.36926e-10f, 2.052203e-07f); rotation = new Quaternion(-5.188788e-08f, 6.649644e-07f, -6.063945e-09f, 1f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightEyebrowLowAdjust
				case 110: position = new Vector3(-0.1113974f, 0.03246119f, 0.08004237f); rotation = new Quaternion(0.7257724f, 1.441961e-07f, -0.6879349f, 1.464926e-07f); scale = new Vector3(1f, 1f, 1f); break; //RightEyebrowMiddle
				case 111: position = new Vector3(-6.018572e-09f, 5.275705e-09f, -6.59935e-08f); rotation = new Quaternion(1.686619e-07f, 1.603738e-06f, 2.032358e-08f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightEyebrowMiddleAdjust
				case 112: position = new Vector3(-0.1100761f, 0.05131255f, 0.0698587f); rotation = new Quaternion(0.7432759f, 0.0004888641f, -0.6689848f, -0.0003020421f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightEyebrowUp
				case 113: position = new Vector3(-2.763409e-08f, 3.63616e-09f, -2.463094e-08f); rotation = new Quaternion(-2.608577e-07f, 9.057076e-07f, 2.079493e-08f, 1f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightEyebrowUpAdjust
				case 114: position = new Vector3(-0.02421153f, 0.01067639f, 0.1498739f); rotation = new Quaternion(0.7940922f, -0.05977425f, -0.6030683f, 0.04640236f); scale = new Vector3(1f, 1f, 1f); break; //RightLipsSuperiorMiddle
				case 115: position = new Vector3(-1.280569e-08f, 8.381903e-09f, 6.407208e-08f); rotation = new Quaternion(0.0001277147f, 1.579255e-05f, -3.40087e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 0.9999999f); break; //RightLipsSuperiorMiddleAdjust
				case 116: position = new Vector3(-0.02162368f, 0.03721068f, 0.1099524f); rotation = new Quaternion(-0.6591188f, 0.2391578f, 0.6702256f, -0.2432358f); scale = new Vector3(1f, 1f, 0.9999998f); break; //RightLowCheek
				case 117: position = new Vector3(-3.352761e-08f, 1.303852e-08f, 2.349957e-07f); rotation = new Quaternion(1.86812e-07f, -2.378151e-08f, -2.474408e-07f, 1f); scale = new Vector3(1f, 0.9999999f, 1f); break; //RightLowCheekAdjust
				case 118: position = new Vector3(-0.04580854f, 0.02959676f, 0.1019304f); rotation = new Quaternion(0.7088718f, -0.001020728f, -0.7053237f, -0.00426313f); scale = new Vector3(0.9999998f, 1f, 1f); break; //RightNose
				case 119: position = new Vector3(9.822543e-09f, -1.702574e-08f, -2.903398e-07f); rotation = new Quaternion(0.0003646001f, -1.413429e-06f, 4.644442e-09f, 0.9999999f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightNoseAdjust
				case 120: position = new Vector3(-0.03606353f, -9.259871e-05f, 0.1209728f); rotation = new Quaternion(0.7071069f, 9.415695e-08f, -0.7071067f, 9.415697e-08f); scale = new Vector3(1f, 1f, 1f); break; //UpperLips
				case 121: position = new Vector3(2.049611e-08f, -6.414e-12f, 9.675552e-08f); rotation = new Quaternion(2.152947e-09f, 1.522247e-06f, -9.581807e-11f, 1f); scale = new Vector3(1f, 1f, 1f); break; //UpperLipsAdjust
				case 122: position = new Vector3(-0.03337933f, 2.508342e-09f, 0.0003347322f); rotation = new Quaternion(4.329001e-07f, 2.235174e-08f, -5.40802e-09f, 1f); scale = new Vector3(0.9999999f, 0.8567359f, 0.9034932f); break; //NeckAdjust
				case 123: position = new Vector3(-0.007046778f, 0.08380614f, 0.1105438f); rotation = new Quaternion(-0.1071545f, 0.6737863f, -0.08572649f, 0.7260723f); scale = new Vector3(1.464014f, 1.023973f, 0.437267f); break; //RightOuterBreast
				case 124: position = new Vector3(-0.02676992f, 0.03260385f, -0.03301916f); rotation = new Quaternion(-0.007081468f, -0.05320054f, -0.1804276f, 0.982123f); scale = new Vector3(1.332381f, 1.103445f, 3.780005f); break; //RightInnerBreast
				case 125: position = new Vector3(-0.01666054f, -0.008349052f, -0.002991227f); rotation = new Quaternion(3.051559e-08f, 5.016519e-08f, 4.114157e-07f, 1f); scale = new Vector3(0.380585f, 0.5493519f, 0.4403598f); break; //RightInnerBreastAdjust
				case 126: position = new Vector3(0.03481423f, -0.01849108f, -0.04036427f); rotation = new Quaternion(-3.167098e-07f, 5.467556e-07f, -1.942718e-08f, 1f); scale = new Vector3(1.168506f, 0.9939288f, 2.162619f); break; //RightOuterBreastAdjust
				case 127: position = new Vector3(-0.1767762f, 0.05235615f, 0.003283181f); rotation = new Quaternion(-0.04308153f, -0.03842308f, -0.8381181f, 0.5424257f); scale = new Vector3(0.8780853f, 0.878085f, 0.8780853f); break; //RightShoulder
				case 128: position = new Vector3(-0.1421282f, -9.214121e-05f, 2.829172e-05f); rotation = new Quaternion(0.0136171f, -0.03404342f, -0.1446616f, 0.9888017f); scale = new Vector3(1.112175f, 1.112175f, 1.112175f); break; //RightArm
				case 129: position = new Vector3(-0.06878766f, -0.002432263f, -0.00466672f); rotation = new Quaternion(-0.2379598f, -3.066341e-07f, -2.395702e-07f, 0.971275f); scale = new Vector3(0.9999996f, 0.9137616f, 0.803792f); break; //RightArmAdjust
				case 130: position = new Vector3(-0.2838206f, -0.0003529843f, 0.001950227f); rotation = new Quaternion(-0.05142068f, 0.1061855f, -0.0019194f, 0.993014f); scale = new Vector3(1.098213f, 1.098213f, 1.098213f); break; //RightForeArm
				case 131: position = new Vector3(-0.03565861f, -8.556526e-08f, -1.080334e-07f); rotation = new Quaternion(-1.563538e-07f, 1.597822e-08f, -1.321725e-09f, 1f); scale = new Vector3(0.9908791f, 0.8339108f, 0.8835806f); break; //RightForeArmAdjust
				case 132: position = new Vector3(-0.1475397f, -1.197914e-07f, -7.636845e-08f); rotation = new Quaternion(-3.49246e-07f, 1.350409e-08f, 2.397865e-07f, 1f); scale = new Vector3(1f, 1f, 1f); break; //RightForeArmTwist
				case 133: position = new Vector3(-0.02798354f, 0.001764001f, 0.002338154f); rotation = new Quaternion(7.726673e-08f, 1.350415e-08f, -3.857422e-07f, 1f); scale = new Vector3(0.9999999f, 0.8798943f, 0.9791992f); break; //RightForeArmTwistAdjust
				case 134: position = new Vector3(-0.2832537f, 0.002328217f, 0.0002885368f); rotation = new Quaternion(0.7203786f, 0.01591165f, -0.01711364f, 0.6931874f); scale = new Vector3(0.8723498f, 0.8723497f, 0.8723497f); break; //RightHand
				case 135: position = new Vector3(-0.08598507f, -0.03237573f, 0.004104202f); rotation = new Quaternion(0.1992534f, -0.04946502f, 0.1801338f, 0.9619788f); scale = new Vector3(1.027515f, 1.027516f, 1.027515f); break; //RightHandFinger01_01
				case 136: position = new Vector3(-0.03693619f, 2.980232e-08f, 3.72529e-08f); rotation = new Quaternion(-0.01495301f, -0.1447614f, 0.02117512f, 0.989127f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger01_02
				case 137: position = new Vector3(-0.02210766f, -1.862645e-08f, -5.960464e-08f); rotation = new Quaternion(0.03146787f, -0.04711927f, -0.003357269f, 0.9983879f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightHandFinger01_03
				case 138: position = new Vector3(-0.09408695f, -0.01511249f, 0.01527965f); rotation = new Quaternion(0.13051f, -0.08390955f, 0.1485574f, 0.976656f); scale = new Vector3(1.046258f, 1.046258f, 1.046258f); break; //RightHandFinger02_01
				case 139: position = new Vector3(-0.03892586f, 2.980232e-08f, -1.676381e-07f); rotation = new Quaternion(-0.004608934f, -0.1096316f, -0.008167291f, 0.9939281f); scale = new Vector3(1f, 1f, 0.9999999f); break; //RightHandFinger02_02
				case 140: position = new Vector3(-0.0321392f, -1.862645e-08f, -3.72529e-09f); rotation = new Quaternion(-0.009086289f, -0.06614655f, 0.01302745f, 0.9976835f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger02_03
				case 141: position = new Vector3(-0.09293728f, 0.009208344f, 0.01136513f); rotation = new Quaternion(0.0735148f, -0.03560326f, 0.08247857f, 0.9932398f); scale = new Vector3(1.047823f, 1.047823f, 1.047823f); break; //RightHandFinger03_01
				case 142: position = new Vector3(-0.04725764f, 0f, 1.583248e-07f); rotation = new Quaternion(-0.002872104f, -0.09684271f, 0.01627373f, 0.9951625f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightHandFinger03_02
				case 143: position = new Vector3(-0.04042117f, -1.490116e-08f, 9.49949e-08f); rotation = new Quaternion(-0.01665002f, -0.01604594f, -0.0131781f, 0.9996458f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger03_03
				case 144: position = new Vector3(-0.09677459f, 0.03304063f, 0.002816254f); rotation = new Quaternion(0.03541413f, -0.02375928f, 0.00226276f, 0.9990877f); scale = new Vector3(0.9999999f, 0.9999999f, 1f); break; //RightHandFinger04_01
				case 145: position = new Vector3(-0.05416503f, -9.080395e-09f, -1.266599e-07f); rotation = new Quaternion(-0.002677887f, -0.07985285f, 0.004389253f, 0.9967934f); scale = new Vector3(0.9999999f, 0.9999999f, 0.9999999f); break; //RightHandFinger04_02
				case 146: position = new Vector3(-0.03353663f, -4.656613e-09f, -7.683411e-08f); rotation = new Quaternion(0.001482351f, -0.0727024f, 0.01461355f, 0.9972456f); scale = new Vector3(1f, 1f, 1f); break; //RightHandFinger04_03
				case 147: position = new Vector3(-0.03360313f, 0.0442546f, -0.02465411f); rotation = new Quaternion(-0.5845863f, -0.128429f, -0.3980422f, 0.6952175f); scale = new Vector3(1.045444f, 1.045444f, 1.045444f); break; //RightHandFinger05_01
				case 148: position = new Vector3(-0.02932128f, -8.381903e-09f, 5.587935e-08f); rotation = new Quaternion(0.04490698f, -0.03217192f, 0.02465365f, 0.9981686f); scale = new Vector3(0.9999999f, 1f, 1f); break; //RightHandFinger05_02
				case 149: position = new Vector3(-0.0314065f, 9.49949e-08f, 5.215406e-08f); rotation = new Quaternion(-0.02490251f, -0.05538731f, 0.03426433f, 0.9975661f); scale = new Vector3(0.9999999f, 1f, 0.9999999f); break; //RightHandFinger05_03
				case 150: position = new Vector3(-0.03438604f, -0.01040005f, 0.003151503f); rotation = new Quaternion(-0.1465888f, 4.016914e-08f, -1.551779e-07f, 0.9891976f); scale = new Vector3(0.9045762f, 0.8620701f, 1f); break; //RightShoulderAdjust
				case 151: position = new Vector3(-0.02825282f, -0.01947004f, -0.02666571f); rotation = new Quaternion(-0.6757174f, -0.1421932f, 0.218094f, 0.6896537f); scale = new Vector3(0.9999999f, 1.222223f, 0.9999999f); break; //RightTrapezius
				case 152: position = new Vector3(-0.03833932f, 2.930258e-09f, -0.0005643097f); rotation = new Quaternion(2.965928e-08f, -0.02332881f, -3.646908e-09f, 0.9997278f); scale = new Vector3(0.9945247f, 0.9180464f, 0.8609937f); break; //Spine1Adjust
				case 153: position = new Vector3(-0.02520693f, -5.950533e-09f, 0.01704455f); rotation = new Quaternion(-2.704087e-08f, 1.54703e-08f, -3.437354e-09f, 1f); scale = new Vector3(0.9632108f, 0.8755031f, 0.871305f); break; //SpineAdjust
				case 154: position = new Vector3(0.01696794f, 0.1107857f, 0.001984975f); rotation = new Quaternion(-0.05064208f, 0.07524888f, 0.9954848f, -0.02798479f); scale = new Vector3(1.011157f, 1.011164f, 1.011167f); break; //RightUpLeg
				case 155: position = new Vector3(-2.463057e-07f, 1.117587e-08f, -3.911555e-08f); rotation = new Quaternion(-0.03654366f, -0.795918f, -0.00869193f, 0.604238f); scale = new Vector3(1.044366f, 1.026306f, 0.9089742f); break; //RightGluteus
				case 156: position = new Vector3(-0.4368429f, 4.051253e-08f, 1.164153e-09f); rotation = new Quaternion(0.04299374f, -0.01749801f, 0.03872544f, 0.9981712f); scale = new Vector3(1.032332f, 1.032332f, 1.032332f); break; //RightLeg
				case 157: position = new Vector3(-0.4971315f, 2.793968e-09f, 3.026798e-09f); rotation = new Quaternion(0.8615918f, 0.02199184f, -0.5011111f, -0.07786953f); scale = new Vector3(0.9371328f, 0.9371327f, 0.9371327f); break; //RightFoot
				case 158: position = new Vector3(-0.1806838f, 2.793968e-09f, -1.676381e-08f); rotation = new Quaternion(0.06246534f, -0.2067475f, 0.02363344f, 0.9761122f); scale = new Vector3(1f, 1f, 1f); break; //RightToeBase
				case 159: position = new Vector3(-0.1350076f, -0.005499543f, 0.003245465f); rotation = new Quaternion(6.795975e-08f, -4.799665e-09f, 2.927797e-08f, 1f); scale = new Vector3(0.9701949f, 0.887336f, 0.910664f); break; //RightLegAdjust
				case 160: position = new Vector3(-0.1118843f, 0.0001739305f, -0.002602499f); rotation = new Quaternion(0.03335008f, -7.123713e-09f, -3.032186e-08f, 0.9994438f); scale = new Vector3(1.029126f, 0.9414406f, 0.8839045f); break; //RightUpLegAdjust

				default: position = Vector3.zero; rotation = Quaternion.identity; scale = Vector3.one; break;
			}
			transform.localPosition = position;
			transform.localRotation = rotation;
			transform.localScale = scale;
		}
		#endregion

		#region Static Bones

		private static int[] hashes = new int[0];
		/// <summary>
		/// UMA.UMAUtils.StringToHash
		/// </summary>
		public static int[] Hashes
		{
			get
			{
				if (hashes.Length != Names.Length)
				{
					hashes = new int[names.Length];
					for (int i = 0; i < hashes.Length; i++) hashes[i] = UMA.UMAUtils.StringToHash(names[i]);
				}
				return hashes;
			}
		}

		private static string[] names = new string[0];
		public static string[] Names
		{
			get
			{
				if (names.Length != boneCount) names = System.Enum.GetNames(typeof(UniBones));
				return names;
			}
		}

		public static Transform GetBone(int hash)
		{
			int index = GetIndex(hash);
			return index != -1 ? Bones[index] : null;
		}

		public static Transform GetBone(string name)
		{
			int index = GetIndex(name);
			return index != -1 ? Bones[index] : null;
		}

		public static int GetIndex(int hash)
		{
			int[] hashes = Hashes;
			for (int i = 0; i < hashes.Length; i++) if (hashes[i] == hash) return i;
			return -1;
		}

		public static int GetIndex(string name)
		{
			return GetIndex(UMA.UMAUtils.StringToHash(name));
		}

		private static Transform[] bones = new Transform[0];
		public static Transform[] Bones
		{
			get
			{
				if (bones.Length != boneCount)
				{
					DisposeBones();
					CreateBones();
				}
				EnsurseBones();
				return bones;
			}
		}

		public static void DisposeBones()
		{
			for (int i = 0; i < bones.Length; i++)
				if (bones[i] != null)
					if (Application.isEditor) Object.DestroyImmediate(bones[i].gameObject);
					else Object.Destroy(bones[i].gameObject);
		}

		private static void CreateBones()
		{
			string[] boneNames = System.Enum.GetNames(typeof(UniBones));
			bones = new Transform[boneNames.Length];
			for (int i = 0; i < boneNames.Length; i++)
			{
				Transform transform = new GameObject(boneNames[i]) { hideFlags = HideFlags.None }.transform;
				if (i > 0) transform.SetParent(bones[Parent(i)]);
				bones[i] = transform;
			}

		}

		private static void EnsurseBones()
		{
			for (int i = 0; i < bones.Length; i++)
				if (bones[i] == null)
				{
					DisposeBones();
					CreateBones();
					return;
				}
		}

		public static void SetBindPose()
		{
			Transform[] transforms = Bones;
			for (int i = 0; i < transforms.Length; i++) BindPoseLocalTranslation(i, transforms[i]);
		}

		public static void SetMasculineFemale()
		{
			Transform[] transforms = Bones;
			for (int i = 0; i < transforms.Length; i++) MasculineFemaleLocalTranslation(i, transforms[i]);
		}

		public static void SetFeminineMale()
		{
			Transform[] transforms = Bones;
			for (int i = 0; i < transforms.Length; i++) FeminineMaleLocalTranslation(i, transforms[i]);
		}

		public static void SetAndroToFemale()
		{
			Transform[] transforms = Bones;
			for (int i = 0; i < transforms.Length; i++) AndroToFemaleLocalTranslation(i, transforms[i]);
		}

		public static void SetAndroToMale()
		{
			Transform[] transforms = Bones;
			for (int i = 0; i < transforms.Length; i++) AndroToMaleLocalTranslation(i, transforms[i]);
		}

		public static void SetSlot(UMAMeshData meshData, bool femaleHipBone = true)
		{
			UMATransform[] umaTransforms = meshData.umaBones;
			for (int i = 0; i < umaTransforms.Length; i++)
			{
				Transform bone = GetBone(umaTransforms[i].hash);
				if (bone == null) continue;
				bone.localPosition = umaTransforms[i].position;
				bone.localRotation = umaTransforms[i].rotation;
				bone.localScale = umaTransforms[i].scale;
			}
			if (femaleHipBone)
				Bones[(int)UniBones.Hips].localPosition = new Vector3(0, 1.082601f, -0.03134322f);//F
			else
				Bones[(int)UniBones.Hips].localPosition = new Vector3(0, 1.035215f, -0.01893914f);//M

			Bones[(int)UniBones.Hips].localRotation = Quaternion.Euler(6.335001f, 0f, -90.00001f);
			Bones[(int)UniBones.Hips].localScale = Vector3.one;
		}
		#endregion Bones

	}
}