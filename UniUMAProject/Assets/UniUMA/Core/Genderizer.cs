﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UMA;
using UMA.CharacterSystem;

namespace UniUMA
{
	// takes the value of the MaleFemaleness DNA and applies the reult to the starting weight power
	public class Genderizer : MonoBehaviour
	{

		private DynamicCharacterAvatar targetAvatar;
		private RaceData lastRace;
		private float currentMaleFemaleness = 0.5f;

		[Tooltip("The name of the gender controlling DNA")]
		public string genderDnaName = "MaleFemaleness";
		[Tooltip("Used by UniUMA-Andro and UniUMA-MasculineFemale")]
		public string maleBlendShapeName = "MaleShapePatch";
		[Tooltip("Used by UniUMA-Andro and UniUMA-FeminineMale")]
		public string femaleBlendShapeName = "FemaleShapePatch";
		[Tooltip("Used by UniUMA-Andro and UniUMA-MasculineFemale")]
		public DynamicDNAConverterBehaviour malenessConverter;
		[Tooltip("Used by UniUMA-Andro and UniUMA-FeminineMale")]
		public DynamicDNAConverterBehaviour femalenessConverter;
		public bool applyGenderBlendshapes = true;
		public bool applyGenderUVs = true;
		public bool applyGenderNormals = true;

		private List<SlotData> baseRecipeSlots = new List<SlotData>();
		private List<SlotData> slotsToTweak = new List<SlotData>();
		private List<OverlayData> baseRecipeOverlays = new List<OverlayData>();
		private List<OverlayData> overlaysToTweak = new List<OverlayData>();


		// Use this for initialization
		void Start()
		{
			targetAvatar = gameObject.GetComponent<DynamicCharacterAvatar>();
			if (targetAvatar)
			{
				Debug.Log("Added CharacterBegun listener");
				targetAvatar.CharacterBegun.AddListener(UpdateGender);
				targetAvatar.RecipeUpdated.AddListener(ApplyGender);
			}
		}

		//Triggered on CharacterBegun which happens whenever the avatar changes in any way (recipe changes and DNA).
		public void UpdateGender(UMAData umaData)
		{
			lastRace = targetAvatar.activeRace.racedata;
			var dnaDict = targetAvatar.GetDNA();
			if (dnaDict.ContainsKey(genderDnaName))
			{
				currentMaleFemaleness = dnaDict[genderDnaName].Value;
			}
			else
			{
				Debug.LogWarning(genderDnaName + " was not found in the DnaDictionary");
			}
			Debug.Log("UpdateGender MaleFemaleness was "+currentMaleFemaleness);
			ApplyGenderDNA(umaData);
			if (applyGenderBlendshapes)
			{
				ApplyGenderBlendshape(umaData);
			}
			if (applyGenderUVs)
			{
				ApplyGenderUVs(umaData);
			}
			if (applyGenderNormals)
			{
				ApplyGenderUVs(umaData);
			}
		}

		//Triggered on Recipe updated which only happens when the recipe is changed, not on DNA changes
		//Should rebind any slots to the andro rig. And make sure any recipes that are just overlays have UVs that match the base
		public void ApplyGender(UMAData umaData)
		{
			Debug.Log("ApplyGender on RecipeUpdate MaleFemaleness was " + currentMaleFemaleness);
			GenerateTweakLists(umaData);
			RebindWardrobeRecipesSlots(umaData);
			RecalculateWardrobeRecipesUVs(umaData);
			ClearTweakLists();
		}

		//Sets the starting pose values on the DNAConverters according to what the 'MaleFemaleness' DNA value is
		private void ApplyGenderDNA(UMAData umaData)
		{
			if (lastRace.raceName == "UniUMA-Andro")
			{
				//Andro is using Morphset right now so just return
				return;
				/*foreach (DnaConverterBehaviour dcb in umaData.umaRecipe.raceData.dnaConverterList)
				{
					if (dcb.GetType() == typeof(DynamicDNAConverterBehaviour) && (DynamicDNAConverterBehaviour)dcb == malenessConverter)
					{
						if (currentMaleFemaleness >= 0.5f)
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = 0f;
						else
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = ((1 - currentMaleFemaleness) - 0.5f) * 2f;
					}
					else if (dcb.GetType() == typeof(DynamicDNAConverterBehaviour) && (DynamicDNAConverterBehaviour)dcb == femalenessConverter)
					{
						if (currentMaleFemaleness <= 0.5f)
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = 0f;
						else
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = (currentMaleFemaleness - 0.5f) * 2f;
					}
				}*/
			}
			else if (lastRace.raceName == "UniUMA-MasculineFemale")
			{
				foreach (DnaConverterBehaviour dcb in umaData.umaRecipe.raceData.dnaConverterList)
				{
					if (dcb.GetType() == typeof(DynamicDNAConverterBehaviour) && (DynamicDNAConverterBehaviour)dcb == malenessConverter)
					{
						//Nothing needs to be done if the gender is Female
						if (currentMaleFemaleness == 1f)
						{
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = 0f;
						}
						else
						{
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = (1 - currentMaleFemaleness);
						}
					}
				}
			}
			else if (lastRace.raceName == "UniUMA-FeminineMale")
			{
				//Nothing needs to be done if the gender is Male
				foreach (DnaConverterBehaviour dcb in umaData.umaRecipe.raceData.dnaConverterList)
				{
					if (dcb.GetType() == typeof(DynamicDNAConverterBehaviour) && (DynamicDNAConverterBehaviour)dcb == femalenessConverter)
					{
						//Nothing needs to be done if the gender is Female
						if (currentMaleFemaleness == 0f)
						{
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = 0f;
						}
						else
						{
							((DynamicDNAConverterBehaviour)dcb).startingPoseWeight = currentMaleFemaleness;
						}
					}
				}
			}
		}
		//These should be applied at the power of the MaleFemaleness. i.e. if the char is 70% femmale the blendshape should be applied at 70% (ish cos in the Andro model 50% female means no blendshape is applied at all)
		//also this method should calculate where the verts should be at that percentage and only actually move the verts by the difference between those values and where the verts are as they will have been moved already by the bone deformation
		//OR I need to figure out what the blendshape itself needs to be to just move the verts from the deform at 100% to the female shape...
		private void ApplyGenderBlendshape(UMAData umaData)
		{
			if (lastRace.raceName == "UniUMA-Andro")
			{
				//Andro is using Morphset right now so just return
				return;
				/*if (maleBlendShapeName != "")
				{
					if (currentMaleFemaleness >= 0.5f)
						umaData.SetBlendShape(maleBlendShapeName, 0f);
					else
						umaData.SetBlendShape(maleBlendShapeName, ((1 - currentMaleFemaleness) - 0.5f) * 2f);
				}
				if (femaleBlendShapeName != "")
				{
					if (currentMaleFemaleness <= 0.5f)
						umaData.SetBlendShape(femaleBlendShapeName, 0f);
					else
						umaData.SetBlendShape(femaleBlendShapeName, (currentMaleFemaleness - 0.5f) * 2f);
				}*/
			}
		}
		//Actually just applying the UVs wont actually work I dont think, because existing textures will have been designed to fit existing UVs
		//i.e. female face is designed to fit female UVs. What kind of needs to happen is more 'reverse' stuff where the texture is distored so that its pixels are where they should be if the texture had been designed for the other UVset
		private void ApplyGenderUVs(UMAData umaData)
		{

		}

		//This should change the normal directions on the mesh so that we dont have 'fake boob shadows' on female for example
		//if we have to calculate these- which I think we will need to for content outside the base recipe- this is gonna be costly...
		//So I suggest we run it in a co-routine that happens a frame after the dna is applied- that way when the slider slides, it will still be fast
		//and after the user stops changing MaleFemelness, a frame later the normals will get updated...
		//UMA needs to do something with this anyway because 'fat bellys' still have the shading of 'six pack' bellys and boobs flattenned to nothing still cast shadows etc
		private void ApplyGenderNormals(UMAData umaData)
		{

		}


		//Here we need to do the 'reverse binding' where we fix the vertex binding positions for existing content.
		private void RebindWardrobeRecipesSlots(UMAData umaData)
		{
			if (lastRace == null || string.IsNullOrEmpty(lastRace.raceName) || lastRace.raceName != "UniUMA-Andro")
			{
				return;
			}
			//foreach of the slots that are not in the baseRecipe and reverse bind them to this races bind positions.
			foreach (SlotData slot in slotsToTweak)
			{
				Debug.Log("slotsToTweak had: " + slot.slotName);
				//============================================================================================================//
				//****@CWMANLEY*****//
				//This is where your magic is needed!!!
				//In the SkinnedMeshMixer there is a type UniMesh, this seems to have similar data in it to UMAMeshData which is in SlotDataAsset.meshData
				//What I am hoping is that we can make a TEMP SlotDataAsset that we can assign to the SlotData in the recipe with tweaked data
				//So here we create an asset to tweak
				var tweakedAsset = CloneSlotDataAsset(slot.asset);
				//then in this asset we want to tweak the meshData.bindPoses so they are correct for the UniUMA rig
				//Magic Needed!
				//
				//we know the position of the bones when the wardrobeRecipes mesh was created (the recipes were either created for UMAHumanMale or UMAHumanFemale)
				//we know the poistion of the verts when the mesh was bound to that rig (its the default position of the verts)
				//we know the weighting of the verts to the bones it was bound to
				//SO we need to figure out where the verts would be if they had been shaped to the andro mesh and bound to the andro rig in the first place.
				for (int i = 0; i < tweakedAsset.meshData.bindPoses.Length; i++)
				{
					//For code like SkinnedMeshMixer to work we might need to determine if the slotwas originally bound to UMAHumanMale or UMAHumanFemale
					//and then use that data to manipulate the bind pose
					//(This does nothing right now- see the method)
					var startingRace = DetermineSlotRace(slot);
					if(startingRace != null)
					{

					}
					//Then perhaps once we have that, we can use the differences between the UMATransforms for the startingRace bones and the UMATransforms for the andreo race bones
					//to calculate the differences between the bind poses and make them right?
				}
				//and the assign the tweaked slotDataAsset to the recipies SlotData
				slot.asset = tweakedAsset;
			}

		}
		//returns the race that a given slot was originally bound to
		private RaceData DetermineSlotRace(SlotData slot)
		{
			//once we have unisex race a recipe might wear a hat for a female and a chestplate for a male, so we will need to know that somehow
			//we may be able to do this by taking the slot.meshData.umaBones for the slot and comparing them to the umabones for the base mesh
			//if they match UMAHumanFemale (forexample) the slot is female etc
			return null;
		}

		//This might actually just be AppltGenderUVs of both might be something else!
		private void RecalculateWardrobeRecipesUVs(UMAData umaData)
		{
			//foreach of the overlays that are not in the baseRecipe update their UVs and textures to match the current gender state.
			foreach (OverlayData overlay in overlaysToTweak)
			{
				Debug.Log("overlaysToTweak had: " + overlay.overlayName);
			}
		}

		private void GenerateTweakLists(UMAData umaData)
		{
			//will this work? I think the actual recipe at this point might have removed some of these - or replaced them with other slots that are compatible
			//sometime in the distant past I seem to remember there being someway of getting slots without capsule colliders and the like, but for now just check for nulls etc
			baseRecipeSlots.AddRange(targetAvatar.activeRace.racedata.baseRaceRecipe.GetCachedRecipe(targetAvatar.context).slotDataList);
			for (int i = 0; i < baseRecipeSlots.Count; i++)
			{
				//I *think* we only need to deal with slots that have slotDataAssets (as opposed to things like CapsuleCollider slot etc)
				if (baseRecipeSlots[i] != null && baseRecipeSlots[i].asset != null)
				{
					Debug.Log("BaseRecipeSlots had: " + baseRecipeSlots[i].slotName);
					for (int oi = 0; oi < baseRecipeSlots[i].OverlayCount; oi++)
					{
						if (!baseRecipeOverlays.Contains(baseRecipeSlots[i].GetOverlay(oi)))
						{
							baseRecipeOverlays.Add(baseRecipeSlots[i].GetOverlay(oi));
						}
					}
				}
			}
			//if the base slots were replaced with ones that are compatible those probably need tweaking too! So if there are ANY slots that are not in the base recipe they probably need tweaking
			for (int i = 0; i < umaData.umaRecipe.slotDataList.Length; i++)
			{
				//I *think* we only need to deal with slots that have slotDataAssets
				if (umaData.umaRecipe.slotDataList[i] != null && umaData.umaRecipe.slotDataList[i].asset != null)
				{
					bool sinbase = false;
					//check if this slot is in the base Recipe
					for (int si = 0; si < baseRecipeSlots.Count; si++)
					{
						//I *think* we only need to deal with slots that have slotDataAssets (as opposed to things like CapsuleCollider slot etc)
						if (baseRecipeSlots[si] != null && baseRecipeSlots[si].asset != null)
						{
							if (baseRecipeSlots[si].asset == umaData.umaRecipe.slotDataList[i].asset)
								sinbase = true;
						}
					}
					//if its not add it to slots to tweak if its not already there
					if (!sinbase)
					{
						if (!slotsToTweak.Contains(umaData.umaRecipe.slotDataList[i]))
						{
							slotsToTweak.Add(umaData.umaRecipe.slotDataList[i]);
						}
					}
					for (int oi = 0; oi < umaData.umaRecipe.slotDataList[i].OverlayCount; oi++)
					{
						bool oinbase = false;
						//check if this overlay is in the base recipe
						for (int oii = 0; oii < baseRecipeOverlays.Count; oii++)
						{
							if (OverlayData.EquivalentAssetAndUse(umaData.umaRecipe.slotDataList[i].GetOverlay(oi), baseRecipeOverlays[oii]))
								oinbase = true;
						}
						//if its not add it to overlays to tweak if its not already there
						if (!oinbase)
						{
							if (!overlaysToTweak.Contains(umaData.umaRecipe.slotDataList[i].GetOverlay(oi)))
								overlaysToTweak.Add(umaData.umaRecipe.slotDataList[i].GetOverlay(oi));
						}
					}
				}
			}
		}

		private void ClearTweakLists()
		{
			baseRecipeSlots.Clear();
			slotsToTweak.Clear();
			baseRecipeOverlays.Clear();
			overlaysToTweak.Clear();
		}

		private SlotDataAsset CloneSlotDataAsset(SlotDataAsset source)
		{
			var newSDA = ScriptableObject.CreateInstance<SlotDataAsset>();
			newSDA.slotName = source.slotName;
			newSDA.nameHash = source.nameHash;
			newSDA.material = source.material;
			newSDA.overlayScale = source.overlayScale;
			newSDA.animatedBoneHashes = source.animatedBoneHashes;
			newSDA.slotDNA = source.slotDNA;
			newSDA.subMeshIndex = source.subMeshIndex;
			newSDA.slotGroup = source.slotGroup;
			newSDA.tags = source.tags;
			newSDA.CharacterBegun = source.CharacterBegun;
			newSDA.SlotAtlassed = source.SlotAtlassed;
			newSDA.DNAApplied = source.DNAApplied;
			newSDA.CharacterCompleted = source.CharacterCompleted;
			//clone the existing MeshData
			newSDA.meshData = new UMAMeshData();
			if (source.meshData.bindPoses != null)
			{
				newSDA.meshData.bindPoses = new Matrix4x4[source.meshData.bindPoses.Length];
				System.Array.Copy(source.meshData.bindPoses, newSDA.meshData.bindPoses, source.meshData.bindPoses.Length);
			}
			if (source.meshData.boneWeights != null)
			{
				newSDA.meshData.boneWeights = new UMABoneWeight[source.meshData.boneWeights.Length];
				System.Array.Copy(source.meshData.boneWeights, newSDA.meshData.boneWeights, source.meshData.boneWeights.Length);
			}
			if (source.meshData.unityBoneWeights != null)
			{
				newSDA.meshData.unityBoneWeights = new BoneWeight[source.meshData.unityBoneWeights.Length];
				System.Array.Copy(source.meshData.unityBoneWeights, newSDA.meshData.unityBoneWeights, source.meshData.unityBoneWeights.Length);
			}
			if (source.meshData.vertices != null)
			{
				newSDA.meshData.vertices = new Vector3[source.meshData.vertices.Length];
				System.Array.Copy(source.meshData.vertices, newSDA.meshData.vertices, source.meshData.vertices.Length);
			}
			if (source.meshData.normals != null)
			{
				newSDA.meshData.normals = new Vector3[source.meshData.normals.Length];
				System.Array.Copy(source.meshData.normals, newSDA.meshData.normals, source.meshData.normals.Length);
			}
			if (source.meshData.tangents != null)
			{
				newSDA.meshData.tangents = new Vector4[source.meshData.tangents.Length];
				System.Array.Copy(source.meshData.tangents, newSDA.meshData.tangents, source.meshData.tangents.Length);
			}
			if (source.meshData.colors32 != null)
			{
				newSDA.meshData.colors32 = new Color32[source.meshData.colors32.Length];
				System.Array.Copy(source.meshData.colors32, newSDA.meshData.colors32, source.meshData.colors32.Length);
			}
			if (source.meshData.uv != null)
			{
				newSDA.meshData.uv = new Vector2[source.meshData.uv.Length];
				System.Array.Copy(source.meshData.uv, newSDA.meshData.uv, source.meshData.uv.Length);
			}
			if (source.meshData.uv2 != null)
			{
				newSDA.meshData.uv2 = new Vector2[source.meshData.uv2.Length];
				System.Array.Copy(source.meshData.uv2, newSDA.meshData.uv2, source.meshData.uv2.Length);
			}
			if (source.meshData.uv3 != null)
			{
				newSDA.meshData.uv3 = new Vector2[source.meshData.uv3.Length];
				System.Array.Copy(source.meshData.uv3, newSDA.meshData.uv3, source.meshData.uv3.Length);
			}
			if (source.meshData.uv4 != null)
			{
				newSDA.meshData.uv4 = new Vector2[source.meshData.uv4.Length];
				System.Array.Copy(source.meshData.uv4, newSDA.meshData.uv4, source.meshData.uv4.Length);
			}
			//UMABlendshape is not a struct so needs deeper copying still
			newSDA.meshData.blendShapes = new UMABlendShape[source.meshData.blendShapes.Length];
			for(int i = 0; i < source.meshData.blendShapes.Length; i++)
			{
				newSDA.meshData.blendShapes[i].shapeName = source.meshData.blendShapes[i].shapeName;
				//UMABlendshape.UMABlendFrame is not a struct either so needs deep cloning
				newSDA.meshData.blendShapes[i].frames = new UMABlendFrame[source.meshData.blendShapes[i].frames.Length];
				for(int bi = 0; bi < newSDA.meshData.blendShapes[i].frames.Length; bi++)
				{
					newSDA.meshData.blendShapes[i].frames[bi].frameWeight = source.meshData.blendShapes[i].frames[bi].frameWeight;
					if (source.meshData.blendShapes[i].frames[bi].deltaVertices != null)
					{
						newSDA.meshData.blendShapes[i].frames[bi].deltaVertices = new Vector3[source.meshData.blendShapes[i].frames[bi].deltaVertices.Length];
						System.Array.Copy(source.meshData.blendShapes[i].frames[bi].deltaVertices, newSDA.meshData.blendShapes[i].frames[bi].deltaVertices, source.meshData.blendShapes[i].frames[bi].deltaVertices.Length);
					}
					if (source.meshData.blendShapes[i].frames[bi].deltaNormals != null)
					{
						newSDA.meshData.blendShapes[i].frames[bi].deltaNormals = new Vector3[source.meshData.blendShapes[i].frames[bi].deltaNormals.Length];
						System.Array.Copy(source.meshData.blendShapes[i].frames[bi].deltaNormals, newSDA.meshData.blendShapes[i].frames[bi].deltaNormals, source.meshData.blendShapes[i].frames[bi].deltaNormals.Length);
					}
					if (source.meshData.blendShapes[i].frames[bi].deltaTangents != null)
					{
						newSDA.meshData.blendShapes[i].frames[bi].deltaTangents = new Vector3[source.meshData.blendShapes[i].frames[bi].deltaTangents.Length];
						System.Array.Copy(source.meshData.blendShapes[i].frames[bi].deltaTangents, newSDA.meshData.blendShapes[i].frames[bi].deltaTangents, source.meshData.blendShapes[i].frames[bi].deltaTangents.Length);
					}
				}
			}
			if (source.meshData.clothSkinning != null)
			{
				newSDA.meshData.clothSkinning = new ClothSkinningCoefficient[source.meshData.clothSkinning.Length];
				System.Array.Copy(source.meshData.clothSkinning, newSDA.meshData.clothSkinning, source.meshData.clothSkinning.Length);
			}
			if (source.meshData.clothSkinningSerialized != null)
			{
				newSDA.meshData.clothSkinningSerialized = new Vector2[source.meshData.clothSkinningSerialized.Length];
				System.Array.Copy(source.meshData.clothSkinningSerialized, newSDA.meshData.clothSkinningSerialized, source.meshData.clothSkinningSerialized.Length);
			}
			if (source.meshData.submeshes != null)
			{
				newSDA.meshData.submeshes = new SubMeshTriangles[source.meshData.submeshes.Length];
				System.Array.Copy(source.meshData.submeshes, newSDA.meshData.submeshes, source.meshData.submeshes.Length);
			}
			//not serialized but will be there when we copy
			if (source.meshData.bones != null)
			{
				newSDA.meshData.bones = new Transform[source.meshData.bones.Length];
				System.Array.Copy(source.meshData.bones, newSDA.meshData.bones, source.meshData.bones.Length);
			}
			//not serialized but will be there when we copy
			newSDA.meshData.rootBone = source.meshData.rootBone;
			if (source.meshData.umaBones != null)
			{
				newSDA.meshData.umaBones = new UMATransform[source.meshData.umaBones.Length];
				System.Array.Copy(source.meshData.umaBones, newSDA.meshData.umaBones, source.meshData.umaBones.Length);
			}
			newSDA.meshData.umaBoneCount = source.meshData.umaBoneCount;
			newSDA.meshData.rootBoneHash = source.meshData.rootBoneHash;
			if (source.meshData.boneNameHashes != null)
			{
				newSDA.meshData.boneNameHashes = new int[source.meshData.boneNameHashes.Length];
				System.Array.Copy(source.meshData.boneNameHashes, newSDA.meshData.boneNameHashes, source.meshData.boneNameHashes.Length);
			}
			newSDA.meshData.subMeshCount = source.meshData.subMeshCount;
			newSDA.meshData.vertexCount = source.meshData.vertexCount;
			newSDA.meshData.RootBoneName = source.meshData.RootBoneName;
			return newSDA;
		}
	}
}
