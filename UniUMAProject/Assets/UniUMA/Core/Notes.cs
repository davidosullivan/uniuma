﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UniUMA
{
	public class Notes : MonoBehaviour
	{
		[TextArea(5, 25)]
		public string note;
	}
}
