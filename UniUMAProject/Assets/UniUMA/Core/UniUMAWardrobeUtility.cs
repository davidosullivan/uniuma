﻿using UMA;
using UMA.CharacterSystem;
using System.Collections.Generic;
using UnityEngine;

namespace UniUMA
{
    public class UniUMAWardrobeUtility
    {
        public static bool SlotCompatibility(DynamicCharacterAvatar dynamicCharacterAvatar, string race, SlotDataAsset slot)
        {
            UMAContext context = dynamicCharacterAvatar.context;
            if (context == null) return false;
            DynamicCharacterSystem characterSystem = context.dynamicCharacterSystem as DynamicCharacterSystem;
            if (characterSystem == null) return false;
            Dictionary<string, Dictionary<string, List<UMATextRecipe>>> Recipes = characterSystem.Recipes;
            foreach (string raceName in Recipes.Keys)
            {
                RaceData raceData = (context.raceLibrary as DynamicRaceLibrary).GetRace(raceName, false);
                if (raceData == null) continue;
                if (raceData.raceName != race) continue;
                Dictionary<string, List<UMATextRecipe>> RaceRecipes = Recipes[raceName];
                foreach (KeyValuePair<string, List<UMATextRecipe>> item in RaceRecipes)
                {
                    List<UMATextRecipe> recipes = item.Value;
                    for (int i = 0; i < recipes.Count; i++)
                    {
                        SlotData[] slots = recipes[i].GetCachedRecipe(context).GetAllSlots();
                        for (int ii = 0; ii < slots.Length; ii++)
                        {
                            if (slots[ii] == null) continue;
                            if (slots[ii].asset.nameHash == slot.nameHash)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
       
    }
}