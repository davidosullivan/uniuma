﻿using System.Collections.Generic;
using UnityEngine;
using UMA;
using UMA.CharacterSystem;

namespace UniUMA
{
    public static class UniUMASlotConverter
    {
        public readonly static string uniRace = "UniUMA-Andro";
        public readonly static string femaleRace = "HumanFemaleDCS";
        public readonly static string maleRace = "HumanMaleDCS";
        public readonly static Quaternion umaRotation = Quaternion.Euler(-90f, 180f, 0f);

        public static SlotData[] AfterRecipeUpdated(UMAData umaData)
        {
            DynamicCharacterAvatar avatar = umaData.GetComponent<DynamicCharacterAvatar>();
            if (!avatar || !umaData.umaRecipe.Validate() || string.IsNullOrEmpty(umaData.umaRecipe.raceData.raceName) || umaData.umaRecipe.raceData.raceName != uniRace)
                return new SlotData[0];

            SlotData[] slots = umaData.umaRecipe.slotDataList;

            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i] == null) { Debug.Log(slots[i]); continue; }
                SlotDataAsset slotDataAsset = slots[i].asset;
                if (slotDataAsset == null) continue;
                if (slotDataAsset.meshData == null) continue;
                if (slotDataAsset.meshData.boneNameHashes.Length != slotDataAsset.meshData.bindPoses.Length) continue;

                int adjustRace = 0;
                if (UniUMAWardrobeUtility.SlotCompatibility(avatar, femaleRace, slotDataAsset)) adjustRace = 1;
                if (UniUMAWardrobeUtility.SlotCompatibility(avatar, maleRace, slotDataAsset)) adjustRace += 2;
                if (adjustRace != 1 && adjustRace != 2) continue;

                slots[i].asset = CreateSlot(slots[i].asset, adjustRace == 1);
            }
            return slots;
        }

        private static SlotDataAsset CreateSlot(SlotDataAsset slotDataAsset, bool female)
        {
            SlotDataAsset slot = ScriptableObject.Instantiate(slotDataAsset);
            string conversionTag = female ? "_AndroFemale" : "_AndroMale";
            slot.name = slot.name + conversionTag;
            slot.slotName = slot.slotName + conversionTag;
            slot.nameHash = slot.slotName.GetHashCode();
            //Debug.Log(slot.slotName);

            UniUMASkeleton.SetBindPose();

            UMAMeshData meshData = slot.meshData;
            Transform[] bones = new Transform[meshData.bindPoses.Length];
            int[] umaBonesHashes = meshData.boneNameHashes;
            UMATransform[] umaBones = new UMATransform[bones.Length];
            Dictionary<int, Transform> missingBones = new Dictionary<int, Transform>();
            for (int i = 0; i < bones.Length; i++)
            {
                int umaBoneIndex = GetIndex(meshData.umaBones, umaBonesHashes[i]);
                if (umaBoneIndex == -1) { Debug.LogError(umaBonesHashes); return slot; }
                umaBones[i] = meshData.umaBones[umaBoneIndex];
                Transform bone = UniUMASkeleton.GetBone(umaBonesHashes[i]);

                if (!bone)
                {
                    // bone = new GameObject(umaBones[i].name) { hideFlags = HideFlags.None }.transform;
                    missingBones.Add(i, bone); //TODO// Pool 
                }
                bones[i] = bone;
            }

            if (missingBones.Count > 0)
            {
                Debug.LogWarning(slot.slotName + "" + "//TODO// Missing Bones");
                return slot;
            }

            //TODO Missing Bones
            //Transform[] androBones = GenderBones.Bones;
            //Parent And Set Local Translation
            //UniUMASkeleton.SetSlot(slotDataAsset.meshData);

            int vertexCount = meshData.vertexCount;
            UMABoneWeight[] boneWeights = meshData.boneWeights;
            Vector3[] meshDataVertices = meshData.vertices;
            Vector3[] meshDataNormals = meshData.normals;
            Vector3[] assetVertices = new Vector3[vertexCount];
            Vector3[] assetNormals = new Vector3[vertexCount];
            Matrix4x4[] bindPoses = meshData.bindPoses;
            Vector3[] vertices = new Vector3[vertexCount];
            Vector3[] normals = new Vector3[vertexCount];

            UniUMASkeleton.SetSlot(slotDataAsset.meshData, female);

            for (int i = 0; i < vertexCount; i++)
            {
                UMABoneWeight boneWeight = boneWeights[i];
                Vector3 position = Vector3.zero;
                Vector3 normal = Vector3.zero;
                for (int ii = 0; ii < 4; ii++)
                {
                    float channelWeight = boneWeight.GetWeight(ii);
                    int channelIndex = boneWeight.GetBoneIndex(ii);
                    if (channelWeight > 0f)
                    {
                        position += bones[channelIndex].localToWorldMatrix.MultiplyPoint3x4(bindPoses[channelIndex].MultiplyPoint3x4(meshDataVertices[i])) * channelWeight;
                        normal += (bones[channelIndex].localToWorldMatrix * bindPoses[channelIndex]).MultiplyVector(meshDataNormals[i]) * channelWeight;
                    }
                }
                
                position = Matrix4x4.Rotate(umaRotation) * position;
                normal = Matrix4x4.Rotate(umaRotation) * normal;

                assetVertices[i] = position;
                assetNormals[i] = normal;
            }

            UniUMASkeleton.SetBindPose();

            if (female)
            {
                for (int i = 0; i < bones.Length; i++) bones[i] = ConvertFemaleBones(meshData, umaBones[i], bones[i]);
                UniUMASkeleton.SetMasculineFemale();
            }
            else UniUMASkeleton.SetFeminineMale();


            for (int i = 0; i < vertexCount; i++)
            {
                UMABoneWeight boneWeight = boneWeights[i];
                Vector3 position = Vector3.zero;
                for (int ii = 0; ii < 4; ii++)
                {
                    float channelWeight = boneWeight.GetWeight(ii);
                    int channelIndex = boneWeight.GetBoneIndex(ii);
                    if (channelWeight > 0f)
                        position += bones[channelIndex].localToWorldMatrix.MultiplyPoint3x4(bindPoses[channelIndex].MultiplyPoint3x4(meshDataVertices[i])) * channelWeight;
                }
                position = Matrix4x4.Rotate(umaRotation) * position;
                vertices[i] = position;
            }

            UniUMASkeleton.SetBindPose();
            for (int i = 0; i < bones.Length; i++) {  bindPoses[i] = bones[i].localToWorldMatrix.inverse * Matrix4x4.Rotate(umaRotation); }

            slot.meshData.vertices = vertices;
            slot.meshData.normals = assetNormals;

            if (female) UniUMASkeleton.SetAndroToFemale();
            else UniUMASkeleton.SetAndroToMale();
            AddPatch(meshData, bones, assetVertices, female ? "FemaleShapePatch" : "MaleShapePatch", female);
            AddPatch(meshData, bones, assetVertices, !female ? "FemaleShapePatch" : "MaleShapePatch", !female);

            //DebugPatch
            AddPatch(meshData, bones, assetVertices, female ? "Female_DebugPatch" : "Male_DebugPatch", female);
            AddPatch(meshData, bones, assetVertices, !female ? "Female_DebugPatch" : "Male_DebugPatch", !female);

            return slot;
        }

        private static void AddPatch(UMAMeshData meshData, Transform[] bones,  Vector3[] baseFrame, string blendshapeName, bool positive)
        { 
            int vertexCount = meshData.vertexCount;

            UMABoneWeight[] boneWeights = meshData.boneWeights;
            Matrix4x4[] bindPoses = meshData.bindPoses;
            Vector3[] vertices = meshData.vertices;

            int direction = positive ? 1 : -1;

            Vector3[] deltaVertices = new Vector3[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                UMABoneWeight boneWeight = boneWeights[i];
                Vector3 position = Vector3.zero;
                for (int ii = 0; ii < 4; ii++)
                {
                    float channelWeight = boneWeight.GetWeight(ii);
                    int channelIndex = boneWeight.GetBoneIndex(ii);
                    if (channelWeight > 0f)
                    {
                        position += bones[channelIndex].localToWorldMatrix.MultiplyPoint3x4(bindPoses[channelIndex].MultiplyPoint3x4(vertices[i])) * channelWeight;
                    }
                }
                position = Matrix4x4.Rotate(umaRotation) * position;
                deltaVertices[i] = (baseFrame[i] - position) * direction;
            }

            meshData.blendShapes = new List<UMABlendShape>(meshData.blendShapes)
            {
                new UMABlendShape()
                {
                    shapeName = blendshapeName,
                    frames = new UMABlendFrame[1]{new UMABlendFrame(vertexCount, false, false){ deltaVertices = deltaVertices, frameWeight = 100f } }
                }
            }.ToArray();
        }

        /// should happen after the original verts have been done and in andro bindpose?
        private  static Transform ConvertFemaleBones(UMAMeshData meshData, UMATransform umaBone, Transform transform)
        {
            Transform bone = transform;
            bone = SwapBone(meshData, umaBone, bone, UniUMASkeleton.UniBones.LeftOuterBreast.ToString(), UniUMASkeleton.UniBones.LeftOuterBreastAdjust.ToString());
            bone = SwapBone(meshData, umaBone, bone, UniUMASkeleton.UniBones.LeftInnerBreast.ToString(), UniUMASkeleton.UniBones.LeftInnerBreastAdjust.ToString());
            bone = SwapBone(meshData, umaBone, bone, UniUMASkeleton.UniBones.RightOuterBreast.ToString(), UniUMASkeleton.UniBones.RightOuterBreastAdjust.ToString());
            bone = SwapBone(meshData, umaBone, bone, UniUMASkeleton.UniBones.RightInnerBreast.ToString(), UniUMASkeleton.UniBones.RightInnerBreastAdjust.ToString());
            return bone;
        }

        private static Transform SwapBone(UMAMeshData meshData, UMATransform umaBone, Transform bone, string oringalName, string newName)
        {
            if (umaBone.hash != UMA.UMAUtils.StringToHash(oringalName)) return bone;
            int index = GetIndex(meshData.boneNameHashes, umaBone.hash);
            if (index == -1) return bone;
            meshData.umaBones[index] = new UMATransform(UniUMASkeleton.GetBone(newName), umaBone.hash, UMA.UMAUtils.StringToHash(newName));
            meshData.boneNameHashes[index] = UMA.UMAUtils.StringToHash(newName);
            return UniUMASkeleton.GetBone(newName);
        }

        private static int GetIndex(int[] hashCodes, int id)
        {
            int[] hashes = hashCodes;
            for (int i = 0; i < hashes.Length; i++) if (hashes[i] == id) return i;
            return -1;
        }

        private static int GetIndex(UMATransform[] hashCodes, int id)
        {
            UMATransform[] hashes = hashCodes;
            for (int i = 0; i < hashes.Length; i++) if (hashes[i].hash == id) return i;
            return -1;
        }
    }
}