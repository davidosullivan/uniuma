﻿Shader "Hidden/UVSwap" {
	Properties {
	    _MainTex("Render Texture", 2D) = "white" {}
		[Toggle] _MainTexUV("Standard UV", Float) = 0
	}
	SubShader {

	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag 
		#include "UnityCG.cginc"
		
		float _MainTexUV;

		struct v2f {
			float4 pos : SV_POSITION;
			float4 bakedValue : TEXCOORD0;
			float2 uv : TEXCOORD1;
		};

        sampler2D _MainTex;


		v2f vert(appdata_full v) {
			v2f o;
			UNITY_INITIALIZE_OUTPUT(v2f,o); 
			o.pos = float4 ((v.texcoord.xy * 2) - 1,1,1);
			o.pos.y = -o.pos.y;
			o.uv = _MainTexUV == 0 ? v.texcoord1 : v.texcoord;
			return o;
		}

		half4 frag  (v2f i) : SV_Target {
			return tex2D(_MainTex, i.uv).rgba;
		}

		ENDCG

	}
}
Fallback Off
}