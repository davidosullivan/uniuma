﻿using UnityEngine;
using UMA;
using UMA.CharacterSystem;
namespace UniUMA
{
    [RequireComponent(typeof(DynamicCharacterAvatar))]
    public class GenderSkinningTest : MonoBehaviour
    {

        public SlotData[] slots;

        public void Start()
        {
            DynamicCharacterAvatar avatar = gameObject.GetComponent<DynamicCharacterAvatar>();
            if (avatar) avatar.RecipeUpdated.AddListener(RecipeUpdated);
        }

        public void RecipeUpdated(UMAData umaData)
        {
            slots = UniUMASlotConverter.AfterRecipeUpdated(umaData);
        }

    }
}