﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MeshTransformation;
public class shooting : MonoBehaviour {

    public GameObject[] ObjectsDestroy;
    public LayerMask LayerMask;
    List<Vector3> Pos = new List<Vector3>();
    Vector3[] pos_;
    public Material mat;
    ComputeBuffer buffer;
    public float DistDestroy;

    private void Start()
    {
            mat = GetComponent<TransformationMesh>().material;
    }

    void Update () {
        RaycastHit Hit = new RaycastHit();
        Ray Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetKeyDown(KeyCode.Mouse0) & Physics.Raycast(Ray, out Hit, 10000, LayerMask))
        {
            for (int i = 0;i < ObjectsDestroy.Length;i++)
            {
                if (ObjectsDestroy[i] != null) {
                    if (Vector3.Distance(ObjectsDestroy[i].transform.position, Hit.point) < DistDestroy) {
                        Destroy(ObjectsDestroy[i]);
                    }
                }
            }
            Pos.Add(Hit.point);
            pos_ = Pos.ToArray();
            if (buffer != null) {
                buffer.Dispose();
            }
            buffer = new ComputeBuffer(Pos.Count, sizeof(float) * 3);
            buffer.SetData(pos_);
            mat.SetBuffer("_points", buffer);
            mat.SetInt("_pointsLength", Pos.Count);
            
        }
    }

    void OnDisable()
    {
        if (buffer != null)
        {
            buffer.Release();
        }

    }
    void OnDestroy()
    {
        if (buffer != null)
        {
            buffer.Release();
        }

    }

}
